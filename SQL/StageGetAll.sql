USE Budget 
GO

DROP PROCEDURE IF EXISTS StageGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE StageGetAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		StageId,
		StageName
	FROM Stage
ORDER BY StageId DESC
END

GO

GRANT EXECUTE ON StageGetAll TO Budgetviewer 
