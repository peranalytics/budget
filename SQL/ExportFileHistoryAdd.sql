USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportFileHistoryAdd
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportFileHistoryAdd
	@ExportId INT,
	@ExportFileName NVARCHAR(300)
AS
BEGIN
	
	SET NOCOUNT ON;
	

	INSERT INTO [dbo].[ExportFileHistory]
           ([ExportId]
           ,ExportFileName
		   ,ExportDate)
     SELECT 
		@ExportId,
		@ExportFileName,
		GETDATE()





END

GO

GRANT EXECUTE ON ExportFileHistoryAdd TO Budgetviewer 
