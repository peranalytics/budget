USE Budget

INSERT INTO DataProviders
SELECT 1, 'Non-Market Public Corporations (NMPCs)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Extra Budgetary Funds (EBFs)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'Regulators ', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'Approved Housing Bodies  (AHBs)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 5, 'National Treasury Management Agency (NTMA)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Housing Finance Agency (HFA)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 7, 'Strategic Banking Corporation Ireland (SBCI)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'Local Authority ', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 9, 'Irish Strategic Investment Fund (ISIF)', 'Survey', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 10, 'Central Statistics Office', 'Excel Spreadsheet', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Department of Public Expenditure and Reform ', 'REV File -Excel Spreadsheet', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Central Budget Office', 'Anchor Sheet - Excel Spreadsheet', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 13, 'Social Insurance Fund (SIF)', 'Excel Spreadsheet', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 14, 'IBRC', 'Excel Spreadsheet', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 15, 'HSE', 'Manual - Inputted/Trended', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 16, 'Educational Training Boards ', 'Manual - Inputted/Trended', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 17, 'Institutre of Technologies', 'Maual -Inputted/Trended', GETDATE(), GETDATE(), 1 
