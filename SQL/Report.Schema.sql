USE Budget

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Report')
BEGIN
	EXEC('CREATE SCHEMA Report')
END