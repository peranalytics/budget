USE [Budget]
GO

DROP PROCEDURE IF EXISTS [Report].[GetBudgetDataWithImpute]
/****** Object:  StoredProcedure [Report].[GetBudgetData]    Script Date: 23/01/2023 11:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 23/01/2023
-- Description:	
-- =============================================
CREATE PROCEDURE [Report].[GetBudgetDataWithImpute]
	@StageYear INT,
	@RelevantAuthorityId NVARCHAR(MAX) = '-1',
	@ProviderId NVARCHAR(MAX) = '-1',
	@StageId INT,
	@DataTypeId NVARCHAR(MAX) = '-1',
	@YearID NVARCHAR(MAX) = '-1',
	@Impute INT  -- 1 is impute - get the previous stage / years data, 0 is get the most recent year
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CAST(VALUE AS INT) AS RelevantAuthorityId
	INTO #RelevantAuthority FROM STRING_SPLIT(@RelevantAuthorityId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS ProviderId
	INTO #Provider FROM STRING_SPLIT(@ProviderId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS DataTypeId
	INTO #DataType FROM STRING_SPLIT(@DataTypeId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS YearID
	INTO #YearID FROM STRING_SPLIT(@YearID, ',')  
	WHERE RTRIM(VALUE) <> '';


	SELECT 
		  [VersionId]
		  ,bd.[ProviderId]
		  ,[ProviderName]
		  ,bd.[RelevantAuthorityId]
		  ,[RelevantAuthorityName]
		  ,bd.[StageId]
		  ,StageName
		  ,bd.[StageYearId]
		  ,ec.[EsaCode]
		  ,(CASE WHEN bd.EsaCode IN ('D.2','D.1') THEN 1
				WHEN bd.EsaCode IN ('D.5','D.62') THEN 2
				WHEN bd.EsaCode IN ('D.91R','P.2') THEN 3
				WHEN bd.EsaCode IN ('D.61','D.6') THEN 4
				WHEN bd.EsaCode IN ('D.4','D.41') THEN 5
				WHEN bd.EsaCode IN ('P.13','D.3') THEN 6
				WHEN bd.EsaCode IN ('F.4','P.51G') THEN 7
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_CG') THEN 8
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_NMPC') THEN 9
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_EBF') THEN 10
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_LG') THEN 11
				WHEN (bd.EsaCode = '0' AND bd.EsaCategoryCode = 'REC') THEN 12
				WHEN bd.EsaCode = 'D.9' THEN 8
				WHEN bd.EsaCode = 'D.7' THEN 9
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_CG') THEN 11     -- added 09/02/23, previously PAY_LG which was incorrect! 
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_NMPC') THEN 12
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_EBF') THEN 13
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_LG') THEN 14
     			WHEN (bd.EsaCode = '0' AND bd.EsaCategoryCode = 'PAY') THEN 15	
			END) AS 'Order'				
		  ,[EsaCategoryCode]
		  ,EsaCodeDescription
		  ,[YearId]
		  ,bd.[DataTypeId]
		  ,DataType
		  ,[Amount] 
	FROM BudgetDataFlatFile bd WITH(NOLOCK) 
	INNER JOIN EsaCodes ec WITH(NOLOCK)
	ON bd.[EsaCode] = ec.EsaCode
	INNER JOIN GetLatestStagePerRa(@Impute, @StageYear, @StageId) sra
	ON bd.StageYearId = sra.StageYearId
	AND bd.StageId = sra.StageId
	AND bd.RelevantAuthorityId = sra.RelevantAuthorityId
	WHERE 
	(@RelevantAuthorityId = '-1' OR bd.[RelevantAuthorityId] IN (SELECT ra.RelevantAuthorityId FROM #RelevantAuthority AS ra))
	AND (@ProviderId = '-1' OR [ProviderId] IN (SELECT pr.ProviderId FROM #Provider AS pr))
	AND (@DataTypeId = '-1' OR [DataTypeId] IN (SELECT dt.DataTypeId FROM #DataType AS dt))
	AND (@YearID = '-1' or [YearId] IN (SELECT YearId FROM #YearID))
	AND NOT (Amount = 0 OR Amount IS NULL)

	DROP TABLE #RelevantAuthority
	DROP TABLE #Provider
	DROP TABLE #DataType
	DROP TABLE #YearID
END


GRANT EXECUTE ON [Report].[GetBudgetDataWithImpute] TO Budgetviewer 

