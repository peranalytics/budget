USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE DebtDataFlatFile

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebtDataFlatFile]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[DebtDataFlatFile](
	[VersionId] [int] NOT NULL,
	[ProviderId] [int] NOT NULL,
	ProviderCode NVARCHAR(50) NOT NULL,
	ProviderName NVARCHAR(50) NOT NULL,
	[DebtCategory] NVARCHAR(100) NOT NULL,
	[InputData] NVARCHAR(MAX) NOT NULL,
	[Identifiers] NVARCHAR(100) NOT NULL,
	[Units] NVARCHAR(100) NOT NULL,
	[StageId] [int] NOT NULL,
	Stage NVARCHAR(100) NOT NULL,
	[StageYearId] [int] NOT NULL,
	[YearId] INT NOT NULL,
	[Amount] DECIMAL(10,2) NULL,
	[ModifyDate] [datetime] NULL DEFAULT GETDATE(),
	[CreateDate] [datetime] NULL DEFAULT GETDATE()
	)
END
GO



SELECT * FROM [DebtDataFlatFile]