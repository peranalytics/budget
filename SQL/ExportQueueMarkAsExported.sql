USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportQueueMarkAsExported
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportQueueMarkAsExported
	@ExportId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE ExportQueue
	SET Exported = 1,
	ExportDate = GETDATE()
	WHERE ExportId = @ExportId
END

GO

GRANT EXECUTE ON ExportQueueMarkAsExported TO Budgetviewer 
