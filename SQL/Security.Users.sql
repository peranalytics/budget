USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [Security].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	PasswordHash [nvarchar](MAX) NOT NULL,
	SecurityStamp [nvarchar](MAX) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[ResetRequired] [bit] NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[Deleted] [bit] NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[LastLogin] [datetime] NULL,
	[FailedAttempts] [int] NULL,
	[LastFailedAttempt] [datetime] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users_Id] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF__Users__Enabled__4830B400]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Users] ADD  DEFAULT ((1)) FOR [Enabled]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF__Users__ResetRequ__4924D839]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Users] ADD  DEFAULT ((1)) FOR [ResetRequired]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF__Users__ModifyDat__4C0144E4]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Users] ADD  DEFAULT (getdate()) FOR [ModifyDate]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF__Users__CreateDat__4CF5691D]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Users] ADD  DEFAULT (getdate()) FOR [CreateDate]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[DF__Users__Deleted__090A5324]') AND type = 'D')
BEGIN
ALTER TABLE [Security].[Users] ADD  DEFAULT ((0)) FOR [Deleted]
END
GO


