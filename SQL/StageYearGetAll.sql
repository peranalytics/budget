USE Budget 
GO

DROP PROCEDURE IF EXISTS StageYearGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE StageYearGetAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT DISTINCT
		y.YearId
	FROM Year y
	INNER JOIN BudgetData bd WITH(NOLOCK)
	ON bd.StageYearId = y.YearId
	ORDER by y.YearId DESC
END

GO

GRANT EXECUTE ON StageYearGetAll TO Budgetviewer 
