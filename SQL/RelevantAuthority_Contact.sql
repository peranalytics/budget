USE [Budget]
GO

SELECT*
  FROM [dbo].[RelevantAuthority]
GO


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContactDetails' AND Object_ID = Object_ID(N'RelevantAuthority'))
BEGIN
    ALTER TABLE RelevantAuthority ADD ContactDetails NVARCHAR(MAX)
END

GO

SELECT*
  FROM [dbo].[RelevantAuthority]
GO