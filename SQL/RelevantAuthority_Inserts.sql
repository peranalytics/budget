USE Budget

INSERT INTO RelevantAuthority
SELECT 1, 'Bord BIA', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Teagasc', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Horse Racing Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Sea Fisheries Protection Authority', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Marine Institute ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Bord Iascaigh Mhara', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Environmental Protection Agency ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Inland Fisheries Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'National Oil Reserve Agency ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'TG4', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'RTE ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'F�S �ireann', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, '�dar�s na Gaeltachta', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'Arts Council ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'Higher Education Authority', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'Solas ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'Dublin Institute for Advance Studies ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 5, 'Data Protection Commissioner', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 5, 'Legal Aid Board ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Irish Rail ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Transport Infrastructure Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Road Safety Authority', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Failte Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Medical Bureau of Raod Safety ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Sport Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Commissioners of Irish Light ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 7, 'Law Reform Commission ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 7, 'National Economic & Social Council (NESDO)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'Enterprise Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'Industrial Development Agency (IDA)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'National Standards Authority of Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'Health and Safety Authority ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 9, 'Pension Authority ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 10, 'Western Development Commission ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 10, 'Irish Water Safety ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 10, 'Pobal ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Irish Water', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Land Development Agency ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Housing Finance Agency ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'An Bord Pleanala ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Strategic Bank Corporation of Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Home Building Finance Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Fishery Harbour Centres Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Environment Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Climate Action Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'Lottery Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'Dormant Account Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'Western Development Commission Investment Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'Residential Institutions Redress Board', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 4, 'National Training Board ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 9, 'Social Insurance Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 13, 'Risk Equalisation Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 13, 'Special Account for Compensation of Hepatitis ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Sundry Monies Deposit Account', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Intestate Estate Fund Deposit Account', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'ELG - Bank Guarantee Scheme Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'CIRF - Credit Institute Resolution Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Credit Union Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Coillte No.2', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Eircom No. 2', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'EU Budget Contribution ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'ISIF (Ireland Strategic Investment Fund)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Post Office Saving Bank Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Local Loan Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 14, 'Bilateral Aid Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Local Government Fund ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Voluntary Housing Scheme ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 10, 'Peace and Reconcilliation ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 6, 'Local Authority Swimming Pool Programmes ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'Comreg ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 8, 'Irish Auditing and Accounting Supervisory Authority (IAASA)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 2, 'The Commission for Regulation of Utilities (CRU)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Private Residential Tenancies Board (PRTB)', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 5, 'Private Security Authority ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 3, 'National Lottery Regulator ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 15, 'Cluid Housing', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 16, 'Circle Voluntary Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 17, 'Coopertaive Housing Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 18, 'Clanmill Housing Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 19, 'Focus Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 20, 'Fold Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 21, 'Housing Association for Integrated Living ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 22, 'The Iveagh Trust ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 23, 'North and East Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 24, 'Oaklee Housing ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 25, 'Respond ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 26, 'Society of St Vincent De Paul ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 27, 'Sophia Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 28, 'St John of God Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 29, 'Tuath Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 7, 'Central Statistics Office ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 30, 'Department of Public Expenditure and Reform', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Central Budget Office', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 30, 'Social Welfare Vote ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'National Treasury Mangement Agency ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Irish Strategic Investment Fund', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 12, 'Irish Bank Resolution Corporation ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 11, 'Local Authorities ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 31, 'Health Service Executive ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 32, 'Educational Training Boards ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 33, 'Institute of Technologies', GETDATE(), GETDATE(), 1 
