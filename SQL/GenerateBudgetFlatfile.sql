USE Budget 
GO

DROP PROCEDURE IF EXISTS GenerateBudgetFlatfile
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE GenerateBudgetFlatfile
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE FROM [dbo].[BudgetDataFlatFile]

	INSERT INTO [dbo].[BudgetDataFlatFile]
           ([VersionId]
           ,[ProviderId]
           ,[ProviderName]
           ,[RelevantAuthorityId]
           ,[RelevantAuthorityName]
           ,[StageId]
           ,[StageName]
           ,[StageYearId]
           ,[EsaCode]
           ,[EsaCategoryCode]
           ,[YearId]
           ,[DataTypeId]
           ,[DataType]
           ,[Amount])
	SELECT 
		  [VersionId]
		  ,bd.[ProviderId]
		  ,[ProviderName]
		  ,bd.[RelevantAuthorityId]
		  ,[RelevantAuthorityName]
		  ,bd.[StageId]
		  ,StageName
		  ,[StageYearId]
		  ,[EsaCode]
		  ,[EsaCategoryCode]
		  ,[YearId]
		  ,bd.[DataTypeId]
		  ,DataType
		  ,[Amount] 
	FROM BudgetData bd WITH(NOLOCK) 
	INNER JOIN DataProviders dp WITH(NOLOCK)
	ON dp.ProviderId = bd.ProviderId
	INNER JOIN RelevantAuthority ra WITH(NOLOCK)
	ON ra.RelevantAuthorityId = bd.RelevantAuthorityId
	INNER JOIN Stage s WITH(NOLOCK)
	ON s.StageId = bd.StageId
	INNER JOIN DataType dt WITH(NOLOCK)
	ON dt.DataTypeId = bd.DataTypeId
END

GO

GRANT EXECUTE ON GenerateBudgetFlatfile TO Budgetviewer 
