USE Budget


DROP PROCEDURE IF EXISTS Report.GetAllRAsByDept
GO

/****** Object:  StoredProcedure [dbo].[GetAllYearsByCycle]    Script Date: 22/05/2020 22:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==================================================================
-- Author:		Tracy
-- Create date: 11/06/2020
-- Description:	 Get list of Depts & RA's with Contact Details
-- ===================================================================

CREATE PROCEDURE Report.[GetAllRAsByDept]
@DeptId NVARCHAR(MAX) = '-1'

AS   

BEGIN  

SELECT CAST(VALUE AS INT) AS DeptId
	INTO #DeptList FROM STRING_SPLIT(@DeptId, ',')  
	WHERE RTRIM(VALUE) <> '';

SELECT 
	ra.DepartmentId,
	d.DepartmentName,
	ra.RelevantAuthorityId,
	ra.RelevantAuthorityName, 
	ra.ContactDetails
FROM RelevantAuthority ra
INNER JOIN Department d on ra.DepartmentId = d.DepartmentId
WHERE ra.DepartmentId = -1 or ra.DepartmentId IN (SELECT ra.DepartmentId from #DeptList AS dl)
ORDER BY d.DepartmentName ASC

	DROP TABLE #DeptList

END 

GO
GRANT EXECUTE ON [Report].[GetAllRAsByDept] TO [Budgetviewer]
GO