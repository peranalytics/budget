USE Budget 
GO

DROP PROCEDURE IF EXISTS Report.GetMicroDebtData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tracy
-- Create date: 08/03/2021
-- Description:	Loads DD
-- =============================================
CREATE PROCEDURE Report.GetMicroDebtData
	@StageYear NVARCHAR(MAX) = '-1',
	@StageId NVARCHAR(MAX) = '-1',
	@ProviderId NVARCHAR(MAX) = '-1'
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CAST(VALUE AS INT) AS StageYearId
	INTO #StageYear FROM STRING_SPLIT(@StageYear, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS StageId
	INTO #Stage FROM STRING_SPLIT(@StageId, ',') 
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS ProviderId
	INTO #Provider FROM STRING_SPLIT(@ProviderId, ',') 
	WHERE RTRIM(VALUE) <> '';

SELECT 
       [ProviderId]
      ,[ProviderCode]
      ,[ProviderName]
      ,[DebtCategory]
      ,[InputData]
      ,[Identifiers]
      ,[Units]
      ,[StageId]
      ,[Stage]
      ,[StageYearId]
      ,[YearId]
      ,[Amount]

  FROM [dbo].[DebtDataFlatFile]

	WHERE (@StageYear = '-1' OR [StageYearId] IN (SELECT sy.StageYearId FROM #StageYear AS sy))
		AND (@StageId = '-1'OR [StageId] IN (SELECT st.StageId FROM #Stage AS st))
		AND (@ProviderId = '-1' OR [ProviderId] IN (SELECT p.ProviderId FROM #Provider AS p))


	DROP TABLE #StageYear
	DROP TABLE #Stage
	DROP TABLE #Provider



END

GO

GRANT EXECUTE ON Report.GetMicroDebtData TO Budgetviewer 
