
USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE [FileHistorySummary]
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistorySummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileHistorySummary](
	[YearId] [int] NULL,
	[RelevantAuthorityId] [int] NULL,
	[RelevantAuthorityName] [nvarchar](255) NULL,
	[Stage] [nvarchar](50) NULL,
	[FileName] [nvarchar](255) NULL,
	[Success] [bit] NULL,
	[UploadedDate] DATETIME NULL,
	[CreateDate] [datetime] NULL DEFAULT GETDATE()
) ON [PRIMARY]
END
GO


