USE Budget 
GO

DROP PROCEDURE IF EXISTS GetStandardSurveysToExport
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE GetStandardSurveysToExport
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		s.ProviderId,
		dp.ProviderCode,
		s.RelevantAuthorityId,
		ra.RelevantAuthorityName
	FROM Surveys s
	INNER JOIN RelevantAuthority ra
	ON ra.RelevantAuthorityId = s.RelevantAuthorityId
	INNER JOIN DataProviders dp
	ON dp.ProviderId = s.ProviderId
	WHERE dp.ProviderId <= 4 -- the rest are specially formatted
END

GO

GRANT EXECUTE ON GetStandardSurveysToExport TO Budgetviewer 
