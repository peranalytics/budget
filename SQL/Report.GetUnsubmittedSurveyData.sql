USE Budget 
GO

DROP PROCEDURE IF EXISTS Report.GetUnsubmittedSurveyData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================
-- Author:		Tracy
-- Create date: 30/01/2023
-- Description:	Loads list of RAs who have not submitted a return for selected Stage & Year
-- ==================================================================================================
CREATE PROCEDURE Report.GetUnsubmittedSurveyData
@StageYearId INT,
@StageId INT
	
AS
BEGIN
	
	SET NOCOUNT ON;


	SELECT
		RelevantAuthorityId,
		RelevantAuthorityName,
		[ContactDetails]		
	FROM RelevantAuthority WHERE RelevantAuthorityId NOT IN 
	(SELECT fh.RelevantAuthorityId from FileHistory fh WHERE fh.StageYearId = @StageYearId AND fh.StageId = @StageId)
	
END
GO

GRANT EXECUTE ON Report.GetUnsubmittedSurveyData TO BudgetViewer 
