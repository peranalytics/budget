USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportHistoryGetByTemplateId
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportHistoryGetByTemplateId
	@TemplateId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		ExportFileId,
		ExportId,
		ExportFileName,
		ExportDate
	FROM ExportFileHistory
	WHERE ExportFileId = @TemplateId
END

GO

GRANT EXECUTE ON ExportHistoryGetByTemplateId TO Budgetviewer 
