USE Budget 
GO

DROP PROCEDURE IF EXISTS LoadBudgetData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE LoadBudgetData
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE bd
	FROM [BudgetData] bd
	INNER JOIN Stage s
	ON bd.StageId = s.StageId
	INNER JOIN [StagingBudgetData] sbd
	ON sbd.ProviderId = bd.ProviderId
	AND sbd.RelevantAuthorityId = bd.RelevantAuthorityId
	AND sbd.Stage = s.StageName
	AND sbd.StageYearId = bd.StageYearId

	INSERT INTO [dbo].[BudgetData]
			   ([VersionId]
			   ,[ProviderId]
			   ,[RelevantAuthorityId]
			   ,[StageId]
			   ,[StageYearId]
			   ,[EsaCode]
			   ,[EsaCategoryCode]
			   ,[YearId]
			   ,[DataTypeId]
			   ,[Amount]
			   ,[ModifyDate]
			   ,[CreateDate])
	SELECT [VersionId]
		  ,[ProviderId]
		  ,[RelevantAuthorityId]
		  ,s.[StageId]
		  ,[StageYearId]
		  ,[EsaCode]
		  ,[EsaCategoryCode]
		  ,[YearId]
		  ,[DataTypeId]
		  ,[Amount]
		  ,GETDATE()
		  ,GETDATE()
	  FROM [dbo].[StagingBudgetData] sbd
	  INNER JOIN Stage s
	  ON sbd.Stage = s.StageName
END

GO

GRANT EXECUTE ON LoadBudgetData TO Budgetviewer 
