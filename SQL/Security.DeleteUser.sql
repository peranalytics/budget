USE budget 
GO

DROP PROCEDURE IF EXISTS [Security].DeleteUser
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Delete user
-- =============================================
CREATE PROCEDURE [Security].DeleteUser
	@UserId INT
AS
BEGIN
	UPDATE [Security].Users
	SET [Deleted] = 1,
	ModifyDate = GETDATE()
	WHERE UserId = @UserId

	
END
GO

GRANT EXECUTE ON [Security].DeleteUser TO budgetviewer 
