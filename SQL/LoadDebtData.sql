USE Budget 
GO

DROP PROCEDURE IF EXISTS LoadDebtData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads DD
-- =============================================
CREATE PROCEDURE LoadDebtData
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE bd
	FROM [DebtData] bd
	INNER JOIN Stage s
	ON bd.StageId = s.StageId
	INNER JOIN [StagingDebtData] sbd
	ON sbd.Stage = s.StageName
	AND sbd.StageYear = bd.StageYearId
	
	INSERT INTO [dbo].[DebtData]
           ([VersionId]
           ,[ProviderId]
           ,[DebtCategory]
           ,[InputData]
           ,[Identifiers]
           ,[Units]
           ,[StageId]
           ,[StageYearId]
           ,[YearId]
           ,[Amount]
           ,[ModifyDate]
           ,[CreateDate])
	SELECT [VersionId]
		  ,dp.[ProviderId]
		  ,[DebtCategory]
		  ,[InputData]
		  ,[Identifiers]
		  ,[Units]
		  ,s.[StageId]
		  ,[StageYear]
		  ,[Year]
		  ,[Amount]
		  ,GETDATE()
		  ,GETDATE()
	  FROM [dbo].[StagingDebtData] sbd
	  INNER JOIN Stage s
	  ON sbd.Stage = s.StageName
	  INNER JOIN DataProviders dp
	  ON dp.ProviderCode = sbd.ProviderName
END

GO

GRANT EXECUTE ON LoadDebtData TO Budgetviewer 
