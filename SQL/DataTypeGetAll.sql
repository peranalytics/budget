USE Budget 
GO

DROP PROCEDURE IF EXISTS DataTypeGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE DataTypeGetAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		DataTypeId,
		DataType
	FROM DataType
END

GO

GRANT EXECUTE ON DataTypeGetAll TO Budgetviewer 
