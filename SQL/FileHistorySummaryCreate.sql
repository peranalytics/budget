/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Budget]
GO

DROP PROCEDURE IF EXISTS [dbo].[FileHistorySummaryCreate]
GO


-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all FileHistory
-- =============================================
CREATE PROCEDURE [dbo].[FileHistorySummaryCreate]
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM FileHistorySummary

	CREATE TABLE #Years
	(
		YrId INT 
	)

	;WITH yearlist AS
	(
		SELECT YEAR(GetDate()) - 4 AS YrId
		UNION ALL
		SELECT yl.YrId + 1 AS YrId
		FROM yearlist yl
		WHERE yl.YrId + 1 <= YEAR(GetDate()) + 4
	)
	INSERT INTO #Years(YrId)
	SELECT YrId FROM yearlist

	DECLARE @YearCount INT
	SELECT @YearCount = COUNT(YrId) FROM #Years

	CREATE TABLE #FileHistorySummary(
		RelevantAuthorityId INT NOT NULL,
		Yr int NOT NULL,
		[FileName] NVARCHAR(255) NOT NULL,
		Success bit NULL,
		Stage nvarchar(100) NULL,
		UploadedDate datetime NULL
	)

	;WITH FileHistorySummary AS (
	SELECT ra.RelevantAuthorityId AS RelevantAuthorityId, md.StageYearId AS Year, OriginalFileName AS FileName, Valid, StageName, md.CreateDate, RANK()  OVER (PARTITION BY md.StageYearId,  ra.RelevantAuthorityId, md.StageId ORDER BY md.CreateDate DESC, VersionId DESC) Position
	FROM RelevantAuthority ra WITH(NOLOCK)
	LEFT JOIN FileHistory md WITH(NOLOCK)
	ON ra.RelevantAuthorityId = md.RelevantAuthorityId
	LEFT JOIN Stage st WITH(NOLOCK)
	ON md.StageId = st.StageId
	WHERE Valid = 1
	)
	INSERT INTO #FileHistorySummary(RelevantAuthorityId, Yr, [FileName], Success, Stage, UploadedDate)
	SELECT RelevantAuthorityId, Year, [FileName],  Valid, StageName, CreateDate FROM FileHistorySummary md 
	WHERE Position = 1
	ORDER By RelevantAuthorityId, StageName, md.Year

	CREATE TABLE #AllYearsbyRaAndScheme
	(
		YrId INT, 
		RelevantAuthorityId INT, 
		RelevantAuthorityName NVARCHAR(255),
		Stage NVARCHAR(50)
	)
	INSERT INTO #AllYearsbyRaAndScheme (YrId, RelevantAuthorityId, RelevantAuthorityName, Stage)
	SELECT YrId, ra.RelevantAuthorityId, RelevantAuthorityName, StageName
	FROM #Years, RelevantAuthority ra, Stage

	INSERT INTO FileHistorySummary([YearId], RelevantAuthorityId, RelevantAuthorityName, [Stage], FileName, Success, [UploadedDate])
	SELECT YrId,  y.RelevantAuthorityId, y.RelevantAuthorityName, y.Stage, FileName,  Success, UploadedDate
	FROM #AllYearsbyRaAndScheme y
	OUTER APPLY
	(
		SELECT FileName, Success, UploadedDate
		FROM #FileHistorySummary mds
		WHERE mds.Yr = y.YrId
		AND mds.RelevantAuthorityId = y.RelevantAuthorityId
		AND mds.Stage = y.Stage
	) A

	DROP TABLE #Years
	DROP TABLE #AllYearsbyRaAndScheme
	DROP TABLE #FileHistorySummary
END
GO

GRANT EXECUTE ON [dbo].[FileHistorySummaryCreate] TO Budgetviewer AS [dbo]
GO