USE Budget 
GO

DROP PROCEDURE IF EXISTS RelevantAuthorityUpdateById
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	All RA BY Id
-- =============================================
CREATE PROCEDURE RelevantAuthorityUpdateById
	@RelevantAuthorityId INT,
	@RelevantAuthorityName NVARCHAR(255),
	@DepartmentId INT,
	@ContactDetails NVARCHAR(MAX)

AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE RelevantAuthority
	SET DepartmentId = @DepartmentId,
	RelevantAuthorityName = @RelevantAuthorityName,
	ContactDetails = @ContactDetails
	WHERE RelevantAuthorityId = @RelevantAuthorityId
	
END
GO

GRANT EXECUTE ON RelevantAuthorityUpdateById TO budgetviewer 
