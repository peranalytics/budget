USE Budget

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Retired' AND Object_ID = Object_ID(N'Year'))
BEGIN
ALTER TABLE Year
ADD Retired BIT NULL
END

UPDATE Year
SET Retired = 0

SELECT * FROM Year