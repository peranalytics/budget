USE Budget 
GO

DROP PROCEDURE IF EXISTS FileHistoryByVersionId
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	get meta data by version
-- =============================================
CREATE PROCEDURE FileHistoryByVersionId
	@VersionId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	   [VersionId]
      ,[StageYearId]
      ,ra.RelevantAuthorityId AS RelevantAuthorityId
	  ,RelevantAuthorityName
      ,[OriginalFileName] AS [FileName]
      ,md.[ModifyDate]
      ,md.[CreateDate]
      ,[StageName] AS StageName
      ,[Valid] AS Success
      ,ArchivedFileName
	FROM FileHistory md WITH(NOLOCK)
	INNER JOIN RelevantAuthority ra
	ON ra.RelevantAuthorityId = md.RelevantAUthorityId
	INNER JOIN Stage s
	ON s.StageId = md.StageId
	WHERE VersionId = @VersionId
	ORDER BY StageName, StageYearId

END
GO

GRANT EXECUTE ON FileHistoryByVersionId TO Budgetviewer 
