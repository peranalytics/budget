USE Budget 
GO

DROP PROCEDURE IF EXISTS GenerateDebtFlatfile
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE GenerateDebtFlatfile
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE FROM [dbo].[DebtDataFlatFile]

	INSERT INTO [dbo].[DebtDataFlatFile]
           ([VersionId]
           ,[ProviderId]
           ,[ProviderCode]
           ,[ProviderName]
           ,[DebtCategory]
           ,[InputData]
           ,[Identifiers]
           ,[Units]
           ,[StageId]
           ,[Stage]
           ,[StageYearId]
           ,[YearId]
           ,[Amount]
           ,[ModifyDate]
           ,[CreateDate])
    	SELECT 
		  [VersionId]
		  ,bd.[ProviderId]
		  ,[ProviderCode]
		  ,[ProviderName]
		  ,[DebtCategory]
          ,[InputData]
          ,[Identifiers]
          ,[Units]
		  ,bd.[StageId]
		  ,StageName
		  ,[StageYearId]
		  ,[YearId]
		  ,[Amount] 
		  ,GETDATE()
		  ,GETDATE()
	FROM DebtData bd WITH(NOLOCK) 
	INNER JOIN DataProviders dp WITH(NOLOCK)
	ON dp.ProviderId = bd.ProviderId
	INNER JOIN Stage s WITH(NOLOCK)
	ON s.StageId = bd.StageId
END

GO

GRANT EXECUTE ON GenerateDebtFlatfile TO Budgetviewer 
