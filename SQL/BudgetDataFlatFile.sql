USE [Budget]
GO

DROP TABLE IF EXISTS [dbo].[BudgetDataFlatFile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BudgetDataFlatFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BudgetDataFlatFile](
	[VersionId] [int] NOT NULL,
	[ProviderId] [int] NOT NULL,
	[ProviderName] [nvarchar](50) NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[RelevantAuthorityName] [nvarchar](255) NOT NULL,
	[StageId] [int] NOT NULL,
	[StageName] [nvarchar](100) NOT NULL,
	[StageYearId] [int] NOT NULL,
	[EsaCode] [nvarchar](50) NOT NULL,
	[EsaCategoryCode] [nvarchar](50) NOT NULL,
	[YearId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[DataType] [nvarchar](100) NOT NULL,
	[Amount] [decimal](10, 2) NULL
) ON [PRIMARY]
END
GO


