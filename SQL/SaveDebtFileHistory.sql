USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[SaveDebtFileHistory]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 03/02/2021
-- Description:	Saves Debt file history
-- =============================================
CREATE PROCEDURE [dbo].[SaveDebtFileHistory] 
	@OriginalFileName NVARCHAR(255),
	@VersionId INT OUTPUT
AS

BEGIN
	
	SET @VersionId = -1;

	INSERT INTO [dbo].[FileHistory]
			   ([ProviderId]
			   ,[RelevantAuthorityId]
			   ,[StageId]
			   ,[StageYearId]
			   ,[Valid]
			   ,[OriginalFileName]
			   ,[ArchivedFileName]
			   ,[ModifyDate]
			   ,[CreateDate])
	SELECT DISTINCT 
		999999, 
		999999, -- HARDCODED AS DEBT DATA file covers loads of RAs but RA not supplied
		s.StageId, 
		dd.StageYear,
		NULL,
		@OriginalFileName,
		NULL,
		NULL,
		GETDATE()
	FROM StagingDebtData dd
	INNER JOIN DataProviders dp 
	ON dp.ProviderCode = dd.ProviderName
	INNER JOIN Stage s
	ON s.StageName = dd.Stage

	SELECT @VersionId = SCOPE_IDENTITY()

	UPDATE StagingDebtData
	SET VersionId = @VersionId
END
GO

GRANT EXECUTE ON [dbo].[SaveDebtFileHistory] TO [budgetviewer] AS [dbo]
GO


