USE Budget
GO


IF NOT EXISTS (SELECT 1 FROM [dbo].Department WHERE DepartmentId = 999999)
BEGIN
	SET IDENTITY_INSERT dbo.Department ON;  
	INSERT INTO Department(DepartmentId, DepartmentName, ModifyDate, CreateDate, ModifiedBy)
	SELECT 999999, 'Debt Department', GETDATE(), GETDATE(), 1

	SET IDENTITY_INSERT dbo.Department OFF;  
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[RelevantAuthority] WHERE RelevantAuthorityId = 999999)
BEGIN
	SET IDENTITY_INSERT dbo.[RelevantAuthority] ON;  
	
	INSERT INTO [RelevantAuthority](RelevantAuthorityId, DepartmentId, RelevantAuthorityName, ModifyDate, CreateDate, ModifiedBy, ContactDetails)
	SELECT 999999, 999999, 'Debt Bodies', GETDATE(), GETDATE(), 1, 'N/A'

	SET IDENTITY_INSERT dbo.[RelevantAuthority] OFF;  

END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'Economic')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 90, 'Economic', 'Debt', GETDATE(), GETDATE(), 1, 'Economic'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'Fiscal')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 91, 'Fiscal', 'Debt', GETDATE(), GETDATE(), 1, 'Fiscal'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'HFA Survey')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 92, 'HFA Survey', 'Debt', GETDATE(), GETDATE(), 1, 'HFA Survey'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'Westlink')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 93, 'Westlink', 'Debt', GETDATE(), GETDATE(), 1, 'Westlink'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'HBFI')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 94, 'HBFI', 'Home Building Finance Ireland', GETDATE(), GETDATE(), 1, 'HBFI'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'LDA')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 95, 'LDA', 'Land Development Agency', GETDATE(), GETDATE(), 1, 'LDA'
END

IF NOT EXISTS (SELECT 1 FROM [dbo].DataProviders WHERE ProviderCode = 'DebtProviders')
BEGIN
	
	INSERT INTO DataProviders(ProviderId,  ProviderName, Format, ModifyDate, CreateDate, ModifiedBy, ProviderCode)
	SELECT 999999, 'DebtProviders', 'All Debt Providers', GETDATE(), GETDATE(), 1, 'DebtProviders'
END