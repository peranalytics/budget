USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [EsaCodes]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EsaCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[EsaCodes](
	[EsaCode] [nvarchar](50) NOT NULL,
	[EsaCodeDescription] [nvarchar](255) NOT NULL,
	EsaCodeCategory [nvarchar](255) NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_EsaCode] PRIMARY KEY CLUSTERED 
(
	[EsaCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EsaCodes_ModifiedBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[EsaCodes]'))
ALTER TABLE [dbo].[EsaCodes]  WITH CHECK ADD  CONSTRAINT [FK_EsaCodes_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [Security].[Users] (UserId)
GO


