USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE ExportFileHistory

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFileHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[ExportFileHistory](
	ExportFileId INT IDENTITY (1,1) NOT NULL,
	ExportId INT NOT NULL,
	ExportFileName NVARCHAR(300) NOT NULL,
	[ExportDate] DATETIME NULL
	)
END
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExportFileHistory_ExportId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExportFileHistory]'))
ALTER TABLE [dbo].[ExportFileHistory]  WITH CHECK ADD  CONSTRAINT [FK_ExportFileHistory_ExportId] FOREIGN KEY([ExportId])
REFERENCES [dbo].[ExportQueue] (ExportId)
GO




