USE Budget 
GO

DROP PROCEDURE IF EXISTS Report.RelevantAuthorityGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE Report.RelevantAuthorityGetAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		RelevantAuthorityId,
		RelevantAuthorityName
	FROM RelevantAuthority
END

GO

GRANT EXECUTE ON Report.RelevantAuthorityGetAll TO Budgetviewer 
