USE Budget

DELETE FROM Security.UserReports
DELETE FROM Security.ReportUsage

DELETE FROM Report.Reports

INSERT INTO [Report].[Reports]
           ([ReportName]
           ,[ReportDescription]
           ,[ReportUrl]
           ,[Deleted]
           ,[ModifyDate]
		   ,Category)
SELECT 'Data by Esa Code', 'Gets aggregated data by Esa Code and Esa Code description - use the parameters provided to filter the data ', '/BudgetReports/BudgetDataByEsaCode', 0, GETDATE(), 'Summary' UNION ALL
SELECT 'Data by Esa Code and Year', 'Gets aggregated data by Esa Code and Esa Code description and Year - use the parameters provided to filter the data', '/BudgetReports/BudgetDataEsaCodebyYear', 0, GETDATE(), 'Summary' UNION ALL
SELECT 'Data by Body, EsaCode and Year', 'Gets aggregated data by Esa Code and Esa Code description - use the parameters provided to filter the data', '/BudgetReports/RABudgetDataEsaCodebyYear', 0, GETDATE(), 'Summary' UNION ALL
SELECT 'Variance of data by Stage', 'Gets aggregated data by Esa Code and and compares to the previous stage - use the parameters provided to filter the data', '/BudgetReports/StageVariance', 0, GETDATE(), 'Variance'

SELECT 'Exceptions Report','Gets Aggregated data for Year-on-Year data - use the parameters provided to filter the data','/BudgetReports/ExceptionsReport',  0, GETDATE(), 'Summary' UNION ALL
SELECT 'Forecast Horizon Data by Provider','Gets aggregated data over forecast horizon by provider - use the parameters provided to filter the data','/BudgetReports/ForecastHorizonDataByProvider', 0, GETDATE(), 'Summary' 


SELECT * FROM Report.Reports
SELECT * FROM Security.UserReports

INSERT INTO Security.UserReports
SELECT UserId, ReportId, GETDATE() FROM Security.Users, Report.Reports



          
