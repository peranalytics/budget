USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE BudgetData

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BudgetData]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[BudgetData](
	[VersionId] INT NOT NULL,
	[ProviderId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[StageId] [int] NOT NULL,
	[StageYearId] [int] NOT NULL,
	[EsaCode] NVARCHAR(50) NOT NULL,
	[EsaCategoryCode] NVARCHAR(50) NOT NULL,
	[YearId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[Amount] DECIMAL(10,2) NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL
	)
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_ProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_ProviderId] FOREIGN KEY([ProviderId])
REFERENCES [dbo].[DataProviders] (ProviderId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_EsaCode]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_EsaCode] FOREIGN KEY([EsaCode])
REFERENCES [dbo].[EsaCodes] (EsaCode)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_YearId] FOREIGN KEY([YearId])
REFERENCES [dbo].[Year] (YearId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_RelevantAuthorityId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_RelevantAuthorityId] FOREIGN KEY(RelevantAuthorityId)
REFERENCES [dbo].[RelevantAuthority] (RelevantAuthorityId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_VersionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_VersionId] FOREIGN KEY(VersionId)
REFERENCES [dbo].[FileHistory] (VersionId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_StageIdId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_StageId] FOREIGN KEY(StageId)
REFERENCES [dbo].[Stage] (StageId)
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BudgetData_DataTypeId]') AND parent_object_id = OBJECT_ID(N'[dbo].[BudgetData]'))
ALTER TABLE [dbo].[BudgetData]  WITH CHECK ADD  CONSTRAINT [FK_BudgetData_DataTypeId] FOREIGN KEY([DataTypeId])
REFERENCES [dbo].[DataType] ([DataTypeId])
GO


