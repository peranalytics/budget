USE Budget 
GO

DROP PROCEDURE IF EXISTS Report.GetDebtData
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tracy
-- Create date: 08/03/2021
-- Description:	Loads DD
-- =============================================
CREATE PROCEDURE Report.GetDebtData
	@StageYear NVARCHAR(MAX) = '-1',
	@StageId NVARCHAR(MAX) = '-1'

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT CAST(VALUE AS INT) AS StageYearId
	INTO #StageYear FROM STRING_SPLIT(@StageYear, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS StageId
	INTO #Stage FROM STRING_SPLIT(@StageId, ',')  
	WHERE RTRIM(VALUE) <> '';

CREATE TABLE #TempTable 
	(Seq INT, 
	ProviderId INT,
	ProviderCode NVARCHAR(20),
	StageId INT,
	Stage NVARCHAR(10), 
	StageYearId INT,
	Identifiers NVARCHAR(20), 
	GenGovDebtForecast NVARCHAR(80),
	Year1Amt Decimal(10,2),			--2019	t-2
	Year2Amt Decimal(10,2),			--2020	t-1
	Year3Amt Decimal(10,2),			--2021	t
	Year4Amt Decimal(10,2),			--2022	t+1
	Year5Amt Decimal(10,2),			--2023	t+2
	Year6Amt Decimal(10,2),			--2024	t+3
	Year7Amt Decimal(10,2)			--2025	t+4
	)

 -- Insert lines into #TempTable & Categorise by Identifier, Description & Year (t-2 to t+4) 
INSERT INTO #TempTable
 SELECT
	CASE WHEN Identifiers = 'FV-002' THEN 0
		 WHEN Identifiers = 'NTMA-ND-002' THEN 3	 --**
		 WHEN Identifiers IN('NTMA-DFIN-001','FV-010') THEN 4							 --USES FV-010
		 WHEN Identifiers = 'NTMA-ND-003'THEN 5			--**
		 WHEN Identifiers = 'NTMA-ND-004' THEN 6
		 WHEN Identifiers IN('NTMA-DC-001','NTMA-DC-002') THEN 7   --
		 WHEN Identifiers IN ('NTMA-DC-003','CSO-012') THEN 8					--CSO-012 used for t-1 calculation only
		 WHEN Identifiers IN('NTMA-DC-004','NTMA-DC-005','CSO-009') THEN 9		--CSO-009 used for t-1 calculation only
		 WHEN Identifiers = 'NTMA-DC-006' THEN 10
		 WHEN Identifiers IN('NTMA-DC-007','CSO-003') THEN 11					--CSO-003 used for t-1 calculation only			
		 WHEN Identifiers IN('NTMA-DC-008','CSO-006') THEN 12					--CSO-006 used for t-1 calculation only
		 WHEN Identifiers IN('NTMA-DC-009','CSO-005') THEN 13					--CSO-005 used for t-1 calculation only
		 WHEN Identifiers IN('NTMA-DC-010','CSO-007') THEN 14					--CSO-007 used for t-1 calculation only
		 WHEN Identifiers = 'NTMA-ND-010' THEN 15
		 WHEN Identifiers = 'IBRC-DL-011' THEN 16
		 WHEN Identifiers IN ('HFA-DL-003','HFA-DL-005') THEN 17
		 WHEN Identifiers = 'SBCI-DL-003' THEN 18
		 WHEN Identifiers IN ('LA-001', 'CSO-020') THEN 19					--CSO-020 used for T-1 calculation only
		 WHEN Identifiers IN('NMPC-001','CSO-025','West-001') THEN 20		--CSO-025,West-001 used for t-1 calculation only / also SBCI-DL-003 added in union insert *&*
		 WHEN Identifiers = 'CSO-021' THEN 21								
		 WHEN Identifiers = 'ECO-001' THEN 23
		 WHEN Identifiers = 'ECO-003' THEN 24		
	END,
	ProviderId,
	ProviderCode,
	StageId,
	Stage,
	StageYearId,
	Identifiers, 
	CASE WHEN Identifiers = 'FV-002' THEN 'General Government (previous year outturn)'
		 WHEN Identifiers = 'NTMA-ND-002' THEN 'Exchequer Cash/CSRA/Deposits' 		 
		 WHEN Identifiers IN('NTMA-DFIN-001','FV-010') THEN 'Adjustment to Exchequer Borrowing Requirement (EBR)'				  --USES FV-010
		 WHEN Identifiers = 'NTMA-ND-003' THEN 'HFA Guaranteed Notes'
		 WHEN Identifiers = 'NTMA-ND-004' THEN 'Collateral Funding'
		 WHEN Identifiers IN('NTMA-DC-001','NTMA-DC-002') THEN 'Exchequer Borrowing from POSBF Bonds'
		 WHEN Identifiers IN('NTMA-DC-003','CSO-012') THEN 'POSB Liabilities to Depositors'
		 WHEN Identifiers IN('NTMA-DC-004','NTMA-DC-005','CSO-009') THEN 'Exchequer Borrowing from SPEM Account & other Government Bodies'
		 WHEN Identifiers = 'NTMA-DC-006' THEN 'Short Term Paper Discount from Nominal'
		 WHEN Identifiers IN('NTMA-DC-007','CSO-003') THEN 'Small Savings Accrual'
		 WHEN Identifiers IN('NTMA-DC-008','CSO-006') THEN 'Collateral Cash Held by NTMA'
		 WHEN Identifiers IN('NTMA-DC-009','CSO-005') THEN 'National Loans Advance Interest Cash Balance'
		 WHEN Identifiers IN('NTMA-DC-010','CSO-007') THEN 'Net Repo Adjustment'
		 WHEN Identifiers = 'NTMA-ND-010' THEN 'Other'
		 WHEN Identifiers = 'IBRC-DL-011' THEN 'IBRC'
		 WHEN Identifiers IN('HFA-DL-003','HFA-DL-005') THEN 'HFA'
		 WHEN Identifiers = 'SBCI-DL-003' THEN 'SBCI'
		 WHEN Identifiers IN ('LA-001','CSO-020') THEN 'Change in Local Authorities'
		 WHEN Identifiers IN('NMPC-001','CSO-025','West-001') THEN 'Change in NMPC less HFA,SBCI'
		 WHEN Identifiers =  'CSO-021' THEN 'Current Year Forecast'
		 WHEN Identifiers = 'ECO-001' THEN 'GDP (Nominal)'
		 WHEN Identifiers = 'ECO-003' THEN 'GNI* (Nominal)'
	END, 
	SUM(IIF(YearId =(StageYearId-2),Amount,0)),
	SUM(IIF(YearId=(StageYearId-1),Amount,0)),
	SUM(IIF(YearId=StageYearId,Amount,0)),
	SUM(IIF(YearId=(StageYearId+1),Amount,0)),
	SUM(IIF(YearId=(StageYearId+2),Amount,0)),
	SUM(IIF(YearId=(StageYearId+3),Amount,0)),
	SUM(IIF(YearId=(StageYearId+4),Amount,0))  

 FROM DebtDataFlatFile
 GROUP BY ProviderId,ProviderCode,StageId,Stage,StageYearId,YearId,Identifiers
 

 -- IDENTIFIER 'FV-010' USED IN CALCULATIONS FOR 2 DIFFERENT CATEGORIES -- 
 INSERT INTO #TempTable
 SELECT 1,ProviderId,ProviderCode,StageId,Stage,StageYearId,Identifiers,'Exchequer Borrowing Requirement (EBR)',
	SUM(IIF(YearId =(StageYearId-2),Amount,0)),
	SUM(IIF(YearId=(StageYearId-1),Amount,0)),
	SUM(IIF(YearId=StageYearId,Amount,0)),
	SUM(IIF(YearId=(StageYearId+1),Amount,0)),
	SUM(IIF(YearId=(StageYearId+2),Amount,0)),
	SUM(IIF(YearId=(StageYearId+3),Amount,0)),
	SUM(IIF(YearId=(StageYearId+4),Amount,0))  
 FROM DebtDataFlatFile
 WHERE Identifiers = 'FV-010'
 GROUP BY ProviderId,ProviderCode,StageId,Stage,StageYearId,YearId,Identifiers


 -- INSERT DETAIL IN REPORT FOR "IN YEAR CHANGES TO THE GENERAL GOVERNMENT DEBT COMPONENTS", WITH NO TOTALS
 INSERT INTO #TempTable
 SELECT DISTINCT
	2,91,'Fiscal',StageId,Stage,StageYearId,'N/A','In year changes to the general government debt components',
	NULL,NULL,NULL,NULL,NULL,NULL,NULL
	FROM DebtDataFlatFile


-- INSERT ENTRIES FOR IDENTIFIERS ('NTMA-DC-001','NTMA-DC-002','NTMA-DC-003') TO BE USED FOR 2ND CATEGORY FOR T-1 YEAR CALCULATION
INSERT INTO #TempTable
 SELECT 9,ProviderId,ProviderCode,StageId,Stage,StageYearId,Identifiers,'Exchequer Borrowing from SPEM Account & other Government Bodies',
	IIF(YearId=StageYearId-2,SUM(Amount),0),
	IIF(YearId=StageYearId-1,SUM(Amount),0),
	NULL,NULL,NULL,NULL,NULL	
 FROM DebtDataFlatFile
 WHERE Identifiers IN ('NTMA-DC-003','NTMA-DC-001','NTMA-DC-002','CSO-012') 
 GROUP BY ProviderId,ProviderCode,StageId,Stage,StageYearId,YearId,Identifiers


 -- INSERT ADDITIONAL IDENTIFIERS TO BE USED ALSO FOR 'Change in NMPC less HFA,SBCI'
 INSERT INTO #TempTable
 SELECT 20,1,'NMPC',StageId,Stage,StageYearId,Identifiers,'Change in NMPC less HFA,SBCI',
	SUM(IIF(YearId =(StageYearId-2),Amount,0)),
	SUM(IIF(YearId=(StageYearId-1),Amount,0)),
	NULL, NULL, NULL, NULL, NULL		
 FROM DebtDataFlatFile
 WHERE Identifiers = 'SBCI-DL-003' 
 GROUP BY ProviderId,ProviderCode,StageId,Stage,StageYearId,YearId,Identifiers
 

-- SELECT FROM #TempTable & Calculate for new years within scope
 SELECT 
	Seq,
	ProviderId,
	ProviderCode,
	StageId	,
	Stage,
	StageYearId,
	Identifiers,
	GenGovDebtForecast,
	CASE WHEN Seq = 0 THEN SUM(Year1Amt)
		 WHEN Seq IN (3,5,6,10,16,18) THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 8 AND Identifiers = 'CSO-012' THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 11 AND Identifiers = 'CSO-003' THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 12 AND Identifiers = 'CSO-006' THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 13 AND Identifiers = 'CSO-005' THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 14 AND Identifiers = 'CSO-007' THEN SUM(Year2Amt - Year1Amt)
		 WHEN Seq = 1 THEN SUM(Year2Amt)*-1
		 WHEN Seq = 2 THEN NULL 
	     WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year2Amt,0)) - SUM(IIF(Identifiers='FV-010',Year2Amt,0))) *-1
		 WHEN Seq IN (7,17) THEN SUM(Year2Amt) - SUM(Year1Amt)

-- **Seq 9 - New formula for 1st year (t-1) : Exchequer Borrowing from SPEM Account & other Government Bodies **
/*		 WHEN Seq = 9 THEN (0-(SUM(IIF(Identifiers ='CSO-009',Year2Amt - Year1Amt,0))) - 
					(SUM(IIF(Identifiers IN ('NTMA-DC-002','NTMA-DC-001'),Year2Amt - Year1Amt,0))) -
					 (SUM(IIF(Identifiers ='NTMA-DC-003',Year2Amt - Year1Amt,0))) - 
					 (SUM(IIF(Identifiers ='CSO-012',Year2Amt - Year1Amt,0)))	)
*/
		WHEN Seq = 9 THEN IIF(Identifiers ='CSO-009',SUM(Year2Amt - Year1Amt),0)*-1 -
						  IIF(Identifiers IN ('NTMA-DC-002','NTMA-DC-001'),SUM(Year2Amt - Year1Amt),0) -		  
	--					  IIF(Identifiers ='NTMA-DC-003',SUM(Year2Amt - Year1Amt),0) -						  
						  IIF(Identifiers ='CSO-012',SUM(Year2Amt - Year1Amt),0)						

		 WHEN Seq IN (15,23,24,21) THEN SUM(Year2Amt)
		 when seq = 20 Then IIF(Identifiers = 'CSO-025',SUM(Year2Amt - Year1Amt),0) - 
							IIF(Identifiers = 'SBCI-DL-003',SUM(Year2Amt - Year1Amt),0) - 
							IIF(Identifiers = 'West-001',SUM(Year2Amt),0)
		 WHEN Seq = 19 AND Identifiers = 'CSO-020' THEN SUM(Year2Amt - Year1Amt)
 END AS AmtYr1,	--2020
	CASE WHEN Seq IN (3,5,6,10,16,18) THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 8 AND NOT Identifiers = 'CSO-012' THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 11 AND Identifiers = 'NTMA-DC-007' THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 12 AND Identifiers = 'NTMA-DC-008' THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 13 AND Identifiers = 'NTMA-DC-009' THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 14 AND Identifiers = 'NTMA-DC-010' THEN SUM(Year3Amt - Year2Amt)
		 WHEN Seq = 1 THEN SUM(Year3Amt)*-1
		 WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year3Amt,0)) - SUM(IIF(Identifiers='FV-010',Year3Amt,0))) *-1
		 WHEN Seq IN (7,17) THEN SUM(Year3Amt) - SUM(Year2Amt)
		 WHEN Seq = 9 AND Identifiers IN ('NTMA-DC-004','NTMA-DC-005') THEN SUM(Year3Amt) - SUM(Year2Amt)  -- **
		 WHEN Seq IN (15,23,24) THEN SUM(Year3Amt)
		 WHEN Seq = 20 AND Identifiers = 'NMPC-001' THEN SUM(Year3Amt)
		 WHEN Seq = 19 AND Identifiers = 'LA-001' THEN SUM(Year3Amt)*-1
 END AS AmtYr2,	--2021
	CASE WHEN Seq IN (3,5,6,10,16,18) THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 8 AND NOT Identifiers = 'CSO-012' THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 11 AND Identifiers = 'NTMA-DC-007' THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 12 AND Identifiers = 'NTMA-DC-008' THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 13 AND Identifiers = 'NTMA-DC-009' THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 14 AND Identifiers = 'NTMA-DC-010' THEN SUM(Year4Amt - Year3Amt)
		 WHEN Seq = 1 THEN SUM(Year4Amt)*-1
		 WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year4Amt,0)) - SUM(IIF(Identifiers='FV-010',Year4Amt,0)))*-1
		 WHEN Seq  IN (7,17) THEN SUM(Year4Amt) - SUM(Year3Amt)
		 WHEN Seq = 9 AND Identifiers IN ('NTMA-DC-004','NTMA-DC-005') THEN (SUM(Year4Amt) - SUM(Year3Amt))
		 WHEN Seq = 19 AND Identifiers = 'LA-001' THEN SUM(Year4Amt)*-1
		 WHEN Seq IN (15,23,24) THEN SUM(Year4Amt)
		 WHEN Seq = 20 AND Identifiers = 'NMPC-001' THEN SUM(Year4Amt)
 END AS AmtYr3,	--2022
	CASE WHEN Seq IN (3,5,6,10,16,18) THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 8 AND NOT Identifiers = 'CSO-012' THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 11 AND Identifiers = 'NTMA-DC-007' THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 12 AND Identifiers = 'NTMA-DC-008' THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 13 AND Identifiers = 'NTMA-DC-009' THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 14 AND Identifiers = 'NTMA-DC-010' THEN SUM(Year5Amt - Year4Amt)
		 WHEN Seq = 1 THEN SUM(Year5Amt)*-1
	     WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year5Amt,0)) - SUM(IIF(Identifiers='FV-010',Year5Amt,0)))*-1
		 WHEN Seq IN (7,17) THEN SUM(Year5Amt) - SUM(Year4Amt)
		 WHEN Seq = 9 AND Identifiers IN ('NTMA-DC-004','NTMA-DC-005') THEN SUM(Year5Amt) - SUM(Year4Amt) 
		 WHEN Seq = 19 AND Identifiers = 'LA-001' THEN SUM(Year5Amt)*-1
		 WHEN Seq IN (15,23,24) THEN SUM(Year5Amt)
		 WHEN Seq = 20 AND Identifiers = 'NMPC-001' THEN SUM(Year5Amt)
 END AS AmtYr4,	--2023
	CASE WHEN Seq IN (3,5,6,10,14,16,18) THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 8 AND NOT Identifiers = 'CSO-012' THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 11 AND Identifiers = 'NTMA-DC-007' THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 12 AND Identifiers = 'NTMA-DC-008' THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 13 AND Identifiers = 'NTMA-DC-009' THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 14 AND Identifiers = 'NTMA-DC-010' THEN SUM(Year6Amt - Year5Amt)
		 WHEN Seq = 1 THEN SUM(Year6Amt)*-1
	     WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year6Amt,0)) - SUM(IIF(Identifiers='FV-010',Year6Amt,0)))*-1	
		 WHEN Seq IN (7,17) THEN SUM(Year6Amt) - SUM(Year5Amt)
		 WHEN Seq = 9 AND Identifiers IN ('NTMA-DC-004','NTMA-DC-005') THEN SUM(Year6Amt) - SUM(Year5Amt) 
		 WHEN Seq = 19 AND Identifiers = 'LA-001' THEN SUM(Year6Amt)*-1
		 WHEN Seq  IN (15,23,24) THEN SUM(Year6Amt)
		 WHEN Seq = 20 AND Identifiers = 'NMPC-001' THEN SUM(Year6Amt)
 END AS AmtYr5,	--2024
	CASE WHEN Seq IN (3,5,6,10,14,16,18) THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 8 AND NOT Identifiers = 'CSO-012' THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 11 AND Identifiers = 'NTMA-DC-007' THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 12 AND Identifiers = 'NTMA-DC-008' THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 13 AND Identifiers = 'NTMA-DC-009' THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 14 AND Identifiers = 'NTMA-DC-010' THEN SUM(Year7Amt - Year6Amt)
		 WHEN Seq = 1 THEN SUM(Year7Amt)*-1	
 	     WHEN Seq = 4 THEN (SUM(IIF(Identifiers='NTMA-DFIN-001',Year7Amt,0)) - SUM(IIF(Identifiers='FV-010',Year7Amt,0)))*-1
		 WHEN Seq IN (7,17) THEN SUM(Year7Amt) - SUM(Year6Amt)
		 WHEN Seq = 9 AND Identifiers IN ('NTMA-DC-004','NTMA-DC-005') THEN SUM(Year7Amt) - SUM(Year6Amt) 
		 WHEN Seq = 19 AND Identifiers = 'LA-001' THEN SUM(Year7Amt)*-1
		 WHEN Seq IN (15,23,24) THEN SUM(Year7Amt)
		 WHEN Seq = 20 AND Identifiers = 'NMPC-001' THEN SUM(Year7Amt)
 END AS AmtYr6	--2025

  -- No calculations required for Year 7  
 FROM #TempTable 
	WHERE (@StageYear = '-1' OR [StageYearId] IN (SELECT sy.StageYearId FROM #StageYear AS sy))
		AND (@StageId = '-1'OR [StageId] IN (SELECT st.StageId FROM #Stage AS st))
		AND Seq BETWEEN 0 AND 25
 GROUP BY Seq,ProviderId,ProviderCode,StageId,Stage,StageYearId,GenGovDebtForecast, Identifiers
 

	DROP TABLE #TempTable
	DROP TABLE #StageYear
	DROP TABLE #Stage



END

GO

GRANT EXECUTE ON Report.GetDebtData TO Budgetviewer 
