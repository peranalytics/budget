USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[ESACodeGetAll]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Get the Relevant Authories
-- =============================================
CREATE PROCEDURE [dbo].[ESACodeGetAll] 
	@Page INT = 1,
	@PageSize INT = 10,
	@FilterBy NVARCHAR(100) = '',
	@TotalEsaCount INT OUTPUT
AS

BEGIN
	SET @TotalEsaCount = (SELECT COUNT([EsaCodeId]) FROM EsaCodes WHERE EsaCode LIKE '%' + @FilterBy + '%')

	;WITH PagedEsaCode AS
	(
		SELECT 
		 [EsaCodeId],[EsaCode],[EsaCodeDescription],[EsaCodeCategory],[ModifyDate],[CreateDate],[ModifiedBy]
		  ,ROW_NUMBER() OVER(ORDER BY EsaCode) AS rowNumber
		 FROM EsaCodes
		WHERE EsaCode LIKE '%' + @FilterBy + '%'
	)
	SELECT 
		[EsaCodeId],[EsaCode],[EsaCodeDescription],[EsaCodeCategory],[ModifyDate],[CreateDate],[ModifiedBy], rowNumber
	FROM PagedEsaCode
	WHERE rowNumber BETWEEN ((@Page - 1) * @PageSize) + 1 AND (@Page * @PageSize) 
END
GO

GRANT EXECUTE ON [dbo].[ESACodeGetAll] TO [budgetviewer] AS [dbo]
GO


