USE Budget 
GO

DROP PROCEDURE IF EXISTS Report.RAswithDataProvider
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE Report.RAswithDataProvider
--	@RelevantAuthorityId NVARCHAR(MAX) = '-1',
	@ProviderId NVARCHAR(MAX) = '-1'

AS
BEGIN
	
	SET NOCOUNT ON;

/*
	SELECT CAST(VALUE AS INT) AS RelevantAuthorityId
	INTO #RelevantAuthority FROM STRING_SPLIT(@RelevantAuthorityId, ',')  
	WHERE RTRIM(VALUE) <> '';
*/

	SELECT CAST(VALUE AS INT) AS ProviderId
	INTO #Provider FROM STRING_SPLIT(@ProviderId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT 
		   s.[ProviderId]
		  ,dp.[ProviderName]
		  ,s.RelevantAuthorityId
		  ,ra.[RelevantAuthorityName]
		  ,ra.ContactDetails
		  
	FROM Surveys s WITH(NOLOCK) 
	INNER JOIN RelevantAuthority ra WITH(NOLOCK) ON s.RelevantAuthorityId = ra.RelevantAuthorityId
	INNER JOIN DataProviders dp WITH(NOLOCK) ON s.ProviderId = dp.ProviderId
	WHERE 
--    (@RelevantAuthorityId = '-1' OR s.[RelevantAuthorityId] IN (SELECT ra.RelevantAuthorityId FROM #RelevantAuthority AS ra)) AND
	(@ProviderId = '-1' OR s.[ProviderId] IN (SELECT pr.ProviderId FROM #Provider AS pr))	 

--	DROP TABLE #RelevantAuthority
	DROP TABLE #Provider

END

GO

GRANT EXECUTE ON Report.RAswithDataProvider TO Budgetviewer 
