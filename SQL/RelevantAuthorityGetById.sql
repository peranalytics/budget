USE Budget 
GO

DROP PROCEDURE IF EXISTS RelevantAuthorityGetById
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	All RA BY Id
-- =============================================
CREATE PROCEDURE RelevantAuthorityGetById
	@RelevantAuthorityId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		ra.RelevantAuthorityId, 
		ra.RelevantAuthorityName,
		d.[DepartmentId], 
		DepartmentName,
		ContactDetails
	FROM RelevantAuthority ra WITH(NOLOCK)
	INNER JOIN Department d WITH(NOLOCK)
	ON d.DepartmentId = ra.DepartmentId
	WHERE ra.RelevantAuthorityId = @RelevantAuthorityId
	
END
GO

GRANT EXECUTE ON RelevantAuthorityGetById TO budgetviewer 
