USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportHistoryGetById
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportHistoryGetById
	@ExportId INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT 
		ExportFileId,
		ExportId,
		ExportFileName,
		ExportDate
	FROM ExportFileHistory
	WHERE ExportId = @ExportId
	ORDER By ExportFileName
END

GO

GRANT EXECUTE ON ExportHistoryGetById TO Budgetviewer 
