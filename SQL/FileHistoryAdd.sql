USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[FileHistoryAdd]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Registers the user
-- =============================================
CREATE PROCEDURE [dbo].[FileHistoryAdd] 
	@ProviderId INT,
	@RelevantAuthorityId INT,
	@Stage NVARCHAR(100),
	@StageYearId INT,
	@OriginalFileName NVARCHAR(255),
	@VersionId INT OUTPUT
AS

BEGIN

	DECLARE @StageId INT
	SELECT @StageId = StageId FROM Stage WHERE StageName = @Stage

	SET @VersionId = -1;

	INSERT INTO [dbo].[FileHistory]
			   ([ProviderId]
			   ,[RelevantAuthorityId]
			   ,[StageId]
			   ,[StageYearId]
			   ,[Valid]
			   ,[OriginalFileName]
			   ,[ArchivedFileName]
			   ,[ModifyDate]
			   ,[CreateDate])
	SELECT
		@ProviderId, 
		@RelevantAuthorityId,
		@StageId,
		@StageYearId,
		NULL,
		@OriginalFileName,
		NULL,
		NULL,
		GETDATE()

	SELECT @VersionId = SCOPE_IDENTITY()


END
GO

GRANT EXECUTE ON [dbo].[FileHistoryAdd] TO [budgetviewer] AS [dbo]
GO


