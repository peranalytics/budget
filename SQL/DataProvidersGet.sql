USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[DataProvidersGet]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Registers the user
-- =============================================
CREATE PROCEDURE [dbo].[DataProvidersGet] 
AS

BEGIN

	SELECT ProviderId, ProviderName FROM DataProviders

END
GO

GRANT EXECUTE ON [dbo].[DataProvidersGet] TO [budgetviewer] AS [dbo]
GO


