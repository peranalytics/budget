USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[YearsGetAllByStageYear]
GO

-- =============================================
-- Author:		Tracy
-- Create date: 27/01/2023
-- Description:	Get the Years by Stage Year
-- =============================================
CREATE PROCEDURE [dbo].[YearsGetAllByStageYear] 
@StageYear INT

AS	
BEGIN

	SELECT DISTINCT
		YearId 
	FROM BudgetDataFlatFile
    WHERE @StageYear = StageYearId

END
GO

GRANT EXECUTE ON [dbo].[YearsGetAllByStageYear] TO [budgetviewer] AS [dbo]
GO



