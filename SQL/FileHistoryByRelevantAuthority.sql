USE Budget 
GO

DROP PROCEDURE IF EXISTS FileHistoryByRelevantAuthority
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all member Status
-- =============================================
CREATE PROCEDURE FileHistoryByRelevantAuthority
	@RelevantAuthorityId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
	   [VersionId]
      ,[StageYearId]
      ,RA.RelevantAuthorityId AS RelevantAuthorityId
	  ,RelevantAuthorityName
      ,OriginalFileName AS FileName
      ,'Unknown' AS [UploadedBy]
      ,md.[ModifyDate]
      ,md.[CreateDate]
      ,[StageName]
      ,md.[Valid] AS Success
      ,ArchivedFileName
	FROM FileHistory md WITH(NOLOCK)
	INNER JOIN RelevantAuthority ra
	ON ra.RelevantAuthorityId = md.RelevantAUthorityId
	INNER JOIN Stage s
	ON s.StageId = md.StageId
	WHERE ra.RelevantAuthorityId = @RelevantAuthorityId
	AND [Valid] = 1
	ORDER BY [StageName], StageYearId

END
GO

GRANT EXECUTE ON FileHistoryByRelevantAuthority TO BudgetViewer 
