USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[DepartmentGetAll]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Get the Relevant Authories
-- =============================================
CREATE PROCEDURE [dbo].[DepartmentGetAll] 
	
AS

BEGIN
	SELECT
		DepartmentId,
		DepartmentName 
	FROM Department
	ORDER BY DepartmentName
END
GO

GRANT EXECUTE ON [dbo].[DepartmentGetAll] TO [budgetviewer] AS [dbo]
GO


