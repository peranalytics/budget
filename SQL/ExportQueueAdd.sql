USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportQueueAdd
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportQueueAdd
	@StageId INT,
	@YearId INT,
	@UserId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	UPDATE ExportQueue
	SET Active = 0,
	ModifyDate = GETDATE()
	

	INSERT INTO [dbo].[ExportQueue]
           ([StageId]
           ,[YearId]
           ,[UserId]
           ,[Active]
           ,[Exported]
           ,[ExportDate]
           ,[ModifyDate]
           ,[CreateDate])
     SELECT 
		@StageId,
		@YearId,
		@UserId,
		1,
		0,
		NULL,
		GETDATE(),
		GETDATE()





END

GO

GRANT EXECUTE ON ExportQueueAdd TO Budgetviewer 
