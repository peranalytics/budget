
USE [Budget]
GO

DROP PROCEDURE IF EXISTS [dbo].[FileHistorySummaryGet]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Summary for all member Status
-- =============================================
CREATE PROCEDURE [dbo].[FileHistorySummaryGet]
	@StartYear INT = 2019,
	@RelevantAuthorityId INT = -1
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @StartYear = YEAR(GETDATE()) - 3
	
	CREATE TABLE #YearList
	(
		YearId INT
	)

	;WITH yearlist AS
	(
		SELECT  @StartYear AS YearId
		UNION ALL
		SELECT yl.YearId + 1 AS YearId
		FROM yearlist yl
		WHERE yl.YearId + 1 <= YEAR(GetDate()) + 5
	)
	INSERT INTO #YearList(YearId)
	SELECT YearId FROM yearlist

	; WITH FileHistoryMd AS (
		SELECT mds.YearId, mds.RelevantAuthorityId, RelevantAuthorityName, Stage, FileName, mds.UploadedDate, Success,  RANK() over (partition by mds.RelevantAuthorityId, mds.YearId, mds.Stage order by mds.UploadedDate desc) AS rankedHistory
		FROM FileHistorySummary mds WITH(NOLOCK)
		INNER JOIN #YearList yl
		ON yl.YearId = mds.YearId
		WHERE
		(Success = 1 OR Success IS NULL)
		AND (@RelevantAuthorityId = -1 OR mds.RelevantAuthorityId = @RelevantAuthorityId)
	)

	SELECT YearId, RelevantAuthorityId, RelevantAuthorityName, Stage, FileName, UploadedDate, Success
	FROM FileHistoryMd
	WHERE rankedHistory = 1
	ORDER BY RelevantAuthorityId, Stage, YearId DESC

END

GO

GRANT EXECUTE ON [dbo].[FileHistorySummaryGet] TO budgetviewer AS [dbo]
GO


