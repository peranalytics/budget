USE Budget 
GO

DROP PROCEDURE IF EXISTS StageYearManageAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE StageYearManageAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT DISTINCT
		y.YearId
	FROM Year y
	
END

GO

GRANT EXECUTE ON StageYearManageAll TO Budgetviewer 
