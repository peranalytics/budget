/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Budget]
GO

DROP PROCEDURE [Report].[GetVarianceBudgetData]
GO

/****** Object:  StoredProcedure [Report].[GetVarianceBudgetData]    Script Date: 06/02/2020 15:52:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:		Tracy
-- Create date: 01/11/2019
-- Description:	Loads Budget Data for 'Variance of Data by Stage' Report
-- =====================================================================
CREATE PROCEDURE [Report].[GetVarianceBudgetData]
	@InitialStage NVARCHAR(MAX) = '-1',
	@FinalStage NVARCHAR(MAX) = '-1',
	@RelevantAuthorityId NVARCHAR(MAX) = '-1',
	@ProviderId NVARCHAR(MAX) = '-1',
	@DataTypeId NVARCHAR(MAX) = '-1',
	@YearID NVARCHAR(MAX) = '-1'
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT VALUE AS StageNameYear
	INTO #InitialStage FROM STRING_SPLIT(@InitialStage, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT VALUE AS StageNameYear
	INTO #FinalStage FROM STRING_SPLIT(@FinalStage, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS RelevantAuthorityId
	INTO #RelevantAuthority FROM STRING_SPLIT(@RelevantAuthorityId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS ProviderId
	INTO #Provider FROM STRING_SPLIT(@ProviderId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS DataTypeId
	INTO #DataType FROM STRING_SPLIT(@DataTypeId, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT CAST(VALUE AS INT) AS YearID
	INTO #YearID FROM STRING_SPLIT(@YearID, ',')  
	WHERE RTRIM(VALUE) <> '';

	SELECT 
		  [VersionId]
		  ,bd.[ProviderId]
		  ,[ProviderName]
		  ,bd.[RelevantAuthorityId]
		  ,[RelevantAuthorityName]
		  ,bd.[StageId]
		  ,StageName
		  ,[StageYearId]
		  ,CAST(StageName AS nvarchar)+ ' ' +CAST(StageYearId AS nvarchar) AS StageNameYear
		  ,(CASE WHEN bd.EsaCode IN ('D.2','D.1') THEN 1
				WHEN bd.EsaCode IN ('D.5','D.62') THEN 2
				WHEN bd.EsaCode IN ('D.91R','P.2') THEN 3
				WHEN bd.EsaCode IN ('D.61','D.6') THEN 4
				WHEN bd.EsaCode IN ('D.4','D.41') THEN 5
				WHEN bd.EsaCode IN ('P.13','D.3') THEN 6
				WHEN bd.EsaCode IN ('F.4','P.51G') THEN 7
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_CG') THEN 8
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_NMPC') THEN 9
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_EBF') THEN 10
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_LG') THEN 11
				WHEN (bd.EsaCode = '0' AND bd.EsaCategoryCode = 'REC') THEN 12
--				WHEN bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'REC_CG' THEN 11     -- check this?? 
				WHEN bd.EsaCode = 'D.9' THEN 8
				WHEN bd.EsaCode = 'D.7' THEN 9
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_NMPC') THEN 12
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_EBF') THEN 13
				WHEN (bd.EsaCode = 'D.73' AND bd.EsaCategoryCode = 'PAY_LG') THEN 14
     			WHEN (bd.EsaCode = '0' AND bd.EsaCategoryCode = 'PAY') THEN 15	
			END) AS 'Order'			
		  ,ec.[EsaCode]
		  ,[EsaCategoryCode]
		  ,EsaCodeDescription
		  ,[YearId]
		  ,bd.[DataTypeId]
		  ,DataType
		  ,[Amount] 
	FROM BudgetDataFlatFile bd WITH(NOLOCK) 
	INNER JOIN EsaCodes ec WITH(NOLOCK)
	ON bd.[EsaCode] = ec.EsaCode
	WHERE 
	((@InitialStage = '-1' OR CAST(StageName AS nvarchar)+ ' ' +CAST(StageYearId AS nvarchar) IN (SELECT StageNameYear FROM #InitialStage AS Initial))
	OR  (@FinalStage = '-1' OR CAST(StageName AS nvarchar)+ ' ' +CAST(StageYearId AS nvarchar) IN (SELECT StageNameYear FROM #FinalStage AS Final)))
	AND (@RelevantAuthorityId = '-1' OR [RelevantAuthorityId] IN (SELECT ra.RelevantAuthorityId FROM #RelevantAuthority AS ra))
	AND (@ProviderId = '-1' OR [ProviderId] IN (SELECT pr.ProviderId FROM #Provider AS pr))
	AND (@DataTypeId = '-1' OR [DataTypeId] IN (SELECT dt.DataTypeId FROM #DataType AS dt))
	AND (@YearID = '-1' or [YearId] IN (SELECT YearId FROM #YearID))
	AND NOT (Amount = 0 OR Amount IS NULL)

	DROP TABLE #InitialStage
	DROP TABLE #FinalStage
	DROP TABLE #RelevantAuthority
	DROP TABLE #Provider
	DROP TABLE #DataType
	DROP TABLE #YearID
END

GO


