
USE [budget]
GO

DROP PROCEDURE IF EXISTS [Security].[GetAllUsers]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Security].[GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Security].[GetAllUsers] AS' 
END
GO

-- =============================================
-- Author:		Audrey
-- Create date: 20/03/2018
-- Description:	Get all users
-- =============================================
ALTER PROCEDURE [Security].[GetAllUsers]
	@Page INT = 1,
	@PageSize INT = 10,
	@FilterBy NVARCHAR(100) = '',
	@TotalUserCount INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;

	CREATE TABLE #Users
	(
		UserId INT,
		UserName NVARCHAR(50),
		FirstName NVARCHAR(50),
		LastName NVARCHAR(50),
		RoleName NVARCHAR(50),
		RoleId INT,
		RowNumber INT
	)

	INSERT INTO #Users(UserId, UserName, FirstName, LastName, RoleName, RoleId, RowNumber)
	SELECT
		u.UserId, 
		u.UserName,
		u.FirstName,
		u.LastName,
		r.RoleName,
		r.RoleId,
		ROW_NUMBER() OVER(ORDER BY u.UserId) AS rowNumber
	FROM Security.Users u
	INNER JOIN Security.UserRoles ur
	ON u.UserId = ur.UserId
	INNER JOIN Security.Roles r
	ON r.RoleId = ur.RoleId
	WHERE Deleted = 0
	AND UserName LIKE '%' + @FilterBy + '%'

	SELECT @TotalUserCount = COUNT(UserId) FROM #Users

	
	SELECT 
		UserId, 
		UserName,
		FirstName,
		LastName, 
		RoleName, 
		RoleId
	FROM #Users
	WHERE rowNumber BETWEEN ((@Page - 1) * @PageSize) + 1 AND (@Page * @PageSize) 

	DROP TABLE #Users

END
GO

GRANT EXECUTE ON [Security].[GetAllUsers] TO [budgetviewer] AS [dbo]
GO


