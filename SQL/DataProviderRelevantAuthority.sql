USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Surveys]
GO 

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Surveys]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[Surveys](
	[SurveyId] INT IDENTITY(1,1) NOT NULL,
	[ProviderId] [int] NOT NULL,
	[RelevantAuthorityId] INT NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Provider_RelevantAuthority_Id] PRIMARY KEY CLUSTERED 
(
	[SurveyId] ASC,[RelevantAuthorityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Surveys_ModifiedBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[Surveys]'))
ALTER TABLE [dbo].[Surveys]  WITH CHECK ADD  CONSTRAINT [FK_Surveys_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [Security].[Users] (UserId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Surveys_ProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Surveys]'))
ALTER TABLE [dbo].[Surveys]  WITH CHECK ADD  CONSTRAINT [FK_Surveys_ProviderId] FOREIGN KEY(ProviderId)
REFERENCES [dbo].[DataProviders] (ProviderId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Surveys_RelevantAuthorityId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Surveys]'))
ALTER TABLE [dbo].[Surveys]  WITH CHECK ADD  CONSTRAINT [FK_Surveys_RelevantAuthorityId] FOREIGN KEY(RelevantAuthorityId)
REFERENCES [dbo].[RelevantAuthority] (RelevantAuthorityId)
GO

TRUNCATE TABLE [Surveys]

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 1, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId <=42

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 2, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId > 42 and RelevantAuthorityId < 70

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 3, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId >= 70 and RelevantAuthorityId < 76

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 4, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId > 75 and RelevantAuthorityId < 91

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 10, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 91

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 10, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 91

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 11, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 92

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 12, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 93

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 13, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 94

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 5, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 95

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 6, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 39

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 7, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 41

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 9, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 96

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 14, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 97

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 8, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 98

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 15, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 99

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 16, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 100

INSERT INTO [Surveys]([ProviderId],[RelevantAuthorityId],[ModifyDate],[CreateDate],[ModifiedBy])
SELECT 17, RelevantAuthorityId, GETDATE(), GETDATE(), 1
FROM RelevantAuthority 
WHERE RelevantAuthorityId = 101


SELECT * FROM RelevantAuthority where RelevantAuthorityId not in (select RelevantAuthorityId from Surveys)
SELECT * FROM Surveys