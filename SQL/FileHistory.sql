USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE FileHistory

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[FileHistory](
	[VersionId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[StageId] [int] NOT NULL,
	[StageYearId] [int] NOT NULL,
	[Valid] BIT NULL,
	[OriginalFileName] NVARCHAR(255) NULL,
	[ArchivedFileName] NVARCHAR(255) NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_FileHistory_Id] PRIMARY KEY CLUSTERED 
(
	[VersionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileHistory_ProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileHistory]'))
ALTER TABLE [dbo].[FileHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileHistory_ProviderId] FOREIGN KEY([ProviderId])
REFERENCES [dbo].[DataProviders] (ProviderId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileHistory_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileHistory]'))
ALTER TABLE [dbo].[FileHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileHistory_StageId] FOREIGN KEY(StageId)
REFERENCES [dbo].[Stage] (StageId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FileHistory_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileHistory]'))
ALTER TABLE [dbo].[FileHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileHistory_RelevantAuthorityId] FOREIGN KEY(RelevantAuthorityId)
REFERENCES [dbo].[RelevantAuthority] (RelevantAuthorityId)
GO