/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Budget]
GO
/****** Object:  StoredProcedure [dbo].[YearsGetAll]    Script Date: 04/02/2020 16:49:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads Years
-- =============================================
ALTER PROCEDURE [dbo].[YearsGetAll]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	  SELECT 
		YearId,
		Retired
	  FROM Year yr WITH(NOLOCK)
	  ORDER BY YearId asc
	
END

