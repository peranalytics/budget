USE Budget

IF NOT EXISTS(SELECT 1 FROM sys.columns  WHERE Name = N'ProviderCode' AND Object_ID = Object_ID(N'DataProviders'))
BEGIN
    -- Column Exists
	ALTER TABLE DataProviders
	ADD ProviderCode NVARCHAR(50) NULL
END

GO

UPDATE DataProviders SET ProviderCode = 'NMPC' WHERE ProviderId = 1
UPDATE DataProviders SET ProviderCode = 'EBF' WHERE ProviderId = 2
UPDATE DataProviders SET ProviderCode = 'Regulators' WHERE ProviderId = 3
UPDATE DataProviders SET ProviderCode = 'AHB' WHERE ProviderId = 4
UPDATE DataProviders SET ProviderCode = 'NTMA' WHERE ProviderId = 5
UPDATE DataProviders SET ProviderCode = 'HFA' WHERE ProviderId = 6
UPDATE DataProviders SET ProviderCode = 'SBCI' WHERE ProviderId = 7
UPDATE DataProviders SET ProviderCode = 'LA' WHERE ProviderId = 8
UPDATE DataProviders SET ProviderCode = 'ISIF' WHERE ProviderId = 9
UPDATE DataProviders SET ProviderCode = 'CSO' WHERE ProviderId = 10
UPDATE DataProviders SET ProviderCode = 'DPER' WHERE ProviderId = 11
UPDATE DataProviders SET ProviderCode = 'CBO' WHERE ProviderId = 12
UPDATE DataProviders SET ProviderCode = 'SIF' WHERE ProviderId = 13
UPDATE DataProviders SET ProviderCode = 'IBRC' WHERE ProviderId = 14
UPDATE DataProviders SET ProviderCode = 'HSE' WHERE ProviderId = 15
UPDATE DataProviders SET ProviderCode = 'ETB' WHERE ProviderId = 16
UPDATE DataProviders SET ProviderCode = 'IT' WHERE ProviderId = 17

