/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Budget]
GO
/****** Object:  StoredProcedure [dbo].[StageNameYearGetAll]    Script Date: 04/02/2020 15:29:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:		Tracy
-- Create date: 16/01/2020
-- Description:	Loads Concatenated Year & Stage Dataset
-- ========================================================
ALTER PROCEDURE [dbo].[StageNameYearGetAll]
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT DISTINCT
	CAST(st.StageName AS nvarchar)+ ' ' +CAST(bd.StageYearId AS nvarchar) AS StageNameYear,
	StageYearId

FROM 
	BudgetData AS bd 
	INNER JOIN Stage AS st WITH(NOLOCK)
	ON bd.StageId = st.StageId
	INNER JOIN year AS y WITH(NOLOCK)
	ON bd.StageYearId = y.YearId
	ORDER BY StageYearId ASC 

END

