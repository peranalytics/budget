USE Budget

INSERT INTO EsaCodes
SELECT '0','Other','Other' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1','Total economy','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.11','Non-financial corporations','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.11001','Public non-financial corporations ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.11002','National private non-financial corportations ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.11003','Foreign controlled non-financial corporations ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12','Financial corporations','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.121','Central bank (public)','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.122','Deposit-taking corporations except the central bank ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12201','Public ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12202','National private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12203','Foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.123','Money market funds','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12301','Public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12302','National private','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12303','Foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.124','Non-MMF investments funds','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12401','Public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12402','national private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12403','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.125','other financial intermediaries, except insurance corporations and pension funds ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12501','public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12502','national private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12503','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.126','financial auxiliaries ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12601','public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12602','national private','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12603','Foreign controlled ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.127','Captive financial instititions and money lenders','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12701','public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12702','national private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12703','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.128','insurance corporations','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12801','public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12802','national private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12803','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.129','pension funds','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12901','public','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12902','national private ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.12903','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.121+S.122+S.123','monetary financial institutions','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.13','general government','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1311 ','central government (excluding social security funds)','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1312','state government (excluding social security funds)','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1313','Local government (excluding social security funds)','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1314','social security funds','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.14','households','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.141','employers ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.142','own-account workers ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.143','employees','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.144','recipients of property and transfer income','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1441','recipients of property  income','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1442','recipients of pensions','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.1443','recipients of other transfers','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.15','non-profit institutions serving households ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.15002','national private','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.15003','foreign controlled','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.2','rest of world','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.21','member states and institutions and bodies of the European Union ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.211','member states of the european union ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.2111','member states of the euro area ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.2112','member states outside the euro area ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.212','institutions and bodies of the european union ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.2121','the european central bank (ecb)','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.2122','european instituitions and bodies, except the ECB','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'S.22','non-member countries and international organisations non-resident in the European Union ','Classification of Institutional Sectors (S)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.1','output','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.11','market output ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.119','financial intermediation services indirectly measured (FISIM)','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.12','output for own final use','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.13','non-market output','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.2','intermediate consumption ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.3','final consumption expenditure ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.31','individual consumption expenditure ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.32','collective consumption expenditure ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.4','actual final consumption','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.41','actual individual consumption','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.42','actual collective consumption','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.5','gross capital formation/p.5n net capital formation','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.51G','gross fixed capital formation','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.511','acquisition less disposal of fixed assets ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.5111','acquisitions of new fixed assets ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.5112','acquisitions of existing fixed assets ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.5113','disposals of existing fixed assets ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.512','costs of ownership transfer on non-produced assets ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.51C','consumption of fixed capital (-)','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.51C1','consumption of fixed capital on gross operating surplus (-)','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P51C2','consumption of fixed capital on gross mixed income (-)','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.51N','net fixed capital formation ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.52','changes in inventories','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.53','acquissitions less disposals of valuables ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.6 ','exports of goods and services ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.61','exports of goods','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.612','exports of services ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.7','imports of goods and services','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.71','imprts of goods ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'P.72','imports of services ','Classification of transctions and other flows -  Transactions in Preoducts (P)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'NP ','Acquisitions less disposals of non-produced assets','Transactions in non-produced non-financial assets (NP codes)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'NP.1 ','Acquisitions less disposals of natural resources','Transactions in non-produced non-financial assets (NP codes)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'NP.2 ','Acquisitions less disposals of contracts, leases and licences','Transactions in non-produced non-financial assets (NP codes)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'NP.3','Purchases less sales of goodwill and marketing assets ','Transactions in non-produced non-financial assets (NP codes)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.1','compensdation of employees','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.11','wages and salaries','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.12','employer''s social contributions ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.121','employer''s actual social contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.1211','employer''s actual pension contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.1212','employers'' actual non-pension contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.122','employers'' imputed social contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.1221','employers'' imputed pension contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.1222','employers'' imputed non-pension contributions','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.2','taxes on production and imports ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.21','taxes on products','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.211','valued added type taxes (VAT)','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.212','taxes and duties on imports excluding VAT ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.2121','import duties ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.2122','taxes on imports excluding VAT and duties','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.124','taxes on production except VAT and import taxes','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.29','othrer taxes on production','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.3','subsidies','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.31','subsidies on products ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.311','import subsidies ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.319','other subsidies on products ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.39','other subsidies on production ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.4','property income','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.41 ','interest ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.42','distributed income of corporations','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.421','dividends ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.422','withdrawals from income of quasi-corporations','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.43','reinvested earnings on foreign direct ionvestment ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.44','other investment income ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.441','investment income attributable to insurance policy holders','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.442','investment income payable on pension entitlements ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.443','investment income attributable to collective investment fund shareholders ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.4431','dividends attributable to collective investment fund shareholders','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.4432','retained earnings attributable to collective invetsment fund shareholders ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.45','rent ','Distributive transactions (D)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.5','Current taxes on income, wealth, etc.','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.51','taxes on income ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.59','other current taxes ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6','social contributions and benefits ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.61','net social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.611','employers'' actual social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6111','employers'' actual social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6112','employers'' actual non-pension social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.612','employers'' imputed social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6121','employers'' imputed pension contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6122','employers'' imputed non-pension contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.613','household''s actual social contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6131','household''s actual pension contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6132','household''s actual npn-pension contributions','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.614','household''s social contributions supplements ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6141','household''s pension contributions supplements ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6142','household''s non-pension contributions supplements ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.61SC','social insurance scheme services charges (-)','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.62','social benefits other than social transfers in kind','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.621','social security benefits in cash ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6211','social security pension benefits in cash','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6212','social security non-pension benefits in cash','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.622','other social insurance benefits','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6221','other social insurance pension benefits','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.6222','other social insurance non-pension benefits','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.623','social assistance benefits in cash ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.63','social transfers in kind','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.631','social transfers in kind - non-maket production','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.632','social transfers in kind - purchased market production','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.7',' other current transfers ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.71','net non-life insurance premiums','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.711','net non-life direct insurance premiums','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.712','net non-life direct reinsurance premiums','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.72','non-life insurance claims','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.721','non-life direct insurance claims','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.722','non-life direct reinsurance claims','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.73','current transfers within general government','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.74','current international cooperation ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.75','miscellaneous current transfers ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.751','current transfers to NPISHs','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.752','current transfers between housholds','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.759','other miscellaneous current transfers ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.76','vat-and GNI -bases EU own resources','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.8','adjustment for the change in pension entitlements ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.9','capital transfers','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.9R','capital transfers, receivable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.91R','capital taxes, receivable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.92R','investment grants, receivable ','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.99R','other capital taxes, receivable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.9P','capital transfers, payable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.91P ','capital taxes, payable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.92P ','investment grants, payable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'D.99P','other capital transfers, payable','Current transfers in cash and kind (D.5- D.8)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.1','monetary gold and SDRs','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.11','monetary gold ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.12','SDRs','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.2','currency and deposits ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.21','currency ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.22','transferable deposits ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.221','inter-bank positions','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.229','other transferable deposits ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.29','other  deposits ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.3','debt securities','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.31','short-term','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.32','long-term','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.4','loans','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.41','short-term','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.42','long-term','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.5','equity and investment fund shares/units ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.51','equity ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.511','listed shares','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.512','unlisted shares','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.519','other equity ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.52','investment fund shares/units','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.521','money market fund shares/units','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.522','non-MMF investment fund shares/units','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.6','insurance, pension and standardised guarantee schemes','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.61','non-life insurance technical reserves','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.62','life insurance and annuity entitlements','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.63','pension entitlements ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.64','claims of pension funds on pension  managers ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.65','entitlements to non-pension benefits ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.66','proivisions for calls under standardised guarantees','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.7','financial derivatives and employee stock options','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.71','financial derivatives ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.711','options','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.712','forwards','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.72','employee stock options','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.8','other accounts receivable/payable ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.81','trade credits and advances','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'F.89','other accounts receivable/payable, excluding trade credits and advances ','Transactions in financial assets and liabilities (F)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.1-5','Total changes in volume       ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.1','Economic appearance of assets       ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.2','Economic disappearance of non-produced assets      ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.21','Depletion of natural resources       ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.22','Other economic disappearance of non-produced assets     ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.3','Catastrophic losses         ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.4','Uncompensated seizures         ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.5','Other changes in volume n.e.c.      ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.6','Changes in classification        ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.61','Changes in sector classification and structure     ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.62','Changes in classification of assets and liabilities    ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.7','Nominal holding gains and losses      ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.71','Neutral holding gains and losses      ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'K.72','Real holding gains and losses      ','Other changes in assets (K)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.1G','Value added, gross/Gross domestic product      ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.2G','Operating surplus, gross        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.3G','Mixed income, gross        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.4G','Entrepreneurial income, gross        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.5G','Balance of primary incomes, gross/National income, gross    ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.6G','Disposable income, gross        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.7G','Adjusted disposable income, gross       ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.8G','Saving, gross         ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.9','Net lending (+)/net borrowing (�)      ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.9N','Net lending (+)/net borrowing (�) of the non-financialaccounts   ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.9F','Net lending (+)/net borrowing (�) of the financialaccounts   ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.10','Changes in net worth       ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.101','Changes in net worth due to saving andcapital transfers  ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.102','Changes in net worth due to other changesin volume of assets','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.103','Changes in net worth due to nominal holdinggains and losses ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.1031','Changes in net worth due to neutral holdinggains and losses ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.1032','Changes in net worth due to real holdinggains and losses ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.11','External balance of goods and services     ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.12','Current external balance        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'B.90','Net worth         ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'BF.90','Financial net worth        ','Classification of balancing items and net worth (B)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'LS','Opening balance sheet        ','Classification of Balance Sheet entries (L)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'LX','Changes in balance sheet       ','Classification of Balance Sheet entries (L)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'LE','Closing balance sheet        ','Classification of Balance Sheet entries (L)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1','Produced non-financial assets        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.11','Fixed assets by type of asset     ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.111','Dwellings          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.112','Other buildings and structures       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1121','Buildings other than dwellings       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1122','Other structures         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1123','Land improvements         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.113','Machinery and equipment        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1131','Transport equipment         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1132','ICT equipment         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1139','Other machinery and equipment       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.114','Weapons systems         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.115','Cultivated biological resources        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1151','Animal resources yielding repeat products      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1152','Tree, crop and plant resources yielding repeat products   ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT '(AN.116)','(Costs of ownership transfer on non-produced assets)    ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.117','Intellectual property products        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1171','Research and development        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1172','Mineral exploration and evaluation       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1173','Computer software and databases       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.11731','Computer software         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.11732','Databases          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1174','Entertainment, literary or artistic originals      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1179','Other intellectual property products       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.12','Inventories by type of inventory      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.121','Materials and supplies        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.122','Work-in-progress          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1221','Work-in-progress on cultivated biological assets      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.1222','Other work-in-progress         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.123','Finished goods         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.124','Military inventories         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.125','Goods for resale        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.13','Valuables          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.131','Precious metals and stones       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.132','Antiques and other art objects      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.133','Other valuables         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2','Non-produced non-financial assets        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.21','Natural resources         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.211','Land          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2111','Land underlying buildings and structures      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2112','Land under cultivation        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2113','Recreational land and associated surface water     ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2119','Other land and associated surface water     ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.212','Mineral and energy reserves       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.213','Non-cultivated biological resources        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.214','Water resources         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.215','Other natural resources        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2151','Radio spectra         ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.2159','Other          ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.22','Contracts, leases and licences       ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.221','Marketable operating leases        ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.222','Permits to use natural resources      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.223','Permits to undertake specific activities      ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.224','Entitlement to future goods and services on anexclusive basis  ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AN.23','Purchases less sales of goodwill and marketing assets   ','Non-Financial Assets (AN)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.1','Monetary gold and SDRs       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.11','Monetary gold         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.12','SDRs          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.2','Currency and deposits        ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.21','Currency          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.22','Transferable deposits         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.221','Inter-bank positions         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.229','Other transferable deposits        ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.29','Other deposits         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.3','Debt securities         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.31','Short-term          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.32','Long-term          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.4','Loans1          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.41','Short-term          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.42','Long-term          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.5','Equity and investment fund shares/units      ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.51','Equity          ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.511','Listed shares         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.512','Unlisted shares         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.519','Other equity         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.52','Investment fund shares/units        ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.521','Money Market Fund shares/units       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.522','Non-MMF investment fund shares/units       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.6','Insurance, pension and standardised guarantee schemes     ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.61','Non-life insurance technical reserves       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.62','Life insurance and annuity entitlements      ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.63','Pension entitlements         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.64','Claims of pension funds on pension managers    ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.65','Entitlements to non-pension benefits       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.66','Provisions for calls under standardised guarantees     ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.7','Financial derivatives and employee stock options     ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.71','Financial derivatives         ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.72','Employee stock options        ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.8','Other accounts receivable/payable        ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.81','Trade credits and advances       ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'AF.89','Other accounts receivable/payable, excluding trade credits and advances   ','Financial Assets (AF)' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF4_NNP','Loans: nominal value, non-performing       ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF4_MNP','Loans: market value, non-performing       ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF4_NNP','Loans: nominal value, non-performing       ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF4_MNP','Loans: market value, non-performing       ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF41NNP','Short-term loans: nominal value, non-performing      ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF42MNP','Long-term loans: market value, non-performing      ','Non-Performing Loans' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF11CP','when the pledge of monetised gold may affects its usabilility as reserve asset ','Contingent Positions' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF21NC','local currency notes and coins      ','Currency and Deposits ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF22FC','deposits in foreign currency       ','Currency and Deposits ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF21NC','local currency notes and coins      ','Currency and Deposits ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF22FC','deposits in foreign currency       ','Currency and Deposits ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF321','for transactions in listed long-term debt securities    ','Listed and Unlisted Debt Securities' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF322','for transactions in unlisted long-term debt securities    ','Listed and Unlisted Debt Securities' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF321','for stocks of listed long-term debt securities    ','Listed and Unlisted Debt Securities' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF322','for stocks of unlisted long-term debt securities    ','Listed and Unlisted Debt Securities' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF42L1','for long-term loans with outstanding maturity of lessthan one year ','Long-term Loans with Outstanding Maturity of less than one year and long-term loans secured by mortgage ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF42LM','for long-term loans secured by mortgage     ','Long-term Loans with Outstanding Maturity of less than one year and long-term loans secured by mortgage ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF42L1','for long-term loans with outstanding maturity of lessthan one year ','Long-term Loans with Outstanding Maturity of less than one year and long-term loans secured by mortgage ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF42LM','for long-term loans secured by mortgages     ','Long-term Loans with Outstanding Maturity of less than one year and long-term loans secured by mortgage ' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF5221','for transactions in listed non-MMF investment fund shares   ','Listed and Unlisted Investment Shares' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XF5222','for transactions in unlisted non-MMF investment fund shares   ','Listed and Unlisted Investment Shares' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF5221','for stocks of listed non-MMF investment fund shares   ','Listed and Unlisted Investment Shares' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF5222','for stocks of unlisted non-MMF investment fund shares   ','Listed and Unlisted Investment Shares' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF42IA','for interest arrears on long-term loans; and    ','Arrers in Interest and Repayments' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XAF42PA','for repayment arrears on long-term loans.     ','Arrers in Interest and Repayments' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XD5452PR','for personal remittances between resident and non-resident households   ','Personal and Total Remittances' , GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'XD5452TR','for total remittances between resident and non-resident households   ','Personal and Total Remittances' , GETDATE(), GETDATE(), 1 

