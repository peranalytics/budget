USE Budget

INSERT INTO Department
SELECT 'Department of Agriculture, Food and Marine ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Communication Climate Action and Environment ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department Culture, Heritage and the Gaeltacht', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Education and Skills ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Justice and Equality ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Transport, Tourism and Sport', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'An Taoiseach ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Business, Enterprise amd Innovation', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Social Protection ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Rural and Community Development ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Housing Planning and Local Government ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Finance ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Health', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department Foreign Affairs ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Cluid Housing', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Circle Voluntary Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Coopertaive Housing Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Clanmill Housing Ireland', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Focus Ireland ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Fold Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Housing Association for Integrated Living ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'The Iveagh Trust ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'North and East Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Oaklee Housing ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Respond ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Society of St Vincent De Paul ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Sophia Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'St John of God Housing Association ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Tuath Housing Association', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Department of Public Expenditure and Reform', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Health Service Executive ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Educational Training Boards ', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 'Institute of Technologies', GETDATE(), GETDATE(), 1
