USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [DataType]

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataType]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[DataType](
	[DataTypeId] [int]  NOT NULL,
	[DataType] NVARCHAR(100) NOT NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_DataType_Id] PRIMARY KEY CLUSTERED 
(
	[DataTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DataType_ModifiedBy]') AND parent_object_id = OBJECT_ID(N'[dbo].[DataType]'))
ALTER TABLE [dbo].[DataType]  WITH CHECK ADD  CONSTRAINT [FK_DataType_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [Security].[Users] (UserId)
GO

INSERT INTO [DataType](DataTypeId, DataType, [ModifyDate], [CreateDate], [ModifiedBy])
SELECT 0, 'Receipts', GETDATE(), GETDATE(), 1 UNION ALL
SELECT 1, 'Expenditure', GETDATE(), GETDATE(), 1 


