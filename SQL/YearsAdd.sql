USE Budget 
GO

DROP PROCEDURE IF EXISTS YearsAdd
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE YearsAdd
	@YearId INT,
	@UserId INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[Year]
			   ([YearId]
			   ,[ModifyDate]
			   ,[CreateDate]
			   ,[ModifiedBy]
			   ,[Retired])
	SELECT 
		@YearId,
		GETDATE(),
		GETDATE(),
		@UserId,
		0

END

GO

GRANT EXECUTE ON YearsAdd TO Budgetviewer 
