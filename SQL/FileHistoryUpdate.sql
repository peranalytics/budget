USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[FileHistoryUpdate]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Updates file history
-- =============================================
CREATE PROCEDURE [dbo].[FileHistoryUpdate] 
	@VersionId INT,
	@ArchivedFileName NVARCHAR(255),
	@Valid BIT
AS

BEGIN

	UPDATE FileHistory
	SET ArchivedFileName = @ArchivedFileName,
	Valid = @Valid,
	ModifyDate = GETDATE()
	WHERE VersionId = @VersionId


END
GO

GRANT EXECUTE ON [dbo].[FileHistoryUpdate] TO [budgetviewer] AS [dbo]
GO


