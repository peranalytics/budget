USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportQueueGet
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportQueueGet
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT [ExportId]
		  ,eq.[StageId]
		  ,StageName
		  ,[YearId]
		  ,eq.[UserId]
		  ,UserName
		  ,[Active]
		  ,[Exported]
		  ,[RequestedDate]
		  ,[ExportDate]
		  ,eq.[ModifyDate]
		  ,eq.[CreateDate]
	  FROM [dbo].[ExportQueue] eq WITH(NOLOCK)
	  INNER JOIN Stage s WITH(NOLOCK)
	  ON s.StageId = eq.StageId
	  INNER JOIN Security.Users u WITH(NOLOCK)
	  ON u.UserId = eq.UserId
	  WHERE [Active] = 1 AND Exported = 0
END

GO

GRANT EXECUTE ON ExportQueueGet TO Budgetviewer 
