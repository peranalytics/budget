USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE ExportQueue

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[ExportQueue](
	ExportId INT IDENTITY (1,1) NOT NULL,
	[StageId] [int] NOT NULL,
	[YearId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Active] BIT NOT NULL,
	[Exported] BIT NOT NULL,
	[RequestedDate] DATETIME NULL DEFAULT GETDATE(),
	[ExportDate] DATETIME NULL,
	[ModifyDate] [datetime] NULL,
	[CreateDate] [datetime] NULL
	CONSTRAINT [PK_Roles_RoleId] PRIMARY KEY CLUSTERED 
(
	ExportId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExportQueue_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExportQueue]'))
ALTER TABLE [dbo].[ExportQueue]  WITH CHECK ADD  CONSTRAINT [FK_ExportQueue_UserId] FOREIGN KEY([UserId])
REFERENCES [Security].[Users] (UserId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExportQueue_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExportQueue]'))
ALTER TABLE [dbo].[ExportQueue]  WITH CHECK ADD  CONSTRAINT [FK_ExportQueue_YearId] FOREIGN KEY([YearId])
REFERENCES [dbo].[Year] (YearId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExportQueue_StageIdId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExportQueue]'))
ALTER TABLE [dbo].[ExportQueue]  WITH CHECK ADD  CONSTRAINT [FK_ExportQueue_StageId] FOREIGN KEY(StageId)
REFERENCES [dbo].[Stage] (StageId)
GO



