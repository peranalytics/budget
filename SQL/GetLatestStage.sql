USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[GetLatestStage]
GO

-- =============================================
-- Author:		Tracy
-- Create date: 27/01/2023
-- Description:	Get the Latest Stage
-- =============================================
CREATE PROCEDURE [dbo].[GetLatestStage]
--	@LatestStageYear INT,
--	@LatestStageId INT

AS	
BEGIN

SELECT DISTINCT TOP 1
	StageYearId AS LatestStageYear,
	StageId AS LatestStage
FROM BudgetDataFlatFile
--WHERE StageYearId = @LatestStageYear AND StageId = @LatestStageId
ORDER BY StageYearId DESC

END
GO

GRANT EXECUTE ON [dbo].[GetLatestStage] TO [budgetviewer] AS [dbo]
GO



