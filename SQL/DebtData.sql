USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE DebtData

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebtData]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[DebtData](
	[VersionId] [int] NOT NULL,
	[ProviderId] [int] NOT NULL,
	[DebtCategory] NVARCHAR(100) NOT NULL,
	[InputData] NVARCHAR(MAX) NOT NULL,
	[Identifiers] NVARCHAR(100) NOT NULL,
	[Units] NVARCHAR(100) NOT NULL,
	[StageId] [int] NOT NULL,
	[StageYearId] [int] NOT NULL,
	[YearId] INT NOT NULL,
	[Amount] DECIMAL(10,2) NULL,
	[ModifyDate] [datetime] NULL DEFAULT GETDATE(),
	[CreateDate] [datetime] NULL DEFAULT GETDATE()
	)
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DebtData_ProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebtData]'))
ALTER TABLE [dbo].[DebtData]  WITH CHECK ADD  CONSTRAINT [FK_DebtData_ProviderId] FOREIGN KEY([ProviderId])
REFERENCES [dbo].[DataProviders] (ProviderId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DebtData_YearId]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebtData]'))
ALTER TABLE [dbo].[DebtData]  WITH CHECK ADD  CONSTRAINT [FK_DebtData_YearId] FOREIGN KEY([YearId])
REFERENCES [dbo].[Year] (YearId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DebtData_VersionId]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebtData]'))
ALTER TABLE [dbo].[DebtData]  WITH CHECK ADD  CONSTRAINT [FK_DebtData_VersionId] FOREIGN KEY(VersionId)
REFERENCES [dbo].[FileHistory] (VersionId)
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DebtData_StageIdId]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebtData]'))
ALTER TABLE [dbo].[DebtData]  WITH CHECK ADD  CONSTRAINT [FK_DebtData_StageId] FOREIGN KEY(StageId)
REFERENCES [dbo].[Stage] (StageId)
GO

SELECT * FROM [DebtData]