USE Budget 
GO

DROP PROCEDURE IF EXISTS ExportQueueGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ExportQueueGetAll
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT [ExportId]
		  ,eq.[StageId]
		  ,StageName
		  ,[YearId]
		  ,eq.[UserId]
		  ,UserName
		  ,[Active]
		  ,[Exported]
		  ,[RequestedDate]
		  ,[ExportDate]
		  ,eq.[ModifyDate]
		  ,eq.[CreateDate]
	  FROM [dbo].[ExportQueue] eq WITH(NOLOCK)
	  INNER JOIN Stage s WITH(NOLOCK)
	  ON s.StageId = eq.StageId
	  INNER JOIN Security.Users u WITH(NOLOCK)
	  ON u.UserId = eq.UserId
	  ORDER BY [RequestedDate] desc
END

GO

GRANT EXECUTE ON ExportQueueGetAll TO Budgetviewer 
