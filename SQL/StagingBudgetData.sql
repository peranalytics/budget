USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE StagingBudgetData

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingBudgetData]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[StagingBudgetData](
	[VersionId] [int] NOT NULL,
	[ProviderId] [int] NOT NULL,
	[RelevantAuthorityId] [int] NOT NULL,
	[Stage] NVARCHAR(30) NOT NULL,
	[StageYearId] [int] NOT NULL,
	[EsaCodeDescription] NVARCHAR(500) NOT NULL,
	[EsaCode] NVARCHAR(50) NOT NULL,
	[EsaCategoryCode] NVARCHAR(50) NOT NULL,
	[YearId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[Amount] DECIMAL(10,2) NULL
	)
END
GO
