USE Budget

GO
DROP FUNCTION IF EXISTS GetLatestStagePerRa
GO

-- ================================================================================
-- Author:		Audrey
-- Create date: 23/01/2023
-- Description:	Get most recent data by RA for Stage/ Year for Impute function
-- =================================================================================


CREATE FUNCTION GetLatestStagePerRa (
    @Impute INT, -- 1 is impute - get the previous stage / years data, 0 is get the most recent year
	@StageYearId INT,
	@StageId INT
)
RETURNS @RankedRas TABLE 
	   (
			RelevantAuthorityId INT,
			StageYearId INT,
			StageId INT,
			RankedOrder INT
		)
AS
BEGIN
		IF (@Impute = 1)
		BEGIN
			INSERT INTO @RankedRas(RelevantAuthorityId, StageYearId, StageId, RankedOrder)
			SELECT DISTINCT RelevantAuthorityId, StageYearId, StageId, DENSE_RANK() OVER (PARTITION BY RelevantAuthorityId ORDER BY RelevantAuthorityId, StageYearId DESC, StageId DESC)
			FROM BudgetDataFlatFile
			ORDER BY RelevantAuthorityId, StageYearId DESC, StageId DESC
	 

			DELETE FROM @RankedRas WHERE NOT RankedOrder = 1
		END

		ELSE

		BEGIN
			INSERT INTO @RankedRas(RelevantAuthorityId, StageYearId, StageId, RankedOrder)
			SELECT DISTINCT RelevantAuthorityId, @StageYearId, @StageId, 1  FROM RelevantAuthority 
		END

		RETURN;
END
GO


