USE Budget 
GO

DROP PROCEDURE IF EXISTS ProviderGetAll
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Audrey
-- Create date: 01/11/2019
-- Description:	Loads BD
-- =============================================
CREATE PROCEDURE ProviderGetAll
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		ProviderId,
		ProviderName
	FROM DataProviders
END

GO

GRANT EXECUTE ON ProviderGetAll TO Budgetviewer 
