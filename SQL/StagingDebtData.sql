USE Budget
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE StagingDebtData

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StagingDebtData]') AND type in (N'U'))
BEGIN
CREATE TABLE[dbo].[StagingDebtData](
	[VersionId] [int] NOT NULL,
	[ProviderName] NVARCHAR(50) NOT NULL,
	[DebtCategory] NVARCHAR(100) NOT NULL,
	[InputData] NVARCHAR(MAX) NOT NULL,
	[Identifiers] NVARCHAR(100) NOT NULL,
	[Units] NVARCHAR(100) NOT NULL,
	[Stage] NVARCHAR(100) NOT NULL,
	[StageYear] INT NOT NULL,
	[Year] INT NOT NULL,
	[Amount] DECIMAL(10,2) NULL
	)
END
GO
