USE Budget
GO

DROP PROCEDURE IF EXISTS [dbo].[TruncateDebtStagingTables]
GO

-- =============================================
-- Author:		Audrey
-- Create date: 01/03/2019
-- Description:	Deletes the staging tables
-- =============================================
CREATE PROCEDURE [dbo].[TruncateDebtStagingTables] 
	
AS

BEGIN
	DELETE FROM [StagingDebtData]
END
GO

GRANT EXECUTE ON [dbo].[TruncateDebtStagingTables] TO [budgetviewer] AS [dbo]
GO


