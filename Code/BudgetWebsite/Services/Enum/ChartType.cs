﻿namespace Services.Enum
{
    public enum ChartType
    {
        PieChart = 0,
        BarChart = 1,
        DonutChart = 2
    }
}
