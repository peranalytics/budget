﻿using System.ComponentModel;

namespace Services.Enum
{
    public enum Stage
    {
        [Description("Budget")]
        Budget = 1,
        [Description("Spu")]
        Spu = 2
    }
}