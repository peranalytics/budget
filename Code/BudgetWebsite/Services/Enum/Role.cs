﻿using System.ComponentModel;

namespace Services.Enum
{
    public enum Role
    {
        [Description("Administrator")]
        Administrator = 1,
        [Description("Budget Administrator")]
        BudgetAdministrator = 2,
        [Description("Budget Viewer")]
        BudgetViewer = 3
    }
}
