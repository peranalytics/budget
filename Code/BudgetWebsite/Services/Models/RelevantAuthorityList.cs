﻿using System.Collections.Generic;

namespace Services.Models
{
    public class RelevantAuthorityList
    {
        public List<RelevantAuthority> RelevantAuthorities { get; set; }

        public int TotalCount { get; set; }
    }
}
