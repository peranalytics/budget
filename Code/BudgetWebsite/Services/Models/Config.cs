﻿namespace Services.Models
{
    public class Config
    {
        public string ConfigName { get; set; }

        public string ConfigValue { get; set; }
    }
}
