﻿using System;

namespace Services.Models
{
    public class Metadata
    {
        public int VersionId { get; set; }

        public int Year { get; set; }

        public RelevantAuthority RelevantAuthority { get; set; }

        public string Stage { get; set; }

        public int FileType { get; set; }

        public string FileName { get; set; }

        public string LocalFileName { get; set; }

        public DateTime? UploadedDate { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? DataloadedDate { get; set; }

        public bool? Success { get; set; }
    }
}
