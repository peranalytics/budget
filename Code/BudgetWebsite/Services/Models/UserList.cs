﻿using System.Collections.Generic;

namespace Services.Models
{
    public class UserList
    {
        public List<User> Users { get; set; }

        public int TotalCount { get; set; }
    }
}
