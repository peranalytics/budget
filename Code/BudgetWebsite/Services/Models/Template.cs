﻿using System;

namespace Services.Models
{
    public class Template
    {
        public int TemplateId { get; set; }

        public int ExportId { get; set; }

        public string FileName { get; set; }

        public DateTime ExportDate { get; set; }
    }
}
