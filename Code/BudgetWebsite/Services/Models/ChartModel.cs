﻿using System.Collections.Generic;
using Chart.Mvc.ComplexChart;
using Chart.Mvc.SimpleChart;

namespace Services.Models
{
    public class ChartModel
    {
        public IEnumerable<ComplexDataset> ComplexDataset { get; set; }

        public IEnumerable<SimpleData> SimpleDataset { get; set; }

        public string[] Labels { get; set; }
    }
}
