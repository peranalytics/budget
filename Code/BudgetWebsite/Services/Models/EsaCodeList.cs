﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
public class EsaCodeList
    {
        public List<EsaCode> EsaCodes { get; set; }

        public int TotalCount { get; set; }
    }
}
