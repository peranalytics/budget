﻿using System;

namespace Services.Models
{
    public class RegisterResult
    {
        public string Message { get; set; }

        public bool Success { get; set; }

        public Exception Exception { get; set; }
    }
}
