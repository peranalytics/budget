﻿using System;
using Services.Enum;

namespace Services.Models
{
    public class Export
    {
        public int ExportId { get; set; }

        public Stage Stage { get; set; }

        public int StageYear { get; set; }

        public User User { get; set; }

        public DateTime RequestedDate { get; set; }

        public DateTime? ExportedDate { get; set; }

        public bool Active { get; set; }
    }
}
