﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
   public class EsaCode
    {
        public int EsaCodeId { get; set; }

        public string EsaCodeName { get; set; }

        public string EsaCodeDescription { get; set; }

        public string EsaCodeCategory { get; set; }

    }
}


