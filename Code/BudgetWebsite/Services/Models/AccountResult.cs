﻿namespace Services.Models
{
    public class AccountResult
    {
        public string UserId { get; set; }

        public string HashedPassword { get; set; }

        public string Message { get; set; }

        public bool Success { get; set; }
    }
}
