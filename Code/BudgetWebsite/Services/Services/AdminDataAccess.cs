﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Services.Enum;
using Services.Models;

namespace Services.Services
{
    public class AdminDataAccess
    {
        private readonly string _connectionString;

        public AdminDataAccess()
        {
            _connectionString = "SQLAuthConnection";
        }

        public RelevantAuthorityList GetAllRelevantAuthorities(int page, int pageSize, string filterBy)
        {
            var relevantAuthorityList = new RelevantAuthorityList();
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthorityGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {

                    command.Parameters.Add("@Page", SqlDbType.Int).Value = page;
                    command.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    if (filterBy != null)
                    {
                        command.Parameters.Add("@FilterBy", SqlDbType.VarChar).Value = filterBy;
                    }

                    command.Parameters.Add("@TotalRaCount", SqlDbType.Int).Direction = ParameterDirection.Output;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        relevantAuthorityList.RelevantAuthorities = new List<RelevantAuthority>();
                        relevantAuthorityList.TotalCount = 0;
                        return relevantAuthorityList;
                    }

                    var raCount = Convert.ToInt32(command.Parameters["@TotalRaCount"].Value);

                    relevantAuthorityList.RelevantAuthorities = GetAllRelevantAuthorities(ds);
                    relevantAuthorityList.TotalCount = raCount;

                    return relevantAuthorityList;
                }
            }
        }

        public RelevantAuthority GetRelevantAuthorityById(int relevantAuthorityId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthorityGetById]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthorityId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }
            
            var ras = GetAllRelevantAuthorities(ds);
            return ras.FirstOrDefault();
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[DepartmentGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return (from DataRow dataRow in ds.Tables[0].Rows
                select new Department
                {
                    DepartmentId = Convert.ToInt32(dataRow["DepartmentId"].ToString()),
                    DepartmentName = dataRow["DepartmentName"].ToString()
                }).ToList();
        }

        public UserList GetAllUsers(int? page, int pageSize, string filterBy)
        {
            var ds = new DataSet();
            var userCount = 0;
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[GetAllUsers]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@Page", SqlDbType.Int).Value = page;
                    command.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    if (filterBy != null)
                    {
                        command.Parameters.Add("@FilterBy", SqlDbType.VarChar).Value = filterBy;
                    }

                    command.Parameters.Add("@TotalUserCount", SqlDbType.Int).Direction = ParameterDirection.Output;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    userCount = Convert.ToInt32(command.Parameters["@TotalUserCount"].Value);

                }
            }

            return new UserList
            {
                Users = CovertDsToUserList(ds),
                TotalCount = userCount
            };
        }

        public void DeleteUser(int userId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[DeleteUser]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateRelevantAuthorityId(RelevantAuthority relevantAuthority)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[RelevantAuthorityUpdateById]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = relevantAuthority.RelevantAuthorityId;
                    command.Parameters.Add("@RelevantAuthorityName", SqlDbType.Text).Value = relevantAuthority.RelevantAuthorityName;
                    command.Parameters.Add("@ContactDetails", SqlDbType.Text).Value = relevantAuthority.ContactDetails;
                    command.Parameters.Add("@DepartmentId", SqlDbType.Int).Value = relevantAuthority.Department.DepartmentId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<int> GetAllStageYears()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[StageYearManageAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        return new List<int>();
                    }

                    return GetAllYears(ds);
                }
            }
        }

        public void QueueTemplateExport(int stageId, int yearId, int userId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportQueueAdd]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@StageId", SqlDbType.Int).Value = stageId;
                    command.Parameters.Add("@YearId", SqlDbType.Int).Value = yearId;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void AddYear(int yearId, int userId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[YearsAdd]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@YearId", SqlDbType.Int).Value = yearId;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public List<Export> GetAllExports()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportQueueGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        return new List<Export>();
                    }

                    return CovertDsToExportList(ds);
                }
            }
        }

        public List<Template> GetAllExportsFiles(int exportId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportHistoryGetById]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@ExportId", SqlDbType.Int).Value = exportId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        return new List<Template>();
                    }

                    return CovertDsToTemplateList(ds);
                }
            }
        }

        public Template GetTemplateFileById(int templateId)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportHistoryGetByTemplateId]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@TemplateId", SqlDbType.Int).Value = templateId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        return null;
                    }

                    var templateList = CovertDsToTemplateList(ds);

                    return templateList.FirstOrDefault();
                }
            }
        }

        public List<Tuple<int, bool>> GetAllYears()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[YearsGetAll]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);

                    if (ds.Tables.Count == 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 1)
                    {
                        return new List<Tuple<int, bool>>();
                    }

                    return CovertDsToTupleList(ds);
                }
            }
        }

        private List<RelevantAuthority> GetAllRelevantAuthorities(DataSet ds)
        {
            var raList = new List<RelevantAuthority>();

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                var ra = new RelevantAuthority();
                var dept = new Department();

                ra.RelevantAuthorityId = Convert.ToInt32(dataRow["RelevantAuthorityId"].ToString());
                ra.RelevantAuthorityName = dataRow["RelevantAuthorityName"].ToString();
                ra.ContactDetails = dataRow["ContactDetails"].ToString();

                dept.DepartmentId = Convert.ToInt32(dataRow["DepartmentId"].ToString());
                dept.DepartmentName = dataRow["DepartmentName"].ToString();

                ra.Department = dept;
                raList.Add(ra);
            }

            return raList;
        }

        private List<User> CovertDsToUserList(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<User>();
            }

            var dt = ds.Tables[0];

            return (from DataRow dr in dt.Rows
                    select new User
                    {
                        Id = Convert.ToInt32(dr["UserId"]),
                        Email = dr["Username"].ToString(),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        Role = (Role)Convert.ToInt32(dr["RoleId"])
                    }).ToList();

        }

        private List<int> GetAllYears(DataSet ds)
        {
            return (from DataRow dataRow in ds.Tables[0].Rows select Convert.ToInt32(dataRow["YearId"].ToString())).ToList();
        }

        private List<Export> CovertDsToExportList(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<Export>();
            }

            var dt = ds.Tables[0];

            return (from DataRow dr in dt.Rows
                select new Export
                {
                    User = new User {
                        Id = Convert.ToInt32(dr["UserId"]),
                        Email = dr["Username"].ToString(),
                    },

                    ExportId = Convert.ToInt32(dr["ExportId"]),
                    StageYear = Convert.ToInt32(dr["YearId"]),
                    Stage = (Stage)Convert.ToInt32(dr["StageId"]),
                    ExportedDate = (DateTime?)(dr["ExportDate"] == DBNull.Value ? null : dr["ExportDate"]),
                    RequestedDate = Convert.ToDateTime(dr["RequestedDate"]),
                    Active = Convert.ToBoolean(dr["Active"])
                }).ToList();
        }

        private List<Template> CovertDsToTemplateList(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<Template>();
            }

            var dt = ds.Tables[0];

            return (from DataRow dr in dt.Rows
                select new Template
                {
                    TemplateId = Convert.ToInt32(dr["ExportFileId"]),
                    ExportId = Convert.ToInt32(dr["ExportId"]),
                    FileName = dr["ExportFileName"].ToString(),
                    ExportDate = Convert.ToDateTime(dr["ExportDate"].ToString()),
                }).ToList();

        }

        private List<Tuple<int, bool>> CovertDsToTupleList(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<Tuple<int, bool>>();
            }

            var dt = ds.Tables[0];

            return (from DataRow dr in dt.Rows
                select new Tuple<int, bool>(Convert.ToInt32(dr["YearId"]), Convert.ToBoolean(dr["Retired"]))
                ).ToList();
        }
    }
}
