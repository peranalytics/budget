﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Services.Services
{
    public class ConfigurationDataAccess
    {
        public DataSet GetConfiguration()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[GetConfig]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return ds;
        }

        public void LogReportView(int userId, int reportId)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[Security].[LogReportUsage]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@ReportId", SqlDbType.Int).Value = reportId;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
