﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Services.Models;

namespace Services.Services
{
    public class ConfigurationService
    {
        private readonly ConfigurationDataAccess _configurationDataAccess;

        public static string DgvtUrl => ConfigurationManager.AppSettings["DGVTUrl"];

        public ConfigurationService()
        {
            _configurationDataAccess = new ConfigurationDataAccess();
        }

        public IEnumerable<Config> GetConfigurataion()
        {
            var ds = _configurationDataAccess.GetConfiguration();

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                return (from DataRow dataRow in ds.Tables[0].Rows
                        select new Config
                        {
                            ConfigName = dataRow["ConfigName"].ToString(),
                            ConfigValue = dataRow["ConfigValue"].ToString()
                        }).ToList();
            }

            return null;
        }

        public ReportConfiguration GetReportConfiguration()
        {
            var configuration = GetConfigurataion().ToList();
            var reportConfig = new ReportConfiguration();

            if (!configuration.Any())
            {
                return null;
            }

            reportConfig.UserName = configuration.FirstOrDefault(m => m.ConfigName == "WindowsReportUsername")?.ConfigValue;
            reportConfig.Password = configuration.FirstOrDefault(m => m.ConfigName == "WindowsReportPassport")?.ConfigValue;
            reportConfig.Domain = configuration.FirstOrDefault(m => m.ConfigName == "WindowsReportDomain")?.ConfigValue;
            reportConfig.ReportServerUrl = configuration.FirstOrDefault(m => m.ConfigName == "ReportServerURL")?.ConfigValue;

            return reportConfig;
        }

        public void LogReportView(int userId, int reportId)
        {
            _configurationDataAccess.LogReportView(userId, reportId);
        }
    }
}
