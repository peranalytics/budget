﻿using System;
using System.Collections.Generic;
using Services.Models;

namespace Services.Services
{
    public class AdminService
    {
        private readonly AdminDataAccess _adminDataAccess;

        public AdminService()
        {
            _adminDataAccess = new AdminDataAccess();
        }

        public RelevantAuthorityList GetRelevantAuthorityList(int page, int pageSize, string filterBy)
        {
            return _adminDataAccess.GetAllRelevantAuthorities(page, pageSize, filterBy);
        }

        public RelevantAuthority GetRelevantAuthorityById(int relevantAuthorityId)
        {
            return _adminDataAccess.GetRelevantAuthorityById(relevantAuthorityId);
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            return _adminDataAccess.GetAllDepartments();
        }

        public UserList GetUsers(int? page, int pageSize, string filterBy)
        {
            return _adminDataAccess.GetAllUsers(page, pageSize, filterBy);
        }

        public void DeleteUser(int userId)
        {
            _adminDataAccess.DeleteUser(userId);
        }

        public void UpdateRelevantAuthorityId(RelevantAuthority relevantAuthority)
        {
            _adminDataAccess.UpdateRelevantAuthorityId(relevantAuthority);
        }

        public List<int> GetAllStageYears()
        {
            return _adminDataAccess.GetAllStageYears();
        }

        public List<Export> GetAllExports()
        {
            return _adminDataAccess.GetAllExports();
        }

        public List<Template> GetTemplatesByExportId(int exportId)
        {
            return _adminDataAccess.GetAllExportsFiles(exportId);
        }

        public Template GetTemplateById(int templateId)
        {
            return _adminDataAccess.GetTemplateFileById(templateId);
        }

        public List<Tuple<int, bool>> GetAllYears()
        {
            return _adminDataAccess.GetAllYears();
        }

        public void QueueTemplateExport(int stageId, int yearId, int userId)
        {
            _adminDataAccess.QueueTemplateExport(stageId, yearId, userId);
        }

        public void AddYear(int yearId, int userId)
        {
            _adminDataAccess.AddYear( yearId, userId);
        }
    }
}
