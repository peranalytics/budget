﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Services.Enum;
using Services.Models;
using Services.Services;

namespace Databank.Helpers
{
    public class SecurityHelper
    {
        public static bool IsAdmin()
        {
            var accountService = new AccountService();
            var role = accountService.GetUserRole(Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId()));

            return (role == Role.BudgetAdministrator) || (role == Role.Administrator);
        }
    }
}