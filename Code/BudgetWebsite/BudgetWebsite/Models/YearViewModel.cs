﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Databank.Models
{
    public class YearViewModel
    {
        public List<Tuple<int, bool>> YearList { get; set; }

        [Range(2000, int.MaxValue, ErrorMessage = "Please enter valid number")]
        [Display(Name = "Year")]
        public int? NewYear { get; set; }
    }
}