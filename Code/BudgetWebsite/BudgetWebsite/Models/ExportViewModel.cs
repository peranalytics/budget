﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Databank.Models
{
    public class ExportViewModel
    {
        [Required]
        [Range(0, Int32.MaxValue, ErrorMessage = "Please select a stage year")]
        public int SelectedStageYear { get; set; }

        public List<SelectListItem> StageYearList { get; set; }

        [Required]
        [Range(0, Int32.MaxValue, ErrorMessage = "Please select a stage")]
        public int SelectedStage { get; set; }

        public List<SelectListItem> StageList { get; set; }
    }
}