﻿using System.Collections.Generic;
using Services.Enum;
using Services.Models;

namespace Databank.Models
{
    public class StageDetailsViewModel
    {
        public FileHistoryViewModel FileHistoryViewModel { get; set; }

        public Stage Stage { get; set; }

        public Dictionary<string, string> StageUsers { get; set; }

        public ChartViewModel LargeChartViewModel { get; set; }

        public IEnumerable<FileLog> FileLogs { get; set; }
    }
}