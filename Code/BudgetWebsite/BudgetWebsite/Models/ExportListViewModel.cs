﻿using System;
using System.Collections.Generic;
using Services.Models;

namespace Databank.Models
{
    public class ExportListViewModel
    {
        public List<Export> Exports { get; set; }
    }
}