﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Services.Models;

namespace Databank.Models
{
    public class RelevantAuthorityViewModel
    {
        public int RelevantAuthorityId { get; set; }

        [Required(ErrorMessage = "Please enter a valid name")]
        [Display(Name = "Relevant Authority Name")]
        public string RelevantAuthorityName { get; set; }

        public string ContactDetails { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Please select a stage year")]
        public int SelectedDepartment { get; set; }

        public List<SelectListItem> DepartmentList { get; set; }
    }
}