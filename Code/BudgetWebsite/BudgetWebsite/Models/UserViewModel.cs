﻿using Services.Enum;

namespace Databank.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Role Role { get; set; }
    }
}