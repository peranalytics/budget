﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Databank.Models
{
    public class CheckLoginViewModel
    {
        public List<SelectListItem> Users { get; set; }
    }
}