﻿using System.Collections.Generic;
using Services.Models;

namespace Databank.Models
{
    public class FileHistoryViewModel
    {
        public RelevantAuthority RelevantAuthority { get; set; }

        public IEnumerable<Metadata> Metadata { get; set; }
    }
}