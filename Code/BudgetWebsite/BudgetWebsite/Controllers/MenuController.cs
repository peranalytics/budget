﻿using System.Web.Mvc;

namespace Databank.Controllers
{
    public class MenuController : Controller
    {
        public ActionResult RenderMenu()
        {
            return View("_Menu");
        }
    }
}