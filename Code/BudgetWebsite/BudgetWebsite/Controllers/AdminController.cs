﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Databank.Extensions;
using Databank.Helpers;
using Microsoft.AspNet.Identity;
using PagedList;
using Services.Enum;
using Services.Models;
using Databank.Models;
using Ionic.Zip;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly AccountService _accountService;
        private readonly AdminService _adminService;
        private readonly int _currentUserId;

        public AdminController()
        {
            _accountService = new AccountService();
            _adminService = new AdminService();
            _currentUserId = Convert.ToInt32(System.Web.HttpContext.Current.User.Identity.GetUserId());
        }

        public ActionResult Index()
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult Register()
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            var registerViewModel = new RegisterViewModel { PermissionList = BuildPermissionList() };
            return View(registerViewModel);
        }

        public ActionResult Users(int? page, string currentFilter, string searchString)
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return GetUserListViewModel(page, currentFilter, searchString);
        }

        public ActionResult RelevantAuthorities(int? page, string currentFilter, string searchString)
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return GetRelevantAuthoritiesViewModel(page, currentFilter, searchString);
        }

        public ActionResult RelevantAuthority(int relevantAuthorityId)
        {
            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            var raVm = new RelevantAuthorityViewModel();
            var ra = _adminService.GetRelevantAuthorityById(relevantAuthorityId);

            raVm.RelevantAuthorityId = ra.RelevantAuthorityId;
            raVm.SelectedDepartment = ra.Department.DepartmentId;
            raVm.ContactDetails = ra.ContactDetails;
            raVm.RelevantAuthorityName = ra.RelevantAuthorityName;
            raVm.DepartmentList = GetAllDepartments();

            return View(raVm);
        }

        private List<SelectListItem> GetAllDepartments()
        {
            var depts = _adminService.GetAllDepartments();
            var deptlist = depts.Select(dept => new SelectListItem
            {
                Text = dept.DepartmentName.ToString(),
                Value = dept.DepartmentId.ToString()
            })
                .ToList();

            return deptlist;
        }

        [HttpPost]
        public ActionResult RelevantAuthority(RelevantAuthorityViewModel raVm)
        {
            if (!ModelState.IsValid)
            {
                raVm.DepartmentList = GetAllDepartments();
                return View(raVm);
            }

            if (!SecurityHelper.IsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            var relevantAuthority = new RelevantAuthority
            {
                RelevantAuthorityId = raVm.RelevantAuthorityId,
                Department = new Department { DepartmentId = raVm.SelectedDepartment },
                ContactDetails = raVm.ContactDetails,
                RelevantAuthorityName = raVm.RelevantAuthorityName
            };

            _adminService.UpdateRelevantAuthorityId(relevantAuthority);

            raVm.DepartmentList = GetAllDepartments();
            ViewBag.ShowSuccess = "true";
            ViewBag.SuccessMessage = $"{raVm.RelevantAuthorityName} - updated successfully";

            return View(raVm);
        }

        public ActionResult Export()
        {
            var exportViewModel = new ExportViewModel
            {
                StageList = BuildStageList(),
                StageYearList = BuildStageYearList()
            };

            return View(exportViewModel);
        }

        public ActionResult ExportHistory()
        {
            var exportListViewModel = new ExportListViewModel
            {
                Exports = _adminService.GetAllExports()
            };

            return View(exportListViewModel);
        }

        public ActionResult Templates(int exportId)
        {
            var templates = _adminService.GetTemplatesByExportId(exportId);
            return View(templates);
        }

        public ActionResult DownloadAllTemplates()
        {
            using (var zip = new ZipFile())
            {
                var filePath = ConfigurationManager.AppSettings["DownloadTemplateFilePath"];
                zip.AddDirectory(filePath);

                var output = new MemoryStream();
                zip.Save(output);
                return File(output.ToArray(), "application/zip", "Templates.zip");
            }
        }

        public ActionResult Year()
        {
            var yearViewModel = new YearViewModel { YearList = _adminService.GetAllYears() };
            return View(yearViewModel);
        }

        [HttpPost]
        public ActionResult Year(YearViewModel yearViewModel)
        {
            ViewBag.ShowSuccess = "false";
            var newYear = yearViewModel.NewYear ?? 0;

            var yearList = _adminService.GetAllYears();
            yearViewModel.YearList = yearList;

            if (!ModelState.IsValid)
            {
                return View(yearViewModel);
            }

            var allYears = yearList.Select(x => x.Item1).ToList();

            if (allYears.Contains(newYear))
            {
                foreach (var modelStateValue in ModelState.Values)
                {
                    modelStateValue.Errors.Clear();
                }

                ModelState.AddModelError("", $"{newYear} already exists - it cannot be added twice");
                return View(yearViewModel);
            }

            _adminService.AddYear(newYear, _currentUserId);
            yearViewModel.YearList = _adminService.GetAllYears();

            ViewBag.ShowSuccess = "true";
            ViewBag.SuccessMessage = $"{yearViewModel.NewYear} - created successfully";

            return View(yearViewModel);
        }

        [HttpPost]
        public ActionResult Export(ExportViewModel exportViewModel)
        {
            exportViewModel.StageList = BuildStageList();
            exportViewModel.StageYearList = BuildStageYearList();

            if (!ModelState.IsValid)
            {
                return View(exportViewModel);
            }

            _adminService.QueueTemplateExport(exportViewModel.SelectedStage, exportViewModel.SelectedStageYear, _currentUserId);

            ViewBag.ShowSuccess = "true";
            ViewBag.SuccessMessage = "Templates will be available for circulation within twenty minutes";

            return View(exportViewModel);
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            ViewBag.ShowSuccess = "false";
            if (!ModelState.IsValid)
            {
                registerViewModel.PermissionList = BuildPermissionList();
                return View(registerViewModel);
            }

            var result = _accountService.RegisterUser(registerViewModel.Email, registerViewModel.Password, registerViewModel.FirstName, registerViewModel.LastName, registerViewModel.SelectedPermission);

            if (result.Success)
            {
                ViewBag.ShowSuccess = "true";
                ViewBag.SuccessMessage = $"{registerViewModel.Email} - created successfully";

                registerViewModel.PermissionList = BuildPermissionList();
                return View(registerViewModel);
            }

            foreach (var modelStateValue in ModelState.Values)
            {
                modelStateValue.Errors.Clear();
            }

            ModelState.AddModelError("", result.Message);

            registerViewModel.PermissionList = BuildPermissionList();
            return View(registerViewModel);
        }

        public ActionResult DeleteUser(int userId)
        {
            if (SecurityHelper.IsAdmin())
            {
                _adminService.DeleteUser(userId);
            }

            return RedirectToAction("Users");
        }

        private ActionResult GetRelevantAuthoritiesViewModel(int? page, string currentFilter, string searchString)
        {
            var pageSize = ConfigurationManager.AppSettings["PageSize"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) : 20;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var raList = _adminService.GetRelevantAuthorityList(page ?? 1, pageSize, searchString);

            var pagenumber = (page ?? 1) - 1;
            var totalCount = raList.TotalCount;

            var pagedRas = new StaticPagedList<RelevantAuthority>(raList.RelevantAuthorities, pagenumber + 1, pageSize, totalCount);
            return View("RelevantAuthorities", pagedRas);
        }

        private List<SelectListItem> BuildPermissionList()
        {
            var role = _accountService.GetUserRole(_currentUserId);
            var list = new List<SelectListItem>();

            if (role == Role.Administrator)
            {
                var adminSi = new SelectListItem
                {
                    Text = Role.BudgetAdministrator.GetDescription(),
                    Value = ((int)Role.BudgetAdministrator).ToString()
                };
                list.Add(adminSi);
            }

            var si = new SelectListItem
            {
                Text = Role.BudgetViewer.GetDescription(),
                Value = ((int)Role.BudgetViewer).ToString()
            };
            list.Add(si);


            list.Insert(0, new SelectListItem { Text = "Please Select", Value = "-1" });
            return list;
        }

        private List<SelectListItem> BuildStageYearList()
        {
            var years = _adminService.GetAllStageYears();

            var list = years.Select(year => new SelectListItem
            {
                Text = year.ToString(),
                Value = year.ToString()
            })
                .ToList();

            list.Insert(0, new SelectListItem { Text = "Please Select", Value = "-1" });
            return list;
        }

        private List<SelectListItem> BuildStageList()
        {
            var list = new List<SelectListItem>();

            var si = new SelectListItem
            {
                Text = Stage.Spu.GetDescription(),
                Value = ((int)(Stage.Spu)).ToString()
            };

            list.Add(si);

            si = new SelectListItem
            {
                Text = Stage.Budget.GetDescription(),
                Value = ((int)(Stage.Budget)).ToString()
            };

            list.Add(si);

            list.Insert(0, new SelectListItem { Text = "Please Select", Value = "-1" });
            return list;
        }

        private ActionResult GetUserListViewModel(int? page, string currentFilter, string searchString)
        {
            var pageSize = ConfigurationManager.AppSettings["PageSize"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) : 20;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var userList = _adminService.GetUsers(page ?? 1, pageSize, searchString);

            var userListVm = userList.Users.Select(user => new UserViewModel
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = user.Role
            }).ToList();


            var pagenumber = (page ?? 1) - 1;
            var totalCount = userList.TotalCount;

            var pagedRas = new StaticPagedList<UserViewModel>(userListVm, pagenumber + 1, pageSize, totalCount);
            return View("Users", pagedRas);
        }
    }
}