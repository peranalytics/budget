﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Databank.Extensions;
using Databank.Helpers;
using Databank.Models;
using PagedList;
using Services.Enum;
using Services.Models;
using Services.Services;

namespace Databank.Controllers
{
    [Authorize]
    public class FileHistoryController : Controller
    {
        private readonly AdminService _adminService;
        private readonly MetadataService _metdataService;

        public FileHistoryController()
        {
            _adminService = new AdminService();
            _metdataService = new MetadataService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Budget(int? page, string currentFilter, string searchString)
        {
            ViewBag.Stage = Stage.Budget;
            return GetFileHistoryListViewModel(page, Stage.Budget, currentFilter, searchString);
        }

        public ActionResult Spu(int? page, string currentFilter, string searchString)
        {
            ViewBag.Stage = Stage.Spu;
            return GetFileHistoryListViewModel(page, Stage.Spu, currentFilter, searchString);
        }

        public ActionResult StageDetails(int relevantAuthorityId, Stage stage)
        {
            var stageDetailsViewModel = GetStageDetailsViewModel(relevantAuthorityId, stage);
            return View(stageDetailsViewModel);
        }

        public ActionResult FileManager(int relevantAuthorityId, Stage stage, int year)
        {
            var relevantAuthority = _adminService.GetRelevantAuthorityById(relevantAuthorityId);

            var fileManagerVm = new FileManagerViewModel
            {
                RelevantAuthority = relevantAuthority,
                Year = year,
                Stage = stage
            };

            var metadataList = _metdataService.GetMetadataByRelevantAuthority(relevantAuthorityId);
            var enumerable = metadataList as IList<Metadata> ?? metadataList.ToList();

            if (!enumerable.Any())
            {
                return View(fileManagerVm);
            }

            var filteredByStageList = enumerable.Where(x => x.Stage == stage.GetDescription()).ToList();
            var filteredByYearList = filteredByStageList.Where(x => x.Year == year).ToList();

            fileManagerVm.MetadataList = filteredByYearList;

            return View(fileManagerVm);
        }

        private ActionResult GetFileHistoryListViewModel(int? page, Stage stage, string currentFilter, string searchString)
        {
            var pageSize = ConfigurationManager.AppSettings["PageSize"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]) : 20;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            var raList = _adminService.GetRelevantAuthorityList(page ?? 1, pageSize, searchString);

            if (raList.TotalCount == 1)
            {
                var ra = raList.RelevantAuthorities.FirstOrDefault();

                if (ra != null)
                {
                    return RedirectToAction("StageDetails", "FileHistory", new { relevantAuthorityId = ra.RelevantAuthorityId, stage });
                }
            }

            var metadataList = _metdataService.GetMetadata().ToList();

            var fileHistoryVmList = raList.RelevantAuthorities.Select(ra => new FileHistoryViewModel
            {
                Metadata = metadataList.Where(x => x.RelevantAuthority.RelevantAuthorityId == ra.RelevantAuthorityId && x.Stage == stage.ToString()),
                RelevantAuthority = ra
            });

            var pagenumber = (page ?? 1) - 1;
            var totalCount = raList.TotalCount;

            var pagedRas = new StaticPagedList<FileHistoryViewModel>(fileHistoryVmList, pagenumber + 1, pageSize, totalCount);
            return View("Stage", pagedRas);
        }

        private StageDetailsViewModel GetStageDetailsViewModel(int relevantAuthorityId, Stage stage)
        {
            var stageDetailsViewModel = new StageDetailsViewModel
            {
                FileHistoryViewModel = GetFileHistoryViewModel(relevantAuthorityId, stage),
                LargeChartViewModel = GetChartViewModel(relevantAuthorityId, stage),
                Stage = stage,
                FileLogs = GetFileLogs(relevantAuthorityId, stage),
            };

            return stageDetailsViewModel;
        }

        private FileHistoryViewModel GetFileHistoryViewModel(int relevantAuthorityId, Stage stage)
        {
            return new FileHistoryViewModel
            {
                Metadata = _metdataService.GetMetadata(relevantAuthorityId: relevantAuthorityId).Where(x => x.Stage == stage.GetDescription()).ToList(),
                RelevantAuthority = _adminService.GetRelevantAuthorityById(relevantAuthorityId)
            };
        }

        private IEnumerable<FileLog> GetFileLogs(int relevantAuthorityId, Stage stage)
        {
            var filelogs = _metdataService.ReadLogFileDownload(relevantAuthorityId).ToList();

            if (filelogs.Any())
            {
                var filteredFileLogs = filelogs.Where(x => x.Stage == stage);
                var fileLogs = filteredFileLogs as IList<FileLog> ?? filteredFileLogs.ToList();

                if (fileLogs.Any())
                {
                    return fileLogs;
                }
            }

            return new List<FileLog>();
        }


        private static ChartViewModel GetChartViewModel(int relevantAuthorityId, Stage stage)
        {
            var stageYearDefaultCount = ConfigurationManager.AppSettings["StageYearDefaultCount"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["StageYearDefaultCount"]) : 8;
            var startYear = (DateTime.Now.Year - stageYearDefaultCount) < 2013 ? 2013 : (DateTime.Now.Year - stageYearDefaultCount); // 2013 year SPS started - Will show last x years always

            var chartHelper = new ChartHelper();
            return chartHelper.BuildFileStatsBySchemeChartViewModel(startYear, stage, relevantAuthorityId, "100%", "400");
        }

    }
}