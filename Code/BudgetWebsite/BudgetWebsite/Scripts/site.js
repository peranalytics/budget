﻿$(function () {
    $('.toggle-menu').click(function () {
        $('.exo-menu').toggleClass('display');
    });
});

// dropdown
$('.user-area > span').on("click", function () {
    $('.dropdwn').toggleClass('active');
    return false;
});

$('body,html').on("click", function () {
    $('.dropdwn').removeClass('active');

});

$('#checkAllGroups').click(function () {
    $('.groups > input:checkbox').prop('checked', this.checked);
});

$('#checkAllRoles').click(function () {
    $('.roles > input:checkbox').prop('checked', this.checked);
});

$('#checkAllReports').click(function () {
    $('.reports > input:checkbox').prop('checked', this.checked);
});

$('#checkAllUsers').click(function () {
    $('.subs > input:checkbox').prop('checked', this.checked);
});

$("#newUsername").change(function () {
    var sgId = -1;
    var securitySourceId = $("#SecuritySourceId").val();

    $.post("GetUserSecurityData", { username: $(this).val(), sourceId: securitySourceId }, function (data) {
        if (data.Success === "true") {
            var sgId = data.SecurityGroupId;

            if (sgId > -1) {
                $(".grouplist").val(data.SecurityGroupId);

                $("#securityGroupDetailsSection").html(data.SecurityPartialView);
                $("#securityGroupAccessDetailsSection").html(data.AccessPartialView);
            }
        }
        else {
            $(".grouplist").val(-1);
            $("#securityGroupDetailsSection").html("<div class=\"unassigned-user\">This user is currently unassigned</div>");
            $("#securityGroupAccessDetailsSection").html("");

        }
    });
    //handle ajax response here
});

$(".grouplist").change(function () {

    $.post("GetSecurityData", { securityGroupId: $(this).val() }, function (data) {
        if (data.Success === "true") {
            $("#securityGroupDetailsSection").html(data.SecurityPartialView);
            $("#securityGroupAccessDetailsSection").html(data.AccessPartialView);
        }
    });
    //handle ajax response here
});

$("#Users").change(function () {
    console.log("Users");

    $.post("CheckUserSecurityData", { username: $(this).val() }, function (data) {
        if (data.Success === "true") {
            //var sgId = data.SecurityGroupId;

            //  if (sgId > -1) {
            $(".grouplist").val(data.SecurityGroupId);

            $("#securityGroupDetailsSection").html(data.SecurityPartialView);
            //$("#securityGroupAccessDetailsSection").html(data.AccessPartialView);
            //}
        }
        else {
            $(".grouplist").val(-1);
            $("#securityGroupDetailsSection").html("<div class=\"unassigned-user\">This user is currently unassigned</div>");
            $("#securityGroupAccessDetailsSection").html("");

        }
    });
});


$("#deleteUser").change(function () {

    $.post("CheckUserSecurityData", { username: $(this).val() }, function (data) {
        if (data.Success === "true") {
            //var sgId = data.SecurityGroupId;

            //  if (sgId > -1) {
            $(".grouplist").val(data.SecurityGroupId);

            $("#securityGroupDetailsSection").html(data.SecurityPartialView);
            //$("#securityGroupAccessDetailsSection").html(data.AccessPartialView);
            //}
        }
        else {
            $(".grouplist").val(-1);
            $("#securityGroupDetailsSection").html("<div class=\"unassigned-user\">This user is currently unassigned</div>");
            $("#securityGroupAccessDetailsSection").html("");

        }
    });
});




$("#RemoveUsername").change(function () {
    var sgId = -1;
    var securitySourceId = $("#SecuritySourceId").val();

    $.post("GetUserSecurityData", { username: $(this).val(), sourceId: securitySourceId }, function (data) {
        if (data.Success === "true") {
            var sgId = data.SecurityGroupId;

            if (sgId > -1) {
                $("#securityGroupDetailsSection").html(data.SecurityPartialView);
                $("#securityGroupAccessDetailsSection").html(data.AccessPartialView);
            }
        }
        else {
            $(".grouplist").val(-1);
            $("#securityGroupDetailsSection").html("<div class=\"unassigned-user\">This user is currently unassigned</div>");
            $("#securityGroupAccessDetailsSection").html("");

        }
    });
    //handle ajax response here
});