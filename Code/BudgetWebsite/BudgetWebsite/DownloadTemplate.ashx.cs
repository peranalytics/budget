﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Services.Services;

namespace Databank
{
    /// <summary>
    /// Summary description for DownloadTemplate
    /// </summary>
    public class DownloadTemplate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.IsAuthenticated)
            {
                Download(context);
            }

            RedirectToHome(context);
        }

        public bool IsReusable => false;

        private static void Download(HttpContext context)
        {
            int.TryParse(context.Request.QueryString["tid"], out var templateId);

            if (templateId == 0)
            {
                return;
            }

            var adminService = new AdminService();
            var template = adminService.GetTemplateById(templateId);

            if (template == null)
            {
                return;
            }

            var filePath = ConfigurationManager.AppSettings["DownloadTemplateFilePath"];
            var fullFilePath = $"{filePath}{template.FileName}";

            context.Response.Clear();
            context.Response.ContentType = "application/octet-stream";
            context.Response.AddHeader("Content-Disposition", $"attachment; filename={template.FileName}");
            context.Response.WriteFile(fullFilePath);
            context.Response.End();
        }

        private static void RedirectToHome(HttpContext context)
        {
            var urlHelper = new UrlHelper(context.Request.RequestContext);
            var homeUrl = urlHelper.Action("Index", "Home", null);
            context.Response.Redirect(homeUrl);
        }
    }

}