﻿using Models.Enums;

namespace Models.Models
{
    public class DataloadResult
    {
        public DataloadStatus Status { get; set; }

        public string Message { get; set; }
    }
}
