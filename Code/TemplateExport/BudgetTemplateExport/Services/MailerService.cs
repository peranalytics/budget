﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using ReportExporter.Services.Interfaces;

namespace ReportExporter.Services
{
    public class MailerService : IMailerService
    {
        public void SendErrorEmail(Exception exception)
        {
            var smtpClient = new SmtpClient(GlobalSettings.SmtpServer)
            {
                Port = GlobalSettings.SmtpPortNumber,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false
            };

            var body = GenerateErrorEmailBody(exception);

            var mail = new MailMessage(GlobalSettings.EmailFromAddress, GlobalSettings.ErrorEmailToAddress)
            {
                Subject = GlobalSettings.ErrorEmailSubject,
                Body = body,
                IsBodyHtml = true
            };
            smtpClient.Send(mail);
        }

        private string GenerateErrorEmailBody(Exception ex)
        {
            var log = ReadCurrentLog();
            var stringbuilder = new StringBuilder();

            stringbuilder.Append($"<h2>{GlobalSettings.ErrorEmailScheme}</h2>");
            stringbuilder.Append("<h3>Reporting Solution - File Import</h3>");

            stringbuilder.Append($"<b>Exception Message:</b><br/>{ex.Message}<br/><br/>");

            if (ex.InnerException != null) stringbuilder.Append($"<b>Inner Exception:</b><br/>{ex.InnerException.Message}<br/><br/>");

            stringbuilder.Append($"<b>Stack Trace:</b><br/>{ex.StackTrace}<br/><br/>");
            stringbuilder.Append($"<b>Logs:</b><br/>{log.Replace("\r\n", "<br/>")}");

            return stringbuilder.ToString();
        }

        private string ReadCurrentLog()
        {
            var rootAppender = ((Hierarchy)log4net.LogManager.GetRepository())
                .Root.Appenders.OfType<FileAppender>()
                .FirstOrDefault();

            var filename = rootAppender != null ? rootAppender.File : string.Empty;

            var stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var streamReader = new StreamReader(stream);

            return streamReader.ReadToEnd();
        }
    }
}
