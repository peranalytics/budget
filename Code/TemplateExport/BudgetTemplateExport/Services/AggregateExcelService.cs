﻿using System;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ReportExporter.DataAccess.Services;
using ReportExporter.Models;
using System.Data;


namespace ReportExporter.Services
{
    public class AggregateExcelService
    {
        private readonly ExportDataAccessService _exportDataAccessService;
        public string ExcelFilePath { get; set; } = string.Empty;
        private readonly log4net.ILog _log;

        public AggregateExcelService(log4net.ILog log)
        {
            _log = log;
            _exportDataAccessService = new ExportDataAccessService(_log);
        }

        public void PopulateSheet(AggregateAnalytics validatedDs, AggregateAnalytics unvalidatedDs,  int year)
        {
            using (var spreadSheet = SpreadsheetDocument.Open(ExcelFilePath, true))
            {
                // Get the SharedStringTablePart. If it does not exist, create a new one.
                SharedStringTablePart shareStringPart;
                if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Any())
                {
                    shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                }
                else
                {
                    shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                }

                var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "A - To be validated");

                if (!sheets.Any())
                {
                    // The specified worksheet does not exist.
                    return;
                }

                var relationshipId = sheets.First().Id.Value;
                var worksheetPart = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(relationshipId);

                // Insert cell A1 into the new worksheet.
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Headcount.ToString(CultureInfo.InvariantCulture), "B", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Headcount.ToString(CultureInfo.InvariantCulture), "C", 6);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Fte.ToString(CultureInfo.InvariantCulture), "B", 7);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Fte.ToString(CultureInfo.InvariantCulture), "C", 7);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.AverageAge.ToString(CultureInfo.InvariantCulture), "B", 8);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.AverageAge.ToString(CultureInfo.InvariantCulture), "C", 8);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Probationers.ToString(CultureInfo.InvariantCulture), "B", 9);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Probationers.ToString(CultureInfo.InvariantCulture), "C", 9);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Attrition.ToString(CultureInfo.InvariantCulture), "B", 12);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Attrition.ToString(CultureInfo.InvariantCulture), "C", 12);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Retirement.ToString(CultureInfo.InvariantCulture), "B", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Retirement.ToString(CultureInfo.InvariantCulture), "C", 13);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Turnover.ToString(CultureInfo.InvariantCulture), "B", 14);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Turnover.ToString(CultureInfo.InvariantCulture), "C", 14);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.InvoluntaryTerminations.ToString(CultureInfo.InvariantCulture), "B", 15);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.InvoluntaryTerminations.ToString(CultureInfo.InvariantCulture), "C", 15);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.InvoluntaryTerminationsProbationers.ToString(CultureInfo.InvariantCulture), "C", 16);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.InvoluntaryTerminationsProbationers.ToString(CultureInfo.InvariantCulture), "C", 16);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.Promotions.ToString(CultureInfo.InvariantCulture), "B", 19);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Promotions.ToString(CultureInfo.InvariantCulture), "C", 19);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.WorkSharing.ToString(CultureInfo.InvariantCulture), "B", 22);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.WorkSharing.ToString(CultureInfo.InvariantCulture), "C", 22);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.ShorterWorkingYear.ToString(CultureInfo.InvariantCulture), "B", 23);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ShorterWorkingYear.ToString(CultureInfo.InvariantCulture), "C", 23);

                worksheetPart.Worksheet.Save();

                sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "B - To be validated");

                if (!sheets.Any())
                {
                    // The specified worksheet does not exist.
                    return;
                }

                relationshipId = sheets.First().Id.Value;
                worksheetPart = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(relationshipId);

                // New Entrants 
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.Hires.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 5);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 6);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.Hires.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 6);

                //ReReHires
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 12);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReHires.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 12);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 13);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.ReHires.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 13);

                //MaleGradeByGender
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 19);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.MaleGradeByGender.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 19);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 20);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.MaleGradeByGender.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 20);

                //FemaleGradeByGender
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 26);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.FemaleGradeByGender.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 26);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "SEC").Value.ToString(CultureInfo.InvariantCulture), "B", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "DS").Value.ToString(CultureInfo.InvariantCulture), "C", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "ASC").Value.ToString(CultureInfo.InvariantCulture), "D", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "PO").Value.ToString(CultureInfo.InvariantCulture), "E", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "AP").Value.ToString(CultureInfo.InvariantCulture), "F", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "HEO").Value.ToString(CultureInfo.InvariantCulture), "G", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "AO").Value.ToString(CultureInfo.InvariantCulture), "H", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "EO").Value.ToString(CultureInfo.InvariantCulture), "I", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "SO").Value.ToString(CultureInfo.InvariantCulture), "J", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "CO").Value.ToString(CultureInfo.InvariantCulture), "K", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "TCO").Value.ToString(CultureInfo.InvariantCulture), "L", 27);
                UpdateCell(shareStringPart, worksheetPart, unvalidatedDs.FemaleGradeByGender.First(x => x.Key == "UKN").Value.ToString(CultureInfo.InvariantCulture), "M", 27);

                worksheetPart.Worksheet.Save();

                sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "C - To be provided");

                if (!sheets.Any())
                {
                    // The specified worksheet does not exist.
                    return;
                }

                relationshipId = sheets.First().Id.Value;
                worksheetPart = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(relationshipId);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.LdHoursPerFte.ToString(CultureInfo.InvariantCulture), "B", 8);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberPips.ToString(CultureInfo.InvariantCulture), "B", 9);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.ReversionOfProbationers.ToString(CultureInfo.InvariantCulture), "B", 10);


                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberFlexitime.ToString(CultureInfo.InvariantCulture), "B", 13);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberRemoteWorking.ToString(CultureInfo.InvariantCulture), "B", 14);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberWellnessProgrammes.ToString(CultureInfo.InvariantCulture), "B", 17);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrAttendingBoardMeetingsYes.ToString(CultureInfo.InvariantCulture), "C", 20);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrAttendingBoardMeetingsNo.ToString(CultureInfo.InvariantCulture), "E", 20);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.PeopleStrategiesInPlaceYes.ToString(CultureInfo.InvariantCulture), "C", 21);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.PeopleStrategiesInPlaceNo.ToString(CultureInfo.InvariantCulture), "E", 21);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.ProfessionalHRPercentage.ToString(CultureInfo.InvariantCulture), "B", 22);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrCostPerEmployee.ToString(CultureInfo.InvariantCulture), "B", 23);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberOfHrStaff.ToString(CultureInfo.InvariantCulture), "B", 24);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrExperienceByFte0To3.ToString(CultureInfo.InvariantCulture), "B", 27);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrExperienceByFte3To5.ToString(CultureInfo.InvariantCulture), "B", 28);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrExperienceByFte5To10.ToString(CultureInfo.InvariantCulture), "B", 29);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.HrExperienceByFte10Plus.ToString(CultureInfo.InvariantCulture), "B", 30);

                worksheetPart.Worksheet.Save();

                sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "D - For Information");

                if (!sheets.Any())
                {
                    // The specified worksheet does not exist.
                    return;
                }

                relationshipId = sheets.First().Id.Value;
                worksheetPart = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(relationshipId);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.LDEesResults.ToString(), "B", 5);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.PerformanceStandardsEesResults.ToString(), "B", 6);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.CareerDevelopmentAndMobilityEesResults.ToString(), "B", 7);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.EmployeeEngagementEeesResults.ToString(), "B", 8);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.WellbeingEesResults.ToString(), "B", 9);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.InnovationClimateEesResults.ToString(), "B", 10);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.InvolvmentClimateEesResults.ToString(), "B", 11);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberCivilServantsCompledEes.ToString(), "B", 12);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.AbsenceRatePercent.ToString(CultureInfo.InvariantCulture), "B", 15);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.AbsenseCostPerFte.ToString(CultureInfo.InvariantCulture), "B", 16);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.PmdsCompliance.ToString(CultureInfo.InvariantCulture), "B", 20);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.PmdsSatisfactory.ToString(CultureInfo.InvariantCulture), "B", 21);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.PmdsNotSatisfactory.ToString(CultureInfo.InvariantCulture), "B", 22);

                UpdateCell(shareStringPart, worksheetPart, validatedDs.PeopleWithDisabilities.ToString(CultureInfo.InvariantCulture), "B", 25);
                UpdateCell(shareStringPart, worksheetPart, validatedDs.NumberOfPlacementsThroughAhead.ToString(CultureInfo.InvariantCulture), "B", 26);

                worksheetPart.Worksheet.Save();


                // Save the new worksheet.
                spreadSheet.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                spreadSheet.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
            }
        }

        private void UpdateCell(SharedStringTablePart shareStringPart, WorksheetPart worksheetPart, string text, string column, uint row)
        {
            var index = InsertSharedStringItem(text, shareStringPart);

            var cell = InsertCellInWorksheet(column, row, worksheetPart);

            // Set the value of cell A1.
            cell.CellValue = new CellValue(index.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
        }

        // Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
        // and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
        private static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            var i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (var item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        // Given a WorkbookPart, inserts a new worksheet.
        private static WorksheetPart InsertWorksheet(WorkbookPart workbookPart)
        {
            // Add a new worksheet part to the workbook.
            var newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            var sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            var relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new sheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Any())
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            var sheetName = "Sheet" + sheetId;

            // Append the new worksheet and associate it with the workbook.
            var sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();
            var cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Count(r => r.RowIndex == rowIndex) != 0)
            {
                row = sheetData.Elements<Row>().First(r => r.RowIndex == rowIndex);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Any(c => c.CellReference.Value == columnName + rowIndex))
            {
                return row.Elements<Cell>().First(c => c.CellReference.Value == cellReference);
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (var cell in row.Elements<Cell>())
                {
                    if (cell.CellReference.Value.Length == cellReference.Length)
                    {
                        if (string.Compare(cell.CellReference.Value, cellReference, StringComparison.OrdinalIgnoreCase) > 0)
                        {
                            refCell = cell;
                            break;
                        }
                    }
                }

                var newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
    }
}
