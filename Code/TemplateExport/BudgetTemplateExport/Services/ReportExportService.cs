﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ReportExporter.DataAccess.Services;
using ReportExporter.Models;
using ReportExporter.Services.Interfaces;

namespace ReportExporter.Services
{
    public class ReportExportService : IReportExportService
    {
        private readonly log4net.ILog _log;
        private readonly ExportDataAccessService _exportDataAccessService;
        private readonly string _fileFolder;
        private readonly string _exportFolder;


        public ReportExportService(log4net.ILog log)
        {
            _log = log;
            _exportDataAccessService = new ExportDataAccessService(_log);
            _exportFolder = $"{GlobalSettings.FileFolder}{GlobalSettings.ExportFolder}";
            _fileFolder = $"{GlobalSettings.FileFolder}";
        }

        public void StartExport()
        {
            StartTemplateExport();
        }

        private void StartTemplateExport()
        {
            _log.Info("Export Reports - Start");
            ExportReports();
            _log.Info("Export Reports - Finish");

            ArchiveLogFiles();
            _log.Info("Log files archived");
        }

        public void ExportReports()
        {
            var exportList = _exportDataAccessService.GetExportList().ToList();

            if (!exportList.Any())
            {
                _log.Info("No Reports to Export");
                return;
            }

            var exportProperties = _exportDataAccessService.GetExportProperties();

            if (exportProperties == null)
            {
                _log.Info("No Properties Queued to Export");
                return;
            }

            _log.Info("Clear queued folder");
            ClearQueuedFolder();

            foreach (var export in exportList)
            {
                _log.Info(
                    $"Exporting Reports for: Department - {export.RelevantAuthorityId} Year - {exportProperties.StageYear} Stage - {exportProperties.Stage}");
                ExportReports(export, exportProperties);
                _log.Info(
                    $"Reports Exported for: Department - {export.RelevantAuthorityId} Year - {exportProperties.StageYear} Stage - {exportProperties.Stage}");

                _log.Info("Report archived");
            }

            _log.Info("Mark as Exported - Start");
            _exportDataAccessService.ExportQueueMarkAsExported(exportProperties);
            _log.Info("Mark as Exported - Finish");
        }

        private void ExportReports(Export export, ExportProperties exportProperties)
        {
            var templateFile = $"{_fileFolder}{GlobalSettings.TemplateFileName}";
            var exportFileName =
                $"GGS_{export.ProviderCode}_{export.RelevantAuthorityName.Replace(" ", "").Replace("&", "And").Replace("'", "").Replace(",", "")}_{exportProperties.StageYear}_{exportProperties.Stage}_{DateTime.Now:yyyMMdd}.xlsx";

            _log.Info($"Export file start - {exportFileName}");
            var newFileName = $"{_exportFolder}{exportFileName}";

            File.Copy(templateFile, newFileName);

            var excelService = new ExcelTemplateService(_log)
            {
                ExcelFilePath = newFileName,
            };

            _log.Info("Populate file - Start");
            excelService.PopulateSheet(export, exportProperties.StageYear, exportProperties.Stage);
            _log.Info("Populate file - Finish");

            _exportDataAccessService.AddExportFileHistory(exportProperties, exportFileName);
            _log.Info("Export file - Finish");
        }

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }

        private void ClearQueuedFolder()
        {
            var info = new DirectoryInfo(_exportFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                return;
            }

            _log.Info("Clearing Export Directory before Start");

            foreach (var queuedFile in files)
            {
                queuedFile.Delete();
            }
        }
    }
}



