﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ReportExporter.DataAccess.Services;
using ReportExporter.Models;

namespace ReportExporter.Services
{
    public class ExcelTemplateService
    {
        private readonly ExportDataAccessService _exportDataAccessService;
        public string ExcelFilePath { get; set; } = string.Empty;
        private readonly log4net.ILog _log;

        public ExcelTemplateService(log4net.ILog log)
        {
            _log = log;
            _exportDataAccessService = new ExportDataAccessService(_log);
        }

        public void PopulateSheet(Export export, int stageYear, string stage)
        {
            using (var spreadSheet = SpreadsheetDocument.Open(ExcelFilePath, true))
            {
                // Get the SharedStringTablePart. If it does not exist, create a new one.
                SharedStringTablePart shareStringPart;
                if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Any())
                {
                    shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                }
                else
                {
                    shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                }

                var sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Sheet1");

                if (!sheets.Any())
                {
                    // The specified worksheet does not exist.
                    return;
                }

                var relationshipId = sheets.First().Id.Value;
                var worksheetPart = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(relationshipId);

                UpdateCell(shareStringPart, worksheetPart, export.RelevantAuthorityName, "C", 2);
                UpdateCell(shareStringPart, worksheetPart, export.RelevantAuthorityId.ToString(), "C", 3);
                UpdateCell(shareStringPart, worksheetPart, stage, "C", 4);
                UpdateCell(shareStringPart, worksheetPart, export.ProviderCode, "C", 5);
                UpdateCell(shareStringPart, worksheetPart, export.ProviderId.ToString(), "C", 6);
                UpdateCell(shareStringPart, worksheetPart, stageYear.ToString(), "C", 7);

                var subTitle = $"A. Receipts & Expenditure of {export.RelevantAuthorityName}";
                UpdateCell(shareStringPart, worksheetPart, subTitle, "B", 12);

                var years = new List<int>();
                var t = stage.ToLower() == "budget" ? stageYear - 2 : stageYear - 1;

                for (var i = t; i < (t + 8); i++)
                {
                    years.Add(i);
                }

                UpdateCell(shareStringPart, worksheetPart, years[0].ToString(), "C", 14);
                UpdateCell(shareStringPart, worksheetPart, years[1].ToString(), "D", 14);
                UpdateCell(shareStringPart, worksheetPart, years[2].ToString(), "E", 14);
                UpdateCell(shareStringPart, worksheetPart, years[3].ToString(), "F", 14);
                UpdateCell(shareStringPart, worksheetPart, years[4].ToString(), "G", 14);
                UpdateCell(shareStringPart, worksheetPart, years[5].ToString(), "H", 14);
                UpdateCell(shareStringPart, worksheetPart, years[6].ToString(), "I", 14);
                UpdateCell(shareStringPart, worksheetPart, years[7].ToString(), "J", 14);


                worksheetPart.Worksheet.Save();

                // Save the new worksheet.
                spreadSheet.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                spreadSheet.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
            }
        }

        private void UpdateCell(SharedStringTablePart shareStringPart, WorksheetPart worksheetPart, string text, string column, uint row)
        {
            var index = InsertSharedStringItem(text, shareStringPart);

            var cell = InsertCellInWorksheet(column, row, worksheetPart);

            // Set the value of cell A1.
            cell.CellValue = new CellValue(index.ToString());
            cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
        }

        // Given text and a SharedStringTablePart, creates a SharedStringItem with the specified text 
        // and inserts it into the SharedStringTablePart. If the item already exists, returns its index.
        private static int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            var i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (var item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();
            var cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Count(r => r.RowIndex == rowIndex) != 0)
            {
                row = sheetData.Elements<Row>().First(r => r.RowIndex == rowIndex);
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Any(c => c.CellReference.Value == columnName + rowIndex))
            {
                return row.Elements<Cell>().First(c => c.CellReference.Value == cellReference);
            }

            // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
            Cell refCell = null;
            foreach (var cell in row.Elements<Cell>())
            {
                if (cell.CellReference.Value.Length == cellReference.Length)
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }
            }

            var newCell = new Cell() { CellReference = cellReference };
            row.InsertBefore(newCell, refCell);

            worksheet.Save();
            return newCell;
        }
    }
}
