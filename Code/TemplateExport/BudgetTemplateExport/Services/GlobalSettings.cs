﻿using System;
using System.Configuration;

namespace ReportExporter.Services
{
    public static class GlobalSettings
    {
        public static string FileFolder => ConfigurationManager.AppSettings["FileFolder"];

        public static string ArchiveFileFolder => ConfigurationManager.AppSettings["ArchiveFileFolder"];

        public static string ExportFolder => ConfigurationManager.AppSettings["ExportFolder"];

        public static string TemplateFileName => ConfigurationManager.AppSettings["TemplateFileName"];

        public static string AggregateTemplateFileName => ConfigurationManager.AppSettings["AggregateTemplateFileName"];

        public static string ErrorEmailToAddress => ConfigurationManager.AppSettings["ErrorEmailToAddress"];

        public static string EmailFromAddress => ConfigurationManager.AppSettings["EmailFromAddress"];

        public static string SmtpServer => ConfigurationManager.AppSettings["SmtpServer"];

        public static int SmtpPortNumber => Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNumber"]);

        public static bool SendEmail => Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]);

        public static string ErrorEmailSubject => ConfigurationManager.AppSettings["ErrorEmailSubject"];

        public static string ErrorEmailScheme => ConfigurationManager.AppSettings["ErrorEmailScheme"];

        public static string LogFileFolder => ConfigurationManager.AppSettings["LogFileFolder"];

        public static int LogDaysKeep => Convert.ToInt32(ConfigurationManager.AppSettings["LogDaysKeep"]);

        public static string Process => ConfigurationManager.AppSettings["Process"];

    }
}
