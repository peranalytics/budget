﻿
namespace ReportExporter.Services.Interfaces
{
    public interface IReportExportService
    {
        void StartExport();
    }
}
