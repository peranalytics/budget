﻿using System;

namespace ReportExporter.Services.Interfaces
{
    public interface IMailerService
    {
        void SendErrorEmail(Exception exception);
    }
}
