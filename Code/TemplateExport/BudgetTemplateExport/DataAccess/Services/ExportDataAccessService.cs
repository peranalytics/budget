﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using ReportExporter.Models;

namespace ReportExporter.DataAccess.Services
{
    public class ExportDataAccessService
    {
        private readonly log4net.ILog _log;

        public ExportDataAccessService(log4net.ILog log)
        {
            _log = log;
        }

        public IEnumerable<Export> GetExportList()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[GetStandardSurveysToExport]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return CovertDsToExportList(ds);
        }

        public ExportProperties GetExportProperties()
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportQueueGet]", con) { CommandType = CommandType.StoredProcedure })
                {
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return CovertDsToExportProperties(ds);
        }

        public ExportProperties AddExportFileHistory(ExportProperties exportProperties, string fileName)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportFileHistoryAdd]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@ExportId", SqlDbType.Int).Value = exportProperties.ExportId;
                    command.Parameters.Add("@ExportFileName", SqlDbType.Text).Value = fileName;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }

            return CovertDsToExportProperties(ds);
        }

        public ExportProperties ExportQueueMarkAsExported(ExportProperties exportProperties)
        {
            var ds = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[ExportQueueMarkAsExported]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@ExportId", SqlDbType.Int).Value = exportProperties.ExportId;

                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                }
            }

            return CovertDsToExportProperties(ds);
        }

        private ExportProperties CovertDsToExportProperties(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return null;
            }

            var dt = ds.Tables[0];
            var dr = dt.Rows[0];

            return new ExportProperties
            {
                Stage = dr["StageName"].ToString(),
                StageYear = Convert.ToInt32(dr["YearId"]),
                ExportId = Convert.ToInt32(dr["ExportId"])
            };
        }

        private static IEnumerable<Export> CovertDsToExportList(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count <= 0)
            {
                return new List<Export>();
            }

            var dt = ds.Tables[0];

            var exportList = new List<Export>();

            foreach (DataRow dr in dt.Rows)
            {
                var export = new Export
                {
                    ProviderId = Convert.ToInt32(dr["ProviderId"]),
                    ProviderCode = dr["ProviderCode"].ToString(),
                    RelevantAuthorityId = Convert.ToInt32(dr["RelevantAuthorityId"]),
                    RelevantAuthorityName = dr["RelevantAuthorityName"].ToString()
                };
                exportList.Add(export);
            }

            return exportList;
        }
    }
}
