﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportExporter.Models
{
    public class AggregateAnalytics
    {
        public decimal Headcount { get; set; }

        public decimal Fte { get; set; }

        public decimal AverageAge { get; set; }

        public decimal Attrition { get; set; }

        public decimal Retirement { get; set; }

        public decimal Turnover { get; set; }

        public decimal InvoluntaryTerminations { get; set; }

        public decimal InvoluntaryTerminationsProbationers { get; set; }

        public decimal Promotions { get; set; }

        public decimal PmdsCompliance { get; set; }

        public decimal PmdsSatisfactory { get; set; }

        public decimal PmdsNotSatisfactory { get; set; }

        public decimal PmdsNotCompleted { get; set; }

        public decimal LeadsCompliance { get; set; }

        public decimal LeadsSatisfactory { get; set; }

        public decimal LeadsNotSatisfactory { get; set; }

        public decimal LeadsNotCompleted { get; set; }

        public decimal WorkSharing { get; set; }

        public decimal ShorterWorkingYear { get; set; }

        public decimal Probationers { get; set; }

        public Dictionary<string, decimal> Hires { get; set; }

        public Dictionary<string, decimal> ReHires { get; set; }

        public Dictionary<string, decimal> FemaleGradeByGender { get; set; }

        public Dictionary<string, decimal> MaleGradeByGender { get; set; }

        public decimal ResignationsDuringDisciplinary { get; set; }

        public decimal LdHoursPerFte { get; set; }

        public decimal NumberPips { get; set; }

        public decimal ReversionOfProbationers { get; set; }

        public decimal NumberFlexitime { get; set; }

        public decimal NumberRemoteWorking { get; set; }

        public decimal NumberWellnessProgrammes { get; set; }

        public decimal HrAttendingBoardMeetingsYes { get; set; }

        public decimal HrAttendingBoardMeetingsNo { get; set; }

        public decimal PeopleStrategiesInPlaceYes { get; set; }

        public decimal PeopleStrategiesInPlaceNo { get; set; }

        public decimal ProfessionalHRPercentage { get; set; }

        public decimal HrCostPerEmployee { get; set; }

        public decimal NumberOfHrStaff { get; set; }

        public decimal HrExperienceByFte0To3 { get; set; }

        public decimal HrExperienceByFte3To5 { get; set; }

        public decimal HrExperienceByFte5To10 { get; set; }

        public decimal HrExperienceByFte10Plus { get; set; }

        public decimal? LDEesResults { get; set; }

        public decimal? PerformanceStandardsEesResults { get; set; }

        public decimal? CareerDevelopmentAndMobilityEesResults { get; set; }

        public decimal? EmployeeEngagementEeesResults { get; set; }

        public decimal? WellbeingEesResults { get; set; }

        public decimal? InnovationClimateEesResults { get; set; }

        public decimal? InvolvmentClimateEesResults { get; set; }

        public decimal? NumberCivilServantsCompledEes { get; set; }

        public decimal AbsenceRatePercent { get; set; }

        public decimal AbsenseCostPerFte { get; set; }

        public decimal PeopleWithDisabilities { get; set; }

        public decimal NumberOfPlacementsThroughAhead { get; set; }
    }
}
