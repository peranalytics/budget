﻿namespace ReportExporter.Models
{
    public class ExportProperties
    {
        public int ExportId { get; set; }

        public string Stage { get; set; }

        public int StageYear { get; set; }
    }
}
