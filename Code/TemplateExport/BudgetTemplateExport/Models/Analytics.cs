﻿using System.Collections.Generic;

namespace ReportExporter.Models
{
    public class Analytics
    {
        public int Headcount { get; set; }

        public decimal Fte { get; set; }

        public decimal AverageAge { get; set; }

        public int Attrition { get; set; }

        public int Retirement { get; set; }

        public int Turnover { get; set; }

        public int InvoluntaryTerminations { get; set; }

        public int Promotions { get; set; }

        public int PmdsCompliance {get; set; }

        public int PmdsSatisfactory { get; set; }

        public int PmdsNotSatisfactory { get; set; }

        public int PmdsNotCompleted { get; set; }

        public int LeadsCompliance { get; set; }

        public int LeadsSatisfactory { get; set; }

        public int LeadsNotSatisfactory { get; set; }

        public int LeadsNotCompleted { get; set; }

        public int WorkSharing { get; set; }

        public int ShorterWorkingYear { get; set; }

        public int Probationers{ get; set; }

        public List<GradeCount> Hires { get; set; }

        public List<GradeCount> ReHires { get; set; }

        public List<GradeCount> FemaleGradeByGender { get; set; }

        public List<GradeCount> MaleGradeByGender { get; set; }
    }
}
