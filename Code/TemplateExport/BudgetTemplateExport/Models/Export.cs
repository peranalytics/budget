﻿
namespace ReportExporter.Models
{
    public class Export
    {
        public int ProviderId { get; set; }

        public string ProviderCode { get; set; }

        public int RelevantAuthorityId { get; set; }

        public string RelevantAuthorityName { get; set; }
    }
}
