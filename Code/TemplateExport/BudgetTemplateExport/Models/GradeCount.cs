﻿namespace ReportExporter.Models
{
    public class GradeCount
    {
        public string Grade { get; set; }

        public int HeadCount { get; set; }
    }
}
