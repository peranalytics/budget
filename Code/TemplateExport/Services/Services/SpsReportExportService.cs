﻿using System;
using System.IO;
using System.Linq;
using Services.Interfaces;

namespace Services.Services
{
    public class SpsReportExportService : ISpsReportExportService
    {
        private readonly log4net.ILog _log;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public SpsReportExportService(log4net.ILog log)
        {
            _mailerService = new MailerService();
            _log = log;
        }

        public void StartSpsReportExport()
        {
            LoadSchemes();
            _log.Info("Finish");

            if (GlobalSettings.SendEmail)
            {
                _mailerService.SendSuccessEmail();
            }

            ArchiveLogFiles();
        }

        public void LoadSchemes()
        {
            
        }

      

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }

        private string ArchiveFile(string filePath)
        {
            var name = Path.GetFileName(filePath);
            var archiveName = $"Year_";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            var file = new FileInfo(filePath);
            file.Delete();

            file = new FileInfo(filePath);
            file.Delete();

            return archiveName;
        }
    }
}



