﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Enums;
using Newtonsoft.Json;

namespace Models.Models
{
    public class JsonDataDetails
    {
        [JsonProperty(PropertyName = "psb_number")]
        public int RelevantAuthority { get; set; }

        [JsonProperty(PropertyName = "file_name")]
        public string FileName { get; set; }

        public int Year { get; set; }

        [JsonProperty(PropertyName = "uploaded_by")]
        public string UploadedBy { get; set; }

        [JsonProperty(PropertyName = "type")]
        public FileType FileType { get; set; }

        [JsonProperty(PropertyName = "num_records")]
        public int RecordCount { get; set; }

        [JsonProperty(PropertyName = "scheme_file")]
        public string SchemeFileName { get; set; }

        public SchemeFile SchemeFile { get; set; }
    }
}
