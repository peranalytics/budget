﻿using System.Collections.Generic;

namespace Models.Models
{
    public class MetaData
    {
        public int VersionId { get; set; }

        public int RelevantAuthorityId { get; set; }

        public string RelevantAuthorityName { get; set; }

        public int ProviderId { get; set; }

        public string ProviderName { get; set; }

        public List<int> Years { get; set; }

        public string Stage { get; set; }

        public int StageYear { get; set; }

        public string UploadedFileName { get; set; }

        public string ArchivedFileName { get; set; }

        public bool Success { get; set; }
    }
}
