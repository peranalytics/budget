﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Models.Models
{
    public class JsonData
    {
        [JsonProperty(PropertyName = "id")]
        public int DataStrategyId { get; set; }

        public int VersionId { get; set; }

        [JsonProperty(PropertyName = "fields")]
        public JsonDataDetails Details { get; set; }

        public string JsonDataFileName { get; set; }
    }
}
