﻿namespace Models.Models
{
    public class ValidationResult
    {
        public bool Result { get; set; }

        public string Message { get; set; }
    }
}
