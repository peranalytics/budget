﻿using System.ComponentModel;

namespace Models.Enums
{
    public enum DataType
    {
        [Description("Receipts")]
        Receipts = 0,
        [Description("Expenditure")]
        Expenditure = 1
    }
}
