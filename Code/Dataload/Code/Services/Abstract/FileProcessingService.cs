﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Ionic.Zip;
using Models.Enums;
using Models.Models;
using Services.Extensions;
using Services.Interfaces;
using Services.Services;

namespace Services.Abstract
{
    public abstract class FileProcessingService : IFileProcessingService
    {
        private readonly IMemberService _memberService;
        private readonly IBenefitsService _benefitsService;

        protected FileProcessingService()
        {
            _memberService = new MembersService();
            _benefitsService = new BenefitsService();
        }

        public ValidationResult ProcessFile(Metadata metadata, SchemeType schemeType, string dataPath, log4net.ILog log)
        {
            log.Info("ProcessFile - File Processing Service");
            var fileData = GetFileAsDataSet(dataPath);
            log.Info("ProcessFile - GetFileAsDataSet Succeeeded");

            if (fileData == null || fileData.Tables.Count == 0 || fileData.Tables[0].Rows.Count == 0)
            {
                log.Info($"ProcessFile -{schemeType.ToString()} - Data Empty - All data for relevant authority Id:  {metadata.Details.RelevantAuthority} and Year: {metadata.Details.Year} will be deleted");

                return new ValidationResult
                {
                    ValidationType = ValidationType.Success,
                    Metadata = metadata
                };
            }

            var validationService = new ValidationService(metadata, fileData);
            var valResult = validationService.Validate();
            log.Info("ProcessFile - Validation Complete");

            if (valResult.ValidationType != ValidationType.Success)
            {
                return valResult;
            }

            switch (schemeType)
            {
                case SchemeType.Members:
                    log.Info("ProcessFile -Scheme Type Members");
                    ProcessMembers(fileData, metadata, log);
                    break;
                case SchemeType.Benefits:
                    log.Info("ProcessFile -Scheme Type Benefits");
                    ProcessBenefits(fileData, metadata, log);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(schemeType), schemeType, null);
            }

            return valResult;
        }

        public virtual DataSet GetFileAsDataSet(string path)
        {
            throw new NotImplementedException();
        }

        private void LoadMemberData(DataSet ds, int versionId)
        {
            var dt = _memberService.GetMemberDataTable(ds, versionId);
            _memberService.LoadMembershipToStaging(dt);
        }

        private void LoadBenefitData(DataSet ds, int versionId)
        {
            var dt = _benefitsService.GetBenefitsDataTable(ds, versionId);
            _benefitsService.LoadBenefitsToStaging(dt);
        }

        private void ProcessMembers(DataSet fileData, Metadata metadata, log4net.ILog log)
        {
            log.Info("ProcessMembers Start");
            LoadMemberData(fileData, metadata.VersionId);
            log.Info("ProcessMembers - LoadMemberData Succeeded");
        }

        private void ProcessBenefits(DataSet fileData, Metadata metadata, log4net.ILog log)
        {
            log.Info("ProcessBenefits Start");
            LoadBenefitData(fileData, metadata.VersionId);
            log.Info("ProcessBenefits - LoadBenefitData Succeeded");
        }
    }
}
