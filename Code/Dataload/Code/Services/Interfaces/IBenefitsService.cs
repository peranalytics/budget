﻿using System.Data;
using Models.Models;

namespace Services.Interfaces
{
    public interface IBenefitsService
    {
        void TruncateStagingTables();

        void LoadBenefitsToStaging(DataTable dt);
        DataTable GetBenefitsDataTable(DataSet ds, int versionId);

        ExecuteResult SaveBenefits(int versionId);
    }
}
