﻿using System.Collections.Generic;
using Models.Enums;
using Models.Models;

namespace Services.Interfaces
{
    public interface IMetadataService
    {
        IEnumerable<Metadata> LoadMetaFiles(string schemePath);

        void MarkMetadataVersionAsSuccess(int versionId, string localFileName);

        void CreateMetadataSummary();
    }
}
