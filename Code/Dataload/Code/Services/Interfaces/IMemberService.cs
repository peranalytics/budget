﻿using System.Data;
using Models.Models;

namespace Services.Interfaces
{
    public interface IMemberService
    {
        void TruncateStagingTables();

        DataTable GetMemberDataTable(DataSet ds, int versionId);

        void LoadMembershipToStaging(DataTable dt);

        ExecuteResult SaveMember(int versionId);

        void CreateMemberFlatFile();
    }
}
