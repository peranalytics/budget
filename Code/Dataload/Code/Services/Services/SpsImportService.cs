﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using Ionic.Zip;
using Models.Enums;
using Models.Models;
using Services.Extensions;
using Services.Interfaces;

namespace Services.Services
{
    public class SpsImportService : ISpsImportService
    {
        private readonly log4net.ILog _log;
        private readonly IMetadataService _metadataService;
        private IFileProcessingService _fileProcessingService;
        private readonly IMemberService _memberService;
        private readonly IBenefitsService _benefitsService;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public SpsImportService(log4net.ILog log)
        {
            _metadataService = new MetadataService(log);
            _mailerService = new MailerService();
            _memberService = new MembersService();
            _benefitsService = new BenefitsService();
            _log = log;
        }

        public void StartSpsDataload()
        {
            LoadSchemes();
            _log.Info("Finish");

            ArchiveLogFiles();
        }

        public void LoadSchemes()
        {
            _fileFolder = GlobalSettings.FileFolder;
            _log.Info($"FolderPath: {_fileFolder}");

            var filesExist = LoadFiles();

            if (!filesExist)
            {
                return; // DO NOT SEND EMAIL FOR NO FILES
            }

            _metadataService.CreateMetadataSummary();
            _log.Info("Meta Data Summary created");

            _memberService.CreateMemberFlatFile();
            _log.Info("Member Flat file created");

            if (GlobalSettings.SendEmail)
            {
                _mailerService.SendSuccessEmail();
            }
        }

        private bool LoadFiles()
        {
            var metas = _metadataService.LoadMetaFiles(_fileFolder).ToList();

            if (!metas.Any())
            {
                _log.Info($"No files exist in {_fileFolder}");
                return false;
            }

            var memberMetas = metas.Where(x => x.Details.SchemeType == SchemeType.Members);
            var memberMetasEnumerable = memberMetas as IList<Metadata> ?? memberMetas.ToList();

            if (memberMetasEnumerable.Any())
            {
                _log.Info("Membership Scheme Start");
                LoadFiles(memberMetasEnumerable, SchemeType.Members);
                _log.Info("Membership Scheme Finish");
            }

            var benefitMetas = metas.Where(x => x.Details.SchemeType == SchemeType.Benefits);
            var benefitMetasEnumerable = benefitMetas as IList<Metadata> ?? benefitMetas.ToList();

            if (benefitMetasEnumerable.Any())
            {
                _log.Info("Benefits Scheme Start");
                LoadFiles(benefitMetasEnumerable, SchemeType.Benefits);
                _log.Info("Benefits Scheme Finish");
            }

            return true;
        }

        private void LoadFiles(IEnumerable<Metadata> metas, SchemeType schemeType)
        {
            var orderedMetas = metas.OrderBy(x => x.Details.Modified);

            foreach (var meta in orderedMetas)
            {
                TruncateStagingTables(schemeType);

                var dataPath = $"{_fileFolder}{meta.Details.FileName}";
                ValidationResult result;

                switch (meta.Details.FileType)
                {
                    case FileType.Excel:
                        _fileProcessingService = new ExcelFileProcessingService();
                        _log.Info($"Processing {dataPath} as File Type Excel");
                        result = _fileProcessingService.ProcessFile(meta, schemeType, dataPath, _log);
                        break;
                    case FileType.Csv:
                        _fileProcessingService = new CsvFileProcessingService();
                        _log.Info($"Processing {dataPath} as File Type CSV");
                        result = _fileProcessingService.ProcessFile(meta, schemeType, dataPath, _log);
                        break;
                    default:
                        result = new ValidationResult
                        {
                            ValidationType = ValidationType.InvalidDataFileType,
                            Metadata = meta
                        };
                        _log.Fatal($"Invalid file type in meta data file { meta.MetadataFileName }");
                        break;
                }

                if (result.ValidationType == ValidationType.Success)
                {
                    var saveResult = Save(schemeType, meta.VersionId);

                    if (saveResult.Success)
                    {
                        if (saveResult.RowsAffected == meta.Details.RecordCount)
                        {
                            _log.Info($"File {dataPath} - saved successfully. {saveResult.RowsAffected} Records Added");
                            var localFileName = ArchiveFile(meta, dataPath);
                            _metadataService.MarkMetadataVersionAsSuccess(meta.VersionId, localFileName);
                        }
                        else
                        {
                            _log.Error(
                                $"Please Review {dataPath} - . {saveResult.RowsAffected} out of {meta.Details.RecordCount} saved. Please review file.");
                        }
                    }
                    else
                    {
                        _log.Error($"An error has occured during the processing of file {dataPath}. Please raise a ticket");
                    }
                }
                else
                {
                    _log.Fatal($"Invalid data in the file - does not match validation description - '{result.ValidationType.GetDescription()}' filename - '{ meta.MetadataFileName }'");
                }
            }
        }

        private void TruncateStagingTables(SchemeType schemeType)
        {
            switch (schemeType)
            {
                case SchemeType.Members:
                    _memberService.TruncateStagingTables();
                    break;
                case SchemeType.Benefits:
                    _benefitsService.TruncateStagingTables();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(schemeType), schemeType, "Scheme type not valid for truncate operation");
            }
        }

        private ExecuteResult Save(SchemeType schemeType, int versionId)
        {
            ExecuteResult result;

            switch (schemeType)
            {
                case SchemeType.Members:
                    result = _memberService.SaveMember(versionId);
                    break;
                case SchemeType.Benefits:
                    result = _benefitsService.SaveBenefits(versionId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(schemeType), schemeType, "Scheme type not valid for save operation");
            }

            return result;
        }

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }

        private string ArchiveFile(Metadata metadata, string filePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);
            var name = ExcludeNonValidChars(fileName);

            var archiveName = $"Year_{metadata.Details.Year}_RA_{metadata.Details.RelevantAuthority}_Version_{metadata.VersionId}_{name}{extension}";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            if (GlobalSettings.ArchiveWeb2)
            {
                var archiveForWeb2Path = $"{GlobalSettings.ArchiveForWeb2}{archiveName}";
                File.Copy(archivePath, archiveForWeb2Path);
            }

            var file = new FileInfo(filePath);
            file.Delete();

            file = new FileInfo(metadata.MetadataFileName);
            file.Delete();

            return archiveName;
        }

        public string ExcludeNonValidChars(string fileName)
        {
            fileName = fileName.ToLower();
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s", "-"); // //Replace spaces by dashes
            return fileName;
        }
    }
}



