﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Services;
using GoogleMaps.LocationServices;
using Models.Models;

namespace Coordinates
{
    public class LocationServices
    {
        private readonly IRelevantAuthorityDataAccessService _raDataAccess;

        public LocationServices()
        {
            _raDataAccess = new RelevantAuthorityDataAccessService();
        }

        public void PopulateLocations()
        {
            var ras = GetRelevantAuthorities();
            var relevantAuthorities = ras as IList<RelevantAuthority> ?? ras.ToList();

            if (!relevantAuthorities.Any())
            {
                return;
            }

            var tryCount = 1;
            var tryAgain = true;

            foreach (var ra in relevantAuthorities.Where( x=> x.Latitude == 0))
            {
                while (tryAgain)
                {
                    try
                    {
                        SetPoint(ra);
                        Thread.Sleep(2000);
                        tryAgain = false;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Exception - Attempt: " + tryCount);
                        tryAgain = tryCount < 4;
                        tryCount++;
                        Thread.Sleep(10000);
                        
                    }
                }

                tryAgain = true;
                tryCount = 1;
            }
        }

        public void SetPoint(RelevantAuthority ra)
        {
            var locationService = new GoogleLocationService();
            var point = locationService.GetLatLongFromAddress(ra.Name + ", Ireland");

            if (point == null) return;

            ra.Latitude = point.Latitude;
            ra.Longitude = point.Longitude;

            _raDataAccess.UpdateRelavantAuthority(ra.Id, ra.Latitude, ra.Longitude);

            Console.WriteLine(ra.Id.ToString());
        }

        private IEnumerable<RelevantAuthority> GetRelevantAuthorities()
        {
            var ras = _raDataAccess.GetRelevantAuthories();
            var raList = new List<RelevantAuthority>();

            if (ras != null && ras.Tables.Count > 0 && ras.Tables[0] != null && ras.Tables[0].Rows.Count > 0)
            {
                var dt = ras.Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    var ra = new RelevantAuthority
                    {
                        Id = Convert.ToInt32(row["Id"]),
                        Name = row["RelevantAuthorityName"].ToString(),
                        Latitude = row["Latitude"] is DBNull ? 0 : Convert.ToDouble(row["Latitude"]),
                        Longitude = row["Longtitude"] is DBNull ? 0 : Convert.ToDouble(row["Longtitude"])
                    };

                    raList.Add(ra);
                }
            }

            return raList;
        }
    }
}
