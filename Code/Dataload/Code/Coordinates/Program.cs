﻿namespace Coordinates
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var ls = new LocationServices();
            ls.PopulateLocations();
        }
    }
}
