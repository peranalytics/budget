﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using Load.DataAccessLayer;
using Load.Services.Extensions;
using Load.Services.Interfaces;
using Models.Enums;
using Models.Models;
using Newtonsoft.Json;

namespace Load.Services.Services
{
    public class ImportService
    {
        private readonly log4net.ILog _log;
        private IFileProcessingService _fileProcessingService;
        private readonly DataAccessService _dataAccessService;
        private readonly IMailerService _mailerService;

        private string _fileFolder;

        public ImportService(log4net.ILog log)
        {
            _dataAccessService = new DataAccessService();
            _mailerService = new MailerService();
            _log = log;
        }

        public void StartDataload()
        {
            LoadData();
            _log.Info("Finish");

            ArchiveLogFiles();
        }

        public void LoadData()
        {
            _fileFolder = GlobalSettings.FileFolder;
            _log.Info($"FolderPath: {_fileFolder}");

            var result = LoadFiles();

            _dataAccessService.GenerateBudgetFlatfile();
            _dataAccessService.GenerateFileHistorySummary();

            if (GlobalSettings.SendSuccessEmail && result)
            {
                _mailerService.SendSuccessEmail();
            }
        }

        private bool LoadFiles()
        {
            var metas = LoadMetaFiles(_fileFolder).ToList();

            if (!metas.Any())
            {
                _log.Info("Zero files to load");
                return false; // DO NOT SEND EMAIL FOR NO FILES
            }

            _log.Info($"Load Files Start - {metas.Count} files to process");

            var surveyMetas = metas.Where(x => x.Details.SchemeFile == SchemeFile.Survey);
            var surveyMetasEnumerable = surveyMetas as IList<JsonData> ?? surveyMetas.ToList();

            if (surveyMetasEnumerable.Any())
            {
                _log.Info("Survey Scheme Start");
                _log.Info("Truncate Staging tables - start");
                TruncateStagingTables();
                _log.Info("Truncate Staging tables - end");

                foreach (var survey in surveyMetasEnumerable)
                {
                    LoadSurvey(survey);
                }
            }

            var debtMetas = metas.Where(x => x.Details.SchemeFile == SchemeFile.Debt);
            var debtMetasEnumerable = debtMetas as IList<JsonData> ?? debtMetas.ToList();

            if (debtMetasEnumerable.Any())
            {
                _log.Info("Survey Scheme Start");
                _log.Info("Truncate Staging tables - start");
                TruncateDebtStagingTables();
                _log.Info("Truncate Staging tables - end");

                foreach (var debt in debtMetasEnumerable)
                {
                    LoadDebt(debt);
                }
            }

            _log.Info("Load Files End");

            return true;
        }

        private void LoadSurvey(JsonData jsonData)
        {
            var fileInfo = new FileInfo($"{_fileFolder}{jsonData.Details.FileName}");
            _fileProcessingService = new CsvFileProcessingService();
            var result = _fileProcessingService.ProcessFile(fileInfo, _log);

            if (result.Result == false)
            {
                _mailerService.SendWarningEmail();
            }

            var path = System.IO.Path.Combine(_fileFolder, jsonData.JsonDataFileName);
            var jsonFileInfo = new FileInfo(path);
            jsonFileInfo.Delete();
        }

        private void LoadDebt(JsonData jsonData)
        {
            var fileInfo = new FileInfo($"{_fileFolder}{jsonData.Details.FileName}");
            _fileProcessingService = new CsvFileProcessingService();
            var result = _fileProcessingService.ProcessDebtFile(fileInfo, jsonData, _log);

            if (result == false)
            {
                _mailerService.SendWarningEmail();
                return;
            }

            var path = System.IO.Path.Combine(_fileFolder, jsonData.JsonDataFileName);
            var jsonFileInfo = new FileInfo(path);
            jsonFileInfo.Delete();
        }

        public IEnumerable<JsonData> LoadMetaFiles(string schemePath)
        {
            _log.Info($"SchemePath: {schemePath}");

            var info = new DirectoryInfo(schemePath);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            if (!files.Any())
            {
                return new List<JsonData>();
            }

            var results = (from file in files where file.Extension == ".json" select ProcessMetaFile(file)).ToList();
            return results;
        }

        private JsonData ProcessMetaFile(FileSystemInfo file)
        {
            var stream = File.ReadAllText(file.FullName);

            var result = JsonConvert.DeserializeObject<JsonData>(stream);
            var resultDetails = JsonConvert.DeserializeObject<JsonDataDetails>(stream);

            if (result.Details == null)
            {
                result.Details = resultDetails;
            }

            result.JsonDataFileName = file.FullName;

            switch (resultDetails.SchemeFileName)
            {
                case "General Government Reporting - Survey":
                    resultDetails.SchemeFile = SchemeFile.Survey;
                    break;
                case "General Government Reporting - Debt":
                    resultDetails.SchemeFile = SchemeFile.Debt;
                    break;
            }

            return result;
        }

        private void TruncateStagingTables()
        {
            _dataAccessService.TruncateStagingTables();
        }

        private void TruncateDebtStagingTables()
        {
            _dataAccessService.TruncateDebtStagingTables();
        }

        private static void ArchiveLogFiles()
        {
            var info = new DirectoryInfo(GlobalSettings.LogFileFolder);
            var files = info.GetFiles().OrderBy(x => x.CreationTime).ToList();

            var deleteDate = DateTime.Now.AddDays(-GlobalSettings.LogDaysKeep);

            if (!files.Any())
            {
                return;
            }

            var filesToDelete = files.Where(x => x.CreationTime < deleteDate);

            foreach (var logFile in filesToDelete)
            {
                logFile.Delete();
            }
        }
    }
}



