﻿using System;

namespace Load.Services.Interfaces
{
    public interface IMailerService
    {
        void SendSuccessEmail();

        void SendWarningEmail();


        void SendErrorEmail(Exception exception);
    }
}
