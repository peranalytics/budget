﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Load.DataAccessLayer;
using Load.Services.Interfaces;
using Load.Services.Services;
using Models.Enums;
using Models.Models;

namespace Load.Services.Abstract
{
    public abstract class FileProcessingService : IFileProcessingService
    {
        private const int YearColumnStart = 2;
        private const int Row1DataStart = 15;
        private const int Row2DataStart = 28;
        private const int Row1DataRowCount = 12;
        private const int Row2DataRowCount = 15;
        private const int YearsRowPosition = 12;
        private const int YearsCount = 8;

        private readonly DataAccessService _dataAccessService;

        protected FileProcessingService()
        {
            _dataAccessService = new DataAccessService();
        }

        public ValidationResult ProcessFile(FileInfo file, log4net.ILog log)
        {
            log.Info($"File Processing - Start: Filename - {file.FullName}");
            var ds = GetFileAsDataSet(file.FullName);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 40)
            {
                log.Info("File - Empty");
                return new ValidationResult();
            }

            var dt = ds.Tables[0];

            log.Info("Reading year, bodies, stage from files - Start");
            var metadata = GenerateMetaData(dt);
            metadata.UploadedFileName = file.Name;

            var versionId = _dataAccessService.SaveMetadata(metadata);

            log.Info("Reading year, bodies, stage from files - End");

            metadata.VersionId = versionId;

            if (versionId == -1)
            {
                throw new Exception($"{file.Name} - Meta Data has not be saved correctly");
            }

            log.Info("Reading Budget Data - Start");
            var resultsDt = GenerateBudgetData(dt, metadata);
            log.Info("Reading Budget Data - End");

            log.Info("Saving Budget Data - Start");
            var res = SaveBudgetData(metadata, resultsDt);
            log.Info("Saving Budget Data - End");

            if (!res.Result)
            {
                return res;
            }

            var acrhiveFileName = ArchiveFile(file.FullName, metadata);
            metadata.ArchivedFileName = acrhiveFileName;

            log.Info($"File archived - {acrhiveFileName}");
            _dataAccessService.UpdateMetdata(metadata);

            log.Info("File Processing - End");

            return res;
        }

        public ValidationResult SaveBudgetData(MetaData metadata, DataTable dt)
        {
            var validationResult = ValidateMetadata(metadata);

            if (!validationResult.Result)
            {
                return validationResult;
            }

            _dataAccessService.LoadBudgetDataToStaging(dt);
            _dataAccessService.SaveBudgetData();
            metadata.Success = true;

            return new ValidationResult { Result = true };
        }

        public bool ProcessDebtFile(FileInfo file, JsonData jsonData, log4net.ILog log)
        {
            log.Info($"File Processing - Start: Filename - {file.FullName}");
            var ds = GetFileAsDataSet(file.FullName);

            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0] == null || ds.Tables[0].Rows.Count < 2)
            {
                log.Info("File - Empty");
                return false;
            }

            var dt = ds.Tables[0];
            var debtDt = CreateDebtTable();

            foreach (DataRow dataRow in dt.Rows)
            {
                for (var i = 0; i <= 6; i++)
                {
                    var debtRow = debtDt.NewRow();
                    debtRow[0] = 999; // VersionID TO DO
                    debtRow[1] = dataRow[0].ToString().Trim();
                    debtRow[2] = dataRow[1].ToString().Trim();
                    debtRow[3] = dataRow[2].ToString().Trim();
                    debtRow[4] = dataRow[3].ToString().Trim().Replace("`", "");
                    debtRow[5] = dataRow[4].ToString().Trim().Replace("`", "");
                    var stage = dataRow[5].ToString().Trim();
                    var stageYear = Convert.ToInt32(dataRow[6].ToString().Trim());
                    var startYear =  stageYear - 2;

                    debtRow[6] = stage;
                    debtRow[7] = stageYear;
                    debtRow[8] = startYear + i;
                    debtRow[9] = dataRow[i+7].ToString().Trim() == "" ? DBNull.Value : dataRow[i+7];

                    debtDt.Rows.Add(debtRow);
                }
            }

            _dataAccessService.LoadDebtDataToStaging(debtDt);

            var versionId = _dataAccessService.SaveDebtMetadata(file.Name);
            _dataAccessService.SaveDebtData();

            var acrhiveFileName = ArchiveDebtFile(file.FullName, versionId);
            _dataAccessService.UpdateMetdata(versionId, true, acrhiveFileName);
            _dataAccessService.GenerateDebtFlatfile();

            return true;
        }

        private ValidationResult ValidateMetadata(MetaData metadata)
        {
            return new ValidationResult
            {
                Result = true
            };
        }

        private DataTable GenerateBudgetData(DataTable dt, MetaData md)
        {
            var rowsPart1 = dt.AsEnumerable().Skip(Row1DataStart).Take(Row1DataRowCount);
            var rowsPart2 = dt.AsEnumerable().Skip(Row2DataStart).Take(Row2DataRowCount);

            var resultsDt = new DataTable();

            resultsDt.Columns.Add("VersionId", typeof(int));

            resultsDt.Columns.Add("ProviderId", typeof(int));
            resultsDt.Columns.Add("RelevantAuthorityId", typeof(int));
            resultsDt.Columns.Add("Stage", typeof(string));
            resultsDt.Columns.Add("StageYearId", typeof(int));

            resultsDt.Columns.Add("EsaCodeDescription", typeof(string));
            resultsDt.Columns.Add("EsaCode", typeof(string));
            resultsDt.Columns.Add("EsaCodeCategory", typeof(string));

            resultsDt.Columns.Add("YearId", typeof(int));
            resultsDt.Columns.Add("DataType", typeof(int));

            resultsDt.Columns.Add("Amount", typeof(decimal));

            resultsDt = ProcessBudgetRows(resultsDt, rowsPart1, md, DataType.Receipts);
            resultsDt = ProcessBudgetRows(resultsDt, rowsPart2, md, DataType.Expenditure);

            return resultsDt;
        }

        private DataTable ProcessBudgetRows(DataTable dt, IEnumerable<DataRow> rows, MetaData md, DataType dataType)
        {
            foreach (var dataRow in rows)
            {
                for (var i = YearColumnStart; i < YearColumnStart + YearsCount; i++)
                {
                    var year = md.Years[i - YearColumnStart];

                    var newRow = dt.NewRow();

                    newRow["VersionId"] = md.VersionId;

                    newRow["ProviderId"] = md.ProviderId;
                    newRow["RelevantAuthorityId"] = md.RelevantAuthorityId;

                    newRow["Stage"] = md.Stage;
                    newRow["StageYearId"] = md.StageYear;

                    newRow["EsaCodeDescription"] = dataRow[1].ToString();
                    newRow["EsaCode"] = dataRow[10].ToString();
                    newRow["EsaCodeCategory"] = dataRow[11].ToString();
                    newRow["YearId"] = year;

                    newRow["DataType"] = (int)dataType;

                    var value = dataRow[i].ToString().Replace(",", "");
                    var valueAsDecimal = !string.IsNullOrEmpty(value) ? Convert.ToDecimal(value) : 0;

                    newRow["Amount"] = valueAsDecimal;

                    dt.Rows.Add(newRow);
                }
            }

            return dt;
        }

        private MetaData GenerateMetaData(DataTable dt)
        {
            var dr = dt.Rows[0];
            var raName = dr[2].ToString();

            dr = dt.Rows[1];
            var raId = Convert.ToInt32(dr[2].ToString());

            dr = dt.Rows[2];
            var stage = dr[2].ToString();

            dr = dt.Rows[3];
            var providerName = dr[2].ToString();

            dr = dt.Rows[4];
            var providerId = Convert.ToInt32(dr[2].ToString());

            dr = dt.Rows[5];
            var stageYear = Convert.ToInt32(dr[2].ToString());

            dr = dt.Rows[YearsRowPosition];
            var years = new List<int>();

            for (var i = YearColumnStart; i < YearColumnStart + YearsCount; i++)
            {
                var year = Convert.ToInt32(dr[i].ToString().Replace(".00", ""));
                years.Add(year);
            }

            var md = new MetaData
            {
                RelevantAuthorityId = raId,
                RelevantAuthorityName = raName,
                ProviderId = providerId,
                ProviderName = providerName,
                Stage = stage,
                Years = years,
                StageYear = stageYear
            };

            return md;
        }

        private DataTable CreateDebtTable()
        {
            var debtDt = new DataTable();

            debtDt.Columns.Add("VersionId", typeof(int));

            debtDt.Columns.Add("ProviderName", typeof(string));
            debtDt.Columns.Add("Category", typeof(string));
            debtDt.Columns.Add("InputData", typeof(string));
            debtDt.Columns.Add("UniqueIds", typeof(string));
            debtDt.Columns.Add("Units", typeof(string));
            debtDt.Columns.Add("Forecast", typeof(string));
            debtDt.Columns.Add("StageYear", typeof(int));
            debtDt.Columns.Add("Year", typeof(int));
            debtDt.Columns.Add("Amount", typeof(decimal));

            return debtDt;
        }

        public virtual DataSet GetFileAsDataSet(string path)
        {
            throw new NotImplementedException();
        }

        private static string ArchiveFile(string filePath, MetaData md)
        {
            var extension = Path.GetExtension(filePath);
            var archiveName = $"Version_{md.VersionId}_Provider_{md.ProviderName}_RA_{md.RelevantAuthorityName}_StageYear_{md.StageYear}_Stage_{md.Stage}_{extension}";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            var file = new FileInfo(filePath);
            file.Delete();

            return archiveName;
        }

        private static string ArchiveDebtFile(string filePath, int versionId)
        {
            var extension = Path.GetExtension(filePath);
            var month = DateTime.Now.ToString("MMMM");
            var year = DateTime.Now.Year;
            var archiveName = $"Version_{versionId}_DebtData_StageYear_{year}_Month_{month}{extension}";
            var archivePath = $"{GlobalSettings.ArchiveFileFolder}{archiveName}";
            File.Copy(filePath, archivePath);

            var file = new FileInfo(filePath);
            file.Delete();

            return archiveName;
        }

        public string ExcludeNonValidChars(string fileName)
        {
            fileName = fileName.ToLower();
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s+", " ").Trim(); // convert multiple spaces into one space  
            fileName = System.Text.RegularExpressions.Regex.Replace(fileName, @"\s", "-"); // //Replace spaces by dashes
            return fileName;
        }
    }
}
