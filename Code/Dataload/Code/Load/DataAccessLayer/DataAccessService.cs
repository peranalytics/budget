﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Models.Models;

namespace Load.DataAccessLayer
{
    public class DataAccessService
    {
        public void LoadBudgetDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingBudgetData]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }


        public void SaveBudgetData()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[LoadBudgetData]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void GenerateBudgetFlatfile()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[GenerateBudgetFlatfile]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void GenerateFileHistorySummary()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistorySummaryCreate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public int SaveMetadata(MetaData md)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryAdd]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@ProviderId", SqlDbType.Int).Value = md.ProviderId;
                    command.Parameters.Add("@RelevantAuthorityId", SqlDbType.Int).Value = md.RelevantAuthorityId;
                    command.Parameters.Add("@Stage", SqlDbType.VarChar).Value = md.Stage;
                    command.Parameters.Add("@StageYearId", SqlDbType.Int).Value = md.StageYear;
                    command.Parameters.Add("@OriginalFileName", SqlDbType.VarChar).Value = md.UploadedFileName;

                    var version = command.Parameters.Add("@VersionId", SqlDbType.Int);
                    version.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    return Convert.ToInt32(version.Value);
                }
            }
        }

        public void UpdateMetdata(MetaData md)
        {
            UpdateMetdata(md.VersionId, md.Success, md.ArchivedFileName);
        }

        public void UpdateMetdata(int versionId, bool success, string archivedFileName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[FileHistoryUpdate]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;
                    command.Parameters.Add("@Valid", SqlDbType.Bit).Value = success;
                    command.Parameters.Add("@ArchivedFileName", SqlDbType.VarChar).Value = archivedFileName;

                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void TruncateStagingTables()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateStagingTables]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void TruncateDebtStagingTables()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateDebtStagingTables]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadDebtDataToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[dbo].[StagingDebtData]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public int SaveDebtMetadata(string uploadedFileName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[SaveDebtFileHistory]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@OriginalFileName", SqlDbType.VarChar).Value = uploadedFileName;

                    var version = command.Parameters.Add("@VersionId", SqlDbType.Int);
                    version.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    return Convert.ToInt32(version.Value);
                }
            }
        }

        public void SaveDebtData()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[LoadDebtData]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void GenerateDebtFlatfile()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[GenerateDebtFlatfile]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
