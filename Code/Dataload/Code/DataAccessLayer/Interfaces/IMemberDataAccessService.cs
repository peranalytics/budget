﻿using System.Data;
using Models.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IMemberDataAccessService
    {
        void TruncateStagingTables();

        void LoadMembershipToStaging(DataTable table);

        ExecuteResult SaveMember(int versionId);

        void CreateMemberFlatfile();
    }
}
