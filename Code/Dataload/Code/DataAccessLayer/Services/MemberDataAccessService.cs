﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Interfaces;
using Models.Models;

namespace DataAccessLayer.Services
{
    public class MemberDataAccessService : IMemberDataAccessService
    {
        public void TruncateStagingTables()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateMemberStagingTables]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadMembershipToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[Staging].[MembershipImport]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public ExecuteResult SaveMember(int versionId)
        {
            var execResult = new ExecuteResult();

            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[MemberSave]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;

                    var result = command.Parameters.Add("@Result", SqlDbType.Int);
                    result.Direction = ParameterDirection.Output;

                    var rowsAffected = command.Parameters.Add("@RowsAffected", SqlDbType.Int);
                    rowsAffected.Direction = ParameterDirection.Output;

                    var resultMessage = command.Parameters.Add("@ResultMessage", SqlDbType.VarChar, 100);
                    resultMessage.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    execResult.VersionId = versionId;
                    execResult.Message = (string)resultMessage.Value;
                    execResult.Success = Convert.ToInt32(result.Value) != -1;
                    execResult.RowsAffected = Convert.ToInt32(rowsAffected.Value);
                }
            }

            return execResult;
        }

        public void CreateMemberFlatfile()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[CreateMemberFlatfile]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
