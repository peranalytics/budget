﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Interfaces;
using Models.Models;

namespace DataAccessLayer.Services
{
    public class BenefitsDataAccessService : IBenefitsDataAccessService
    {
        public void TruncateStagingTables()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[TruncateBenefitStagingTables]", con) { CommandType = CommandType.StoredProcedure })
                {
                    con.Open();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadBenefitsToStaging(DataTable table)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "[Staging].[BenefitsImport]";
                    con.Open();
                    sqlBulkCopy.WriteToServer(table);

                    con.Close();
                }
            }
        }

        public ExecuteResult SaveBenefits(int versionId)
        {
            var execResult = new ExecuteResult();

            var connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
            using (var con = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand("[dbo].[BenefitsSave]", con) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.Add("@VersionId", SqlDbType.Int).Value = versionId;

                    var result = command.Parameters.Add("@Result", SqlDbType.Int);
                    result.Direction = ParameterDirection.Output;

                    var rowsAffected = command.Parameters.Add("@RowsAffected", SqlDbType.Int);
                    rowsAffected.Direction = ParameterDirection.Output;

                    var resultMessage = command.Parameters.Add("@ResultMessage", SqlDbType.VarChar, 100);
                    resultMessage.Direction = ParameterDirection.Output;

                    con.Open();
                    command.ExecuteNonQuery();

                    execResult.VersionId = versionId;
                    execResult.Message = (string)resultMessage.Value;
                    execResult.Success = Convert.ToInt32(result.Value) != -1;
                    execResult.RowsAffected = Convert.ToInt32(rowsAffected.Value);
                }
            }

            return execResult;
        }
    }
}
